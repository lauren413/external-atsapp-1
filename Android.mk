LOCAL_PATH:= $(call my-dir)


include $(CLEAR_VARS)
#include $(LOCAL_PATH)/snappyLibrary/Android.mk
LOCAL_MODULE = SnappyLibrary
LOCAL_MODULE_TAGS := eng
LOCAL_MANIFEST_FILE := snappyLibrary/src/main/AndroidManifest.xml
LOCAL_SRC_FILES := $(call all-java-files-under, snappyLibrary/src/main/java)
LOCAL_AAPT_FLAGS += --auto-add-overlay
LOCAL_STATIC_JAVA_LIBRARIES := kryo kryo-serializers
LOCAL_SDK_VERSION := 17
include $(BUILD_JAVA_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE = SseLibrary
LOCAL_MODULE_TAGS := eng
LOCAL_MANIFEST_FILE := sseLibrary/src/main/AndroidManifest.xml
LOCAL_SRC_FILES := $(call all-java-files-under, sseLibrary/src/main/java)
LOCAL_AAPT_FLAGS += --auto-add-overlay
LOCAL_SDK_VERSION := 17
include $(BUILD_JAVA_LIBRARY)




include $(CLEAR_VARS)
frameworks_res := $(LOCAL_PATH)/../../frameworks/base/core/res/res
appcompat_dir := $(LOCAL_PATH)/../../prebuilts/sdk/current/support/v7/appcompat/
LOCAL_MODULE_TAGS := eng
LOCAL_MANIFEST_FILE := app/src/main/AndroidManifest.xml
LOCAL_SRC_FILES := $(call all-java-files-under, app/src/main/java)
LOCAL_PACKAGE_NAME := Atsapp
LOCAL_CERTIFICATE := device/fsl/common/security/platform
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/app/src/main/res
LOCAL_RESOURCE_DIR += $(LOCAL_PATH)/app/src/main/res-demo
LOCAL_RESOURCE_DIR += $(appcompat_dir)/res
#LOCAL_RESOURCE_DIR += $(frameworks_res)
#LOCAL_AAPT_FLAGS += -v
LOCAL_AAPT_FLAGS += --auto-add-overlay
LOCAL_AAPT_FLAGS += --extra-packages android.support.design
LOCAL_STATIC_JAVA_LIBRARIES += android-common
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v4
#LOCAL_STATIC_JAVA_LIBRARIES += android-support-v7-gridlayout
LOCAL_STATIC_JAVA_LIBRARIES += SnappyLibrary
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v13
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v7-appcompat
LOCAL_STATIC_JAVA_LIBRARIES += retrofit okhttp3 okio gson simplexml commonscodec commonslang3 commonsio
#LOCAL_STATIC_JAVA_LIBRARIES += android-support-design
LOCAL_STATIC_JAVA_AAR_LIBRARIES := android-support-design
LOCAL_JAVA_LIBRARIES := SnappyLibrary SseLibrary
LOCAL_SDK_VERSION := current
LOCAL_REQUIRED_MODULES := SnappyLibrary
LOCAL_JNI_SHARED_LIBRARIES := snappydb_native
LOCAL_PROGUARD_ENABLED := disabled
include $(BUILD_PACKAGE)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := retrofit:app/libs/retrofit-2.1.0.jar \
										okio:app/libs/okio-1.10.0.jar \
	                                    okhttp3:app/libs/okhttp-3.4.1.jar \
										gson:app/libs/gson-2.7.jar \
										simplexml:app/libs/simple-xml-2.7.1.jar \
										commonscodec:app/libs/commons-codec-1.10.jar \
										commonslang3:app/libs/commons-lang3-3.3.2.jar \
										commonsio:app/libs/commons-io-2.4.jar \

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES += kryo:snappyLibrary/libs/kryo-2.24.0.jar \
	                                    kryo-serializers:snappyLibrary/libs/kryo-serializers-0.26.jar

#LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES += android-support-design:app/aars/design-24.2.0.aar
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES += android-support-design:app/aars/design-21.0.0.aar
#LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES += android-support-design:app/aars/design-22.2.1.aar
#LOCAL_SDK_VERSION := 22
include $(BUILD_MULTI_PREBUILT)



include $(CLEAR_VARS)
include $(LOCAL_PATH)/snappyLibrary/src/main/jni/common.mk

#LOCAL_SHARED_LIBRARIES := libstlport
#include external/stlport/libstlport.mk

LOCAL_MODULE := snappydb_native
#LOCAL_NDK_STL_VARIANT := c++_static
LOCAL_NDK_STL_VARIANT := gnustl_static
LOCAL_C_INCLUDES := $(C_INCLUDES)
LOCAL_SDK_VERSION := 17
LOCAL_CPP_EXTENSION := cc
LOCAL_CFLAGS := -DLEVELDB_PLATFORM_ANDROID -std=gnu++0x -g -w
LOCAL_SRC_FILES := $(SOURCES) ./port/port_android.cc snappydb.cpp snappydblocal.cpp
LOCAL_LDLIBS +=  -llog -ldl
include $(BUILD_SHARED_LIBRARY)
#include $(BUILD_STATIC_LIBRARY)


#include $(call all-makefiles-under,$(LOCAL_PATH)/snappyLibrary/src/main/jni/)
