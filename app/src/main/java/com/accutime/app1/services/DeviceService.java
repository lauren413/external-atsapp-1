package com.accutime.app1.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.accutime.app1.common.Constants;
import com.accutime.app1.retrofit.GsonConverterFactory;
import com.accutime.app1.retrofit.RestInterface;
import com.accutime.app1.retrofit.ScalarsConverterFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DeviceService extends Service {

    public final String TAG = this.getClass().getSimpleName();
    private static boolean isRunning = false;

    public static final String KEYBOARD_EVENT_KEY = "keyboardEvent";
    public static final String KEYBOARD_EVENT_ACTION = "keyboard_event_action";

//    // Keeps track of all current registered clients.
//    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
//
//    // Target we publish for clients to send messages to IncomingHandler.
//    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public IBinder onBind(Intent intent) {
        return null;
//        return mMessenger.getBinder();
    }

//    // Handler of incoming messages from clients.
//    class IncomingHandler extends Handler {
//        @Override
//        public void handleMessage(Message msg) {
//            if (msg.what == Constants.ServiceActions.MSG_REGISTER_CLIENT.ordinal()) {
//                 mClients.add(msg.replyTo);
//            }
////            else if (msg.what == Constants.ServiceActions.MSG_UNREGISTER_CLIENT.ordinal()) {
////                 mClients.remove(msg.replyTo);
////            }
//            else  {
//                 super.handleMessage(msg);
//            }
//        }
//    }

    int retryCount=20;
    boolean errored = false;
    public String readFullyAsString(InputStream inputStream, String encoding) throws IOException {
        return readFully(inputStream).toString(encoding);
    }
    public byte[] readFullyAsBytes(InputStream inputStream) throws IOException {
        return readFully(inputStream).toByteArray();
    }

    private ByteArrayOutputStream readFully(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.v(TAG, "Device Service onCreate() started");
        createRetrofitService();

        Thread keyboardThreadWithRetrofit = new Thread(new Runnable() {
            InputStream inputStream = null;
            boolean interrupted = false;

            @Override
            public void run() {
                while (!interrupted){
                    try {
                        if (inputStream != null) {
                            Log.d(TAG, "Closing inputStream");
                            inputStream.close();
                            inputStream = null;
                        }

                        Log.w(TAG, "keyboardThread.run:");
                        Call<ResponseBody> call = restInterface.getKeyboardStream();
                        Response<ResponseBody> response = call.execute();
                        inputStream = response.body().byteStream();

                       // Log.w(TAG, "keyboard stream : "+inputStream.toString());
                       // Log.w(TAG, "keyboard stream as byte : "+readFullyAsBytes(inputStream));
                       // Log.w(TAG, "keyboard stream as string: "+readFullyAsString(inputStream, "UTF_8"));
                        Log.d(TAG, "entering");

                        while (true) {
                            Log.d(TAG, "in while");

                            final byte[] buffer = new byte[256];
                                int rsz = 0;
                            Log.d(TAG, "reading");

                            rsz = inputStream.read(buffer, 0, buffer.length);
                            Log.d(TAG, "have read");

                            if (rsz < 0) {
                                    Log.w(TAG, "returning");
                                    return;
                                }
                            Log.d(TAG, "I'M STILL RUNNING");

                            //Log.d(TAG, "read bytes="+ StringUtilities.bytesToHex(buffer, StringUtilities.ByteFormat.RAW_TYPE));
                                byte[] nb = Arrays.copyOfRange(buffer, 0, rsz);
                                processProgressEvent(new String(nb, StandardCharsets.UTF_8));
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                        Log.d(TAG, e.toString());
                        interrupted = false;
                        try { Thread.sleep(3000); } catch (InterruptedException e1) { e1.printStackTrace(); }
                        //if (e instanceof InterruptedIOException)
                            //interrupted=true;
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }
                            inputStream = null;
                        }
                    }
                }
            }
        });
        keyboardThreadWithRetrofit.start();
        isRunning = true;

        Log.d(TAG, "Device Service onCreate() finished");
    }
//
//    private Thread keyboardThread = new Thread(new Runnable() {
//
//        private boolean keepRunning;
//
//        /** This is the rest api that will be used to listen for key presses */
//        private RestInterface clockRestAPI;
//
//        @Override
//        public void run() {
//            clearInputStream();
//            while (keepRunning) {
//                InputStream inputStream = null;
//                try {
//                    inputStream = getInputStream();
//                    readFromKeypad(inputStream);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    closeInputStream(inputStream);
//                }
//            }
//        }
//
//        private InputStream getInputStream() throws IOException {
//            Call<ResponseBody> call = clockRestAPI.getKeyboardStream();
//            Response<ResponseBody> response = call.execute();
//            return response.body().byteStream();
//        }
//
//        private void readFromKeypad(InputStream inputStream) {
//            int counter = 10;
//            while (keepRunning && counter > 0) {
//                String response = getStringFromInputStream(inputStream);
//                if (response.isEmpty()) {
//                    counter--;
//                }
//                processResponse(response);
//            }
//        }
//
//        private void closeInputStream(InputStream inputStream) {
//            if (inputStream != null) {
//                try {
//                    inputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        private void clearInputStream() {
//            InputStream inputStream = null;
//            try {
//                inputStream = getInputStream();
//                if (inputStream.available() > 0) {
//                    String contents = getStringFromInputStream(inputStream);
//                    Log.i(TAG, "Clearing : " + contents);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                closeInputStream(inputStream);
//            }
//        }
//
//
//
//        /**
//         * Process the full message given by input stream
//         * @param message
//         */
//        private synchronized void processResponse(String message) {
//
//            if (message.isEmpty()) {
////            Log.i(TAG, "Nothing read.");
//                return;
//            }
//
//            String line;
//            try {
//                // This is a string ... see fetchLineFromBuffer()
//                messageBuffer = message;
//                StringBuffer dataBuffer = new StringBuffer();
//                String field;
//                String value;
//                String name;
//
//                // Get first line
//                line = fetchLineFromBuffer();
//
//                // Keep running as long as there is another line
//                while (line != null) {
//
//                    // Find the colon. This separated the field from the value.
//                    int colonAt = line.indexOf(':');
//
//                    // Proceed as long as there is a colon.
//                    if (colonAt != 0) {
//
//                        // No colon? Line is field name with empty value
//                        if (colonAt == -1) {
//
//                            field = line;
//                            value = "";
//
//                        } else {
//
//                            // Field is the first part
//                            field = line.substring(0, colonAt);
//                            int valueAt = colonAt + 1;
//                            if (line.length() > valueAt && line.charAt(valueAt) == ' ') {
//                                valueAt++;
//                            }
//
//                            // Value is the second part
//                            value = line.substring(valueAt);
//                        }
//
//                        // Data field - this has the keypress.
//                        if (field.equals("data")) {
//                            dataBuffer.append(value);
//                        }
//
//                        // So there's stuff in the data buffer. This should be the value for data
//                        if (dataBuffer.length() > 0) {
//
//                            // Synchronized to prevent thread interference.
//                            synchronized (dataBuffer) {
//
////                            Log.v(TAG, "Getting keyboard event");
//
//                                // Get the keyboard event from the value portion
//                                sendKeyboardEvent(dataBuffer.toString());
//
//                                // Reset data buffer
//                                dataBuffer.setLength(0);
//                            }
//                        }
//
//                        // Get the next line.
//                        line = fetchLineFromBuffer();
//
//                    }
//
//                }
//
//            } catch (Exception e) {
//                Log.d(TAG, e.getMessage(), e);
//            }
//        }
//
//        /**
//         * Fetch a single line from the buffer
//         * @return that line
//         */
//        private String fetchLineFromBuffer() {
//
//            // Find the new line index
//            int indexNewLine = this.messageBuffer.indexOf("\n");
//
//            // If it doesn't exist, find the carraige return index
//            if (indexNewLine == -1) { indexNewLine = this.messageBuffer.indexOf("\r"); }
//
//            // Return the whole line up to that point and remove from message buffer
//            if (indexNewLine != -1) {
//                String thisLine = messageBuffer.substring(0, indexNewLine);
//                messageBuffer = messageBuffer.substring(indexNewLine + 1);
//                return thisLine;
//            }
//            // Return null if something went wrong
//            return null;
//        }
//
//        /**
//         * Send away the keyboard event
//         * @param keyboardEvent
//         */
//        private void sendKeyboardEvent(final String keyboardEvent) {
//
//            Log.v(TAG, "Keyboard event : " + keyboardEvent);
//
//            // Parse json to find keycode
//            JSONObject jsonObject = null;
//            try {
//                jsonObject = new JSONObject(keyboardEvent);
//                String keycode = jsonObject.getString("pressedKeycodes");
////            Log.v(TAG, "Keycode : " + keycode);
//
//                if (!keycode.equals("") && keycode.length() < 6) {
//
//                    // If valid, broadcast this keycode back to the activities
//                    Intent broadcastIntent = new Intent();
//                    broadcastIntent.setAction(actionType);
//                    broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
//                    broadcastIntent.putExtra(PARAM_OUT_MSG, keycode);
//                    context.sendBroadcast(broadcastIntent);
//                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    });

    RestInterface restInterface;
    public boolean createRetrofitService() {
        Log.d(TAG, "createRetrofitService");

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                //.readTimeout(1000, TimeUnit.MILLISECONDS)
                //.writeTimeout(1000, TimeUnit.MILLISECONDS)
                .readTimeout(Integer.MAX_VALUE, TimeUnit.MILLISECONDS)
                .writeTimeout(Integer.MAX_VALUE, TimeUnit.MILLISECONDS)
                .build();
        Retrofit deviceAdapter = new Retrofit.Builder()
                .baseUrl(Constants.getDeviceIpAddress())
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(okHttpClient)
                .build();

        restInterface = deviceAdapter.create(RestInterface.class);
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Received start id " + startId + ": " + intent);
        return START_STICKY; // run until explicitly stopped.
    }

    public static boolean isRunning() {
        return isRunning;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //nm.cancel(R.string.service_started); // Cancel the persistent notification.
        Log.d(TAG, "Service Stopped.");
        isRunning = false;
    }



    String lastEventId;
    final StringBuffer dataBuffer = new StringBuffer();
    String messageBuffer = "";
    boolean immediateReconnect = true;
    int retry;
    String sseLocation;

    private synchronized void processProgressEvent(String message) {
        //Log.d(TAG, "processProgressEvent:" + message);
        String line;
        try {
            messageBuffer = messageBuffer + message; // this is a string ... see fetch line from buffer
            String field = null;
            String value = null;
            String name = "message";
            String data = "";
            immediateReconnect = false;

            line = fetchLineFromBuffer();
            while (line != null) {
                //Log.d(TAG, "processProgressEvent:line="+line);
                if (line.length() == 0 && dataBuffer.length() > 0) {
                    synchronized (dataBuffer) {
                        int dataBufferlength = dataBuffer.length();
                        if (dataBuffer.charAt(dataBufferlength - 1) == '\n') {
                            dataBuffer.replace(dataBufferlength - 1, dataBufferlength, "");
                        }
                        sendKeyboardEvent(dataBuffer.toString());
                        dataBuffer.setLength(0);
                    }
                }

                int colonAt = line.indexOf(':');
                if (colonAt != 0) {
                    if (colonAt == -1) {
                        // no colon, line is field name with empty value
                        field = line;
                        value = "";
                    } else {
                        field = line.substring(0, colonAt);
                        int valueAt = colonAt + 1;
                        if (line.length() > valueAt && line.charAt(valueAt) == ' ') {
                            valueAt++;
                        }
                        value = line.substring(valueAt);
                    }
                    // process the field of completed event
                    if (field.equals("event")) {
                        name = value;
                    } else if (field.equals("id")) {
                        this.lastEventId = value;
                    } else if (field.equals("retry")) {
                        retry = Integer.parseInt(value);
                    } else if (field.equals("data")) {
                        // deliver event if data is specified and non-empty, or name is specified and not "message"
                        if (value != null || (name != null && name.length() > 0 && !name.equals("message"))) {
                            dataBuffer.append(value).append("\n");
                        }
                    } else if (field.equals("location")) {
                        if (value != null && value.length() > 0) {
                            this.sseLocation = value;
                        }
                    } else if (field.equals("reconnect")) {
                        immediateReconnect = true;
                    }
                }
                line = fetchLineFromBuffer();
            }


            if (immediateReconnect) {
                retry = 0;
                // this will be done on the load
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
        }
    }

    private void sendKeyboardEvent(final String keyboardEvent) {
        Log.d(TAG, "sendKeyboardEvent");

//        for (int i=mClients.size()-1; i>=0; i--) {
//            try {
//                Message msg = Message.obtain(null, Constants.ServiceActions.MSG_NEW_KEYBOARD_EVENT.ordinal());
//                Bundle b = new Bundle();
//                b.putString(KEYBOARD_EVENT_KEY, keyboardEvent);
//                msg.setData(b);
//                mClients.get(i).send(msg);
//            } catch (RemoteException e) {
//                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
//                mClients.remove(i);
//            }
//        }

        // TODO
        Intent intent = new Intent(KEYBOARD_EVENT_ACTION);
        intent.putExtra(KEYBOARD_EVENT_KEY, keyboardEvent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private String fetchLineFromBuffer() {
        //Log.i(TAG, "fetchLineFromBuffer");
        int lf = this.messageBuffer.indexOf("\n");
        if (lf == -1) {
            lf = this.messageBuffer.indexOf("\r");
        }
        if (lf != -1) {
            String ret = messageBuffer.substring(0, lf);
            messageBuffer = messageBuffer.substring(lf + 1);
            return ret;
        }
        return null;
    }
}
