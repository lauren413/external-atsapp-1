package com.accutime.app1.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.accutime.app1.businessObjects.ATSPunches;
import com.accutime.app1.retrofit.AtsService;
import com.accutime.app1.retrofit.RestAPIs;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * JobService to be scheduled by the MyJobScheduler.
 * start another service
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class PunchJob extends JobService {

    private static final String TAG = PunchJob.class.getSimpleName();

    @Override
    public boolean onStartJob(final JobParameters params) {
        Log.i(TAG, "onStartJob");

        AtsService.Punches batch = ATSPunches.batchToSend;

        if (batch == null) {
            ATSPunches.switchBatch(PunchJob.this);
            batch = ATSPunches.batchToSend;
        }

        Log.i(TAG, "Punches size: " + batch.punchList.size());

        Log.d(TAG, batch.mode);
        Log.d(TAG, batch.terminal);
        Log.d(TAG, batch.tz);
        Log.d(TAG, batch.uid);

        Call<ResponseBody> sendPunch = RestAPIs.atsService.sendPunch(batch);
        sendPunch.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response == null || response.body() == null) {
                    Log.e(TAG, "Null response from server.");
                    return;
                }

//                TreeNode tnode = XmlUtils.retrieveObject(rootNode.nodeId,
//                        atsApplication.getDbIdToPath(),
//                        atsApplication.getDbPathToId(),
//                        atsApplication.getDbRelations()
//                );
//
//                if (tnode != null) {
//                    TreeToXML txml = new TreeToXML(tnode);
//                    Util.Log.d(TAG, "Back to xml: " + txml);
//                }

                ATSPunches.clearBatchToSend(PunchJob.this);

                // TODO : Is this necessary? Doesn't appear so...
//                try {
//                    Log.d(TAG, "Punch response: " + response.body().string());
//
//                    XMLParser parser = new XMLParser();
//                    Document doc = parser.getDomElement(response.body().string());
//                    String operationStatus = doc.getDocumentElement().getTextContent();
//
//                    if (operationStatus.equals("OK")) {
//
//                        // <?xml version="1.0" encoding="UTF-8"?>
//                        // <OperationStatus uid="a3572d0e-f163-4f6f-92e8-bf58fd20106c">OK</OperationStatus>
//                        ATSPunches.clearBatchToSend(PunchJob.this);
//                        jobFinished(params, false);
//                        return;
//                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                jobFinished(params, false);
//                jobFinished(params, true);
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, t.toString());
                jobFinished(params, true);
            }
        });

        // There's still work to be done
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "onStopJob");

        // Don't reschedule. jobFinished() handles that.
        return false;
    }
}