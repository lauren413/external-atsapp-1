package com.accutime.app1.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.businessObjects.Employee;
import com.accutime.app1.loadables.ReadEmployeeFromDatabaseLoadable;
import com.accutime.app1.loader.Loader;

import java.io.IOException;

public class DatabaseService extends Service {

    public final String TAG = this.getClass().getSimpleName();
    private DatabaseService context = this;

    private static boolean isRunning = false;

    private Loader loader = new Loader("DatabaseServiceLoaderThread");
    private AtsApplication atsApplication = (AtsApplication) getApplication();

    protected static final String BADGE_KEY = "badge";
    protected static final String EMPLOYEE_KEY = "employee";

    public static final String READ_EMPLOYEE_FROM_DATABASE_ACTION = "ReadEmployeeFromDatabase";
    public static final String EMPLOYEE_DATA_RESPONSE = "EmployeeDataResponse";

    public static final String EMPLOYEE_BUNDLE_KEY = "EmployeeBundle";

    private BroadcastReceiver readEmployeeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Received an employee.");
            if (intent.getBundleExtra(EMPLOYEE_BUNDLE_KEY) != null) {
                readEmployeeFromDatabase(intent.getBundleExtra(EMPLOYEE_BUNDLE_KEY));
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "Database Service onCreate() started.");

        atsApplication = (AtsApplication) getApplication();
        isRunning = true;

        LocalBroadcastManager.getInstance(this).registerReceiver(readEmployeeReceiver,
                new IntentFilter(READ_EMPLOYEE_FROM_DATABASE_ACTION));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Received start id " + startId + ": " + intent);
        return START_STICKY; // Run until explicitly stopped.
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public void readEmployeeFromDatabase(final Bundle extra) {
        Log.v(TAG, "readEmployeeFromDatabase");

        String badgeNumber = extra.getString(BADGE_KEY);

        Loader.Callback readEmployeeFromDatabaseCallback = new Loader.Callback() {
            @Override public void onLoadCanceled(Loader.Loadable loadable) { }
            @Override public void onLoadError(Loader.Loadable loadable, IOException exception) { }
            @Override
            public void onLoadCompleted(Loader.Loadable loadable) {
                Log.v(TAG, "onLoadCompleted");

                ReadEmployeeFromDatabaseLoadable refl = (ReadEmployeeFromDatabaseLoadable) loadable;
                Employee emp = refl.employee;
                if (emp.badge == null) {
                    return;
                }
                sendMessageToClients(emp, extra);
            }
        };

        Loader.Loadable readEmployeeLoadable = new ReadEmployeeFromDatabaseLoadable(badgeNumber, atsApplication);
        loader.startLoading(readEmployeeLoadable, readEmployeeFromDatabaseCallback);
    }

    private void sendMessageToClients(Employee employee, Bundle extra) {
        if (extra != null) {
            extra.putParcelable(EMPLOYEE_KEY, employee);
        }

        // Broadcast to EmployeeDataReceiver -- for example, in main activity
        Intent intent = new Intent(EMPLOYEE_DATA_RESPONSE);
        intent.putExtra(EMPLOYEE_BUNDLE_KEY, extra);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(readEmployeeReceiver);

        super.onDestroy();
        isRunning = false;
    }
}
