package com.accutime.app1.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.accutime.app1.R;
import com.accutime.app1.businessObjects.State;
import com.accutime.app1.common.Constants;
import com.accutime.app1.services.DeviceService;
import com.accutime.app1.util.MyDialog;

import org.json.JSONException;
import org.json.JSONObject;

import static com.accutime.app1.common.Constants.KEY_MAIN_STATE;
import static com.accutime.app1.common.Constants.LAYOUT_MENU_CENTER_LEFT_RIGHT;
import static com.accutime.app1.common.Constants.LAYOUT_MENU_MAIN;
import static com.accutime.app1.common.Constants.LAYOUT_MENU_STRETCH_LEFT_RIGHT;
import static com.accutime.app1.common.Constants.LAYOUT_MENU_STRETCH_TOP_BOTTOM;

public abstract class BaseActivity extends Activity {

    private final String TAG = BaseActivity.class.getSimpleName();
    private Context context = this;

    private BroadcastReceiver keyboardEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String data = intent.getStringExtra(DeviceService.KEYBOARD_EVENT_KEY);
            if (data != null) {
                try {
                    processKeyboardEvent(data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "BaseActivity onCreate() started");

        // TODO : Start device service?
        LocalBroadcastManager.getInstance(this).registerReceiver(keyboardEventReceiver,
                new IntentFilter(DeviceService.KEYBOARD_EVENT_ACTION));


        Log.d(TAG, "BaseActivity onCreate() finished");
    }

    class MyCountdownTimer {

        private final String TAG = this.getClass().getSimpleName();
        private CountDownTimer timer;
        private static final long SHOW_COUNTDOWN_TIME = 4000;
        private State mState;

        MyCountdownTimer(long time, State state) {
            this.mState = state;
            cancel();
            start(time);
        }

        public void start(long duration) {
            timer = new CountDownTimer(duration, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    updateTimer(millisUntilFinished);
                }

                @Override
                public void onFinish() {
                    MyDialog.dismiss();

                    String timeoutState = mState.getTimeOutState();
                    if (timeoutState == null || timeoutState.isEmpty()) timeoutState = KEY_MAIN_STATE;
                    loadNewState(timeoutState);
                }
            };
            timer.start();
        }

        private void updateTimer(long millisUntilFinished) {
            if (millisUntilFinished <= SHOW_COUNTDOWN_TIME) {
                String time = String.valueOf((int) (Math.ceil(millisUntilFinished / 1000)));
                updateTimerView(time);
                showTimer();
            } else {
                hideTimer();
            }
        }

        public void cancel() {
            if (timer != null) {
                timer.cancel();
            }
        }
    }

    public abstract void updateTimerView(String time);
    public abstract void showTimer();
    public abstract void hideTimer();
    public abstract void loadNewState(String timeoutState);

    public int getLayoutID(String layoutName) {
        if (layoutName == null) return R.layout.menu_main;
        switch (layoutName) {
            case LAYOUT_MENU_MAIN:               return R.layout.menu_main;
            case LAYOUT_MENU_CENTER_LEFT_RIGHT:  return R.layout.menu_center_left_right;
            case LAYOUT_MENU_STRETCH_TOP_BOTTOM: return R.layout.menu_stretch_top_bottom;
            case LAYOUT_MENU_STRETCH_LEFT_RIGHT:
            default:                             return R.layout.menu_stretch_left_right;
        }
    }

    void removeViewIfNull(TextView view) {
        if ((view.getText() == null || view.getText().toString().isEmpty())
                && view.getBackground() == null) {
            view.setVisibility(View.GONE);
        }
    }

    /**
     * Processes a key or set of keys from keyboard.
     * @param data
     * @throws JSONException
     */
    protected synchronized void processKeyboardEvent(String data) throws JSONException {
        Log.d(TAG, "processKeyboardEvent:" + data);

        JSONObject event = new JSONObject(data);
        String pressedKeys = event.getString("pressedKeycodes");
        Log.d(TAG, "pressedKeycodes:" + pressedKeys);

        String[] keys = pressedKeys.split(" ");

        for (String k : keys) {
            // TODO : Make this work
            int scanCode = Integer.valueOf(k.trim());
            String key = Constants.scanCodeToKeyVal.get(scanCode);
            Log.d(TAG, "key = " + key);
        }

        if (keys.length > 1) { return; }

//        if (badgeDialog != null) {
//            badgeEmployeeView.setText(" ");
//            if (badgeDialog.isShowing()) {
////                for (String k : keys) {
////                    final int scanCode = Integer.valueOf(k.trim());
////                    final String key = Constants.scanCodeToKeyVal.get(scanCode);
////                    Log.d(TAG, "key = " + key);
////                    switch (key) {
////                        case "c":
////                            String a = badgeView.getText().toString();
////                            badgeView.setText("");
////                            break;
////                        case "e":
////                            String action = (String) badgeDialogView.getTag();
////                            String badge = badgeView.getText().toString();
////                            badgeDialog.dismiss();
////                            performBadgeProcessing(action, badge, null);
////                            break;
////                        default:
////                            String ss = badgeView.getText().toString() + key;
////                            badgeView.setText(ss);
////                            break;
////                    }
////                }
//            } else {
//                Log.d(TAG, "badgeDialog is not shown:" + pressedKeys);
//            }
//        } else {
//            Log.d(TAG, "badgeDialog is null:" + pressedKeys);
//        }
    }
}
