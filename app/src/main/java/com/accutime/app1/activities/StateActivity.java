package com.accutime.app1.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.R;
import com.accutime.app1.businessObjects.ATSPunches;
import com.accutime.app1.businessObjects.Biometric;
import com.accutime.app1.businessObjects.Employee;
import com.accutime.app1.businessObjects.State;
import com.accutime.app1.receivers.EmployeeDataReceiver;
import com.accutime.app1.retrofit.AtsService;
import com.accutime.app1.services.DatabaseService;
import com.accutime.app1.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.accutime.app1.common.Constants.ACTION_ADD_FINGERPRINT;
import static com.accutime.app1.common.Constants.ACTION_BREAK;
import static com.accutime.app1.common.Constants.ACTION_IN;
import static com.accutime.app1.common.Constants.ACTION_MEAL;
import static com.accutime.app1.common.Constants.ACTION_OUT;
import static com.accutime.app1.common.Constants.BUTTON_1_KEY;
import static com.accutime.app1.common.Constants.BUTTON_2_KEY;
import static com.accutime.app1.common.Constants.BUTTON_3_KEY;
import static com.accutime.app1.common.Constants.BUTTON_4_KEY;
import static com.accutime.app1.common.Constants.BUTTON_5_KEY;
import static com.accutime.app1.common.Constants.BUTTON_6_KEY;
import static com.accutime.app1.common.Constants.BUTTON_7_KEY;
import static com.accutime.app1.common.Constants.BUTTON_8_KEY;
import static com.accutime.app1.common.Constants.PROFILE_FILENAME;
import static com.accutime.app1.common.Constants.EMPLOYEE_EXTRA;
import static com.accutime.app1.common.Constants.ICON_ADD_FINGERPRINT;
import static com.accutime.app1.common.Constants.ICON_BACK;
import static com.accutime.app1.common.Constants.ICON_MANAGER;
import static com.accutime.app1.common.Constants.ICON_PUNCH_IN;
import static com.accutime.app1.common.Constants.ICON_PUNCH_OUT;
import static com.accutime.app1.common.Constants.KEY_ACTION;
import static com.accutime.app1.common.Constants.KEY_BADGE;
import static com.accutime.app1.common.Constants.KEY_HINT;
import static com.accutime.app1.common.Constants.KEY_ICON;
import static com.accutime.app1.common.Constants.KEY_LABEL;
import static com.accutime.app1.common.Constants.KEY_MAIN_STATE;
import static com.accutime.app1.common.Constants.KEY_NEXTSTATE;
import static com.accutime.app1.common.Constants.KEY_POST_ACTION_MESSAGE;
import static com.accutime.app1.common.Constants.STATE_EXTRA;
import static com.accutime.app1.common.Constants.STATE_TIME;
import static com.accutime.app1.common.Constants.TEXT_FIELD_KEY;

public class StateActivity extends BaseActivity implements View.OnClickListener {

    private final String TAG = StateActivity.class.getSimpleName();
    private Context context = StateActivity.this;

    private EditText mViewEditText;

    private TextView mViewTimer;
    private State mState;

    private Employee employee;
    private MyCountdownTimer mCountDownTimer;
    protected Biometric biometricObject;

    private EmployeeDataReceiver employeeDataReceiver = new EmployeeDataReceiver() {
        @Override
        public void performEmployeeProcessing(Bundle extra) {
            StateActivity.this.performEmployeeProcessing(extra);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "StateActivity onCreate() started");

        initializeCurrentState();

        Log.d(TAG, "Should not be here");

        if (mState != null && mState.thisStateExists())
        {
            initializeEmployee();
            setupButtons();
            setupTextViews();

            mCountDownTimer = new MyCountdownTimer(STATE_TIME, mState);
            biometricObject = new Biometric(this);
        }

        Log.v(TAG, "StateActivity onCreate() finished");
    }

    /**
     * Set current state to match the path passed from previous intent.
     */
    public void initializeCurrentState() {

        // Get what the current desired state should be
        String desiredState = getIntent().getStringExtra(STATE_EXTRA);
        if (desiredState == null) {
            Log.e(TAG, "Major error. No state given.");
            Util.showStateDoesNotExistDialog(context, "null");
            return;
        }

        // Find state in profile.
        String profile = Util.getFileFromInternalStorage(PROFILE_FILENAME);
        mState = new State(profile, desiredState);

        // If this state does not exist in the profile, show dialog and go back.
        if (!mState.thisStateExists()) {
            Util.showStateDoesNotExistDialog(context, desiredState);
            return;
        }

        // Set layout
        setContentView(getLayoutID(mState.getLayout()));

        // Set label
        String label = mState.getLabel();
        if (! Util.objectExists(label))
        {
            // Default label
            label = getString(R.string.welcome);
        }

        TextView tvLabel = (TextView) findViewById(R.id.tv_label);
        tvLabel.setText(label);
    }

    private void initializeEmployee() {
        employee = getIntent().getParcelableExtra(EMPLOYEE_EXTRA);

        String label = mState.getLabel();
        if (label == null) {
            label = getString(R.string.welcome_employee) + "\n" + employee.name;
        } else {
            label = String.format(label, employee.name);
        }

        TextView tvWelcome = (TextView) findViewById(R.id.tv_label);
        tvWelcome.setText(label);
    }

    /**
     * Setup all 8 buttons based off of the current state.
     */
    private void setupButtons() {
        TextView mButton1 = (TextView) findViewById(R.id.btn1);
        TextView mButton2 = (TextView) findViewById(R.id.btn2);
        TextView mButton3 = (TextView) findViewById(R.id.btn3);
        TextView mButton4 = (TextView) findViewById(R.id.btn4);
        TextView mButton5 = (TextView) findViewById(R.id.btn5);
        TextView mButton6 = (TextView) findViewById(R.id.btn6);
        TextView mButton7 = (TextView) findViewById(R.id.btn7);
        TextView mButton8 = (TextView) findViewById(R.id.btn8);

        setupButton(mButton1, BUTTON_1_KEY, mState);
        setupButton(mButton2, BUTTON_2_KEY, mState);
        setupButton(mButton3, BUTTON_3_KEY, mState);
        setupButton(mButton4, BUTTON_4_KEY, mState);
        setupButton(mButton5, BUTTON_5_KEY, mState);
        setupButton(mButton6, BUTTON_6_KEY, mState);
        setupButton(mButton7, BUTTON_7_KEY, mState);
        setupButton(mButton8, BUTTON_8_KEY, mState);
    }

    private void setupButton(TextView button, String btnKey, State state) {
        if (button != null) {
            button.setOnClickListener(this);
            button.setText(state.getAttribute(btnKey, KEY_LABEL));
            button.setBackground(getIconFor(state.getAttribute(btnKey, KEY_ICON)));

            removeViewIfNull(button);
        }
    }

    private void setupTextViews() {
        mViewTimer = (TextView) findViewById(R.id.tv_timer);
        mViewEditText = (EditText) findViewById(R.id.et_badge_number);

        String fieldLabel = mState.getAttribute(TEXT_FIELD_KEY, KEY_HINT);

        if (fieldLabel != null) {
            mViewEditText.setVisibility(View.VISIBLE);
            mViewEditText.setHint(fieldLabel);
            mViewEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
                    switch (actionId) {
                        case EditorInfo.IME_ACTION_DONE:
                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                            if (imm != null) {imm.hideSoftInputFromWindow(mViewEditText.getWindowToken(), 0);}
                            return true;
                    }
                    return false;
                }
            });
        }

        else {
            mViewEditText.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        String btnKey;
        switch (v.getId()) {
            case R.id.btn1: btnKey = BUTTON_1_KEY; break;
            case R.id.btn2: btnKey = BUTTON_2_KEY; break;
            case R.id.btn3: btnKey = BUTTON_3_KEY; break;
            case R.id.btn4: btnKey = BUTTON_4_KEY; break;
            case R.id.btn5: btnKey = BUTTON_5_KEY; break;
            case R.id.btn6: btnKey = BUTTON_6_KEY; break;
            case R.id.btn7: btnKey = BUTTON_7_KEY; break;
            case R.id.btn8: btnKey = BUTTON_8_KEY; break;
            default: return;
        }

        Log.i(TAG, "Button key is: " + btnKey);

        String action = mState.getAttribute(btnKey, KEY_ACTION);
        String message = mState.getAttribute(btnKey, KEY_POST_ACTION_MESSAGE);
        String nextState = mState.getAttribute(btnKey, KEY_NEXTSTATE);

        performAction(action, message, nextState);
    }

    private void performAction(String action, String message, String nextState) {
        if (action == null) {
            loadNewState(nextState);
            return;
        }

        switch (action) {
            case ACTION_ADD_FINGERPRINT:
                Bundle bundle = new Bundle();
                bundle.putString(KEY_ACTION, action);
                bundle.putString(KEY_NEXTSTATE, nextState);
                bundle.putString(KEY_BADGE, mViewEditText.getText().toString());
                performBadgeProcessing(bundle);
                break;
            case ACTION_IN:
            case ACTION_OUT:
            case ACTION_MEAL:
            case ACTION_BREAK:
                sendPunch(action, message, nextState);
                break;
        }
    }


    public void performBadgeProcessing(Bundle extra) {
        Log.v(TAG, "performBadgeProcessing");

        // Check for invalid entry
        if (TextUtils.isEmpty(extra.getString(KEY_BADGE))) {
            Util.displayToast(context, "Badge number is empty.");
            return;
        }

        // Broadcast to Database Service
        Intent intent = new Intent(DatabaseService.READ_EMPLOYEE_FROM_DATABASE_ACTION);
        intent.putExtra(DatabaseService.EMPLOYEE_BUNDLE_KEY, extra);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void loadNewState(String nextState) {
        if (nextState == null || nextState.isEmpty() || nextState.equals(KEY_MAIN_STATE)) {
            employee = null;
            startActivity(new Intent(context, MainActivity.class));
        } else {
            Intent intent = new Intent(context, StateActivity.class);
            intent.putExtra(STATE_EXTRA, nextState);
            intent.putExtra(EMPLOYEE_EXTRA, employee);
            startActivity(intent);
        }
    }

    /**
     * Employee has punched the clock. Do something about it.
     */
    public void sendPunch(String action, String message, String nextState) {

        // Get employee info
        final String badge = employee.badge;
        final String name = employee.name;
        Log.d(TAG, "Send Punch " + action +
                " for employee " + name +
                " with badge #" + badge);

        AtsService.Punches.Punch punch = new AtsService.Punches.Punch();
        punch.Badge = badge;

        // TODO : This should be customizable
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
        punch.Timestamp = sdf.format(new Date());

        punch.Action = action;

        ATSPunches.addPunchToBatch(context, punch);
        Util.displayToast(context, message);
        loadNewState(nextState);
    }

    /**
     * Database incoming handler from base activity points here.
     * Start a fingerprint capture scan for the given employee.
     */
    public void performEmployeeProcessing(Bundle extra) {
        Log.d(TAG, "performEmployeeProcessing: " + extra.toString());

        String action = extra.getString(KEY_ACTION);
        if (action == null || TextUtils.isEmpty(action)) {
            Log.e(TAG, getString(R.string.action_not_found));
//            Util.displayToast(context, getString(R.string.action_not_found));
            return;
        }

        Employee employee = extra.getParcelable(EMPLOYEE_EXTRA);
        if (employee == null || TextUtils.isEmpty(employee.name)) {
            Util.displayToast(context, getString(R.string.employee_not_found));
            return;
        }

        final String nextState = extra.getString(KEY_NEXTSTATE);
        if (nextState == null || TextUtils.isEmpty(nextState)) {
            Util.displayToast(context, "Next State not found");
            return;
        }

        Log.d(TAG, String.format("Action is %s, Employee is %s, NextState is %s",
                action, employee, nextState));

        switch (action) {
            case ACTION_ADD_FINGERPRINT:
                AtsApplication application = (AtsApplication) getApplication();
                biometricObject.startCaptureScan(employee.badge, application, new Biometric.BiometricCallback() {
                    @Override public void onFinished() {
                        loadNewState(nextState);
                    }
                });
                break;
        }
    }

    @Override
    public void showTimer() {
        mViewTimer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTimer() {
        mViewTimer.setVisibility(View.INVISIBLE);
    }

    @Override
    public void updateTimerView(String time) {
        mViewTimer.setText(time);
    }

    @Override
    public void onUserInteraction(){
        super.onUserInteraction();

        // Reset the timer on user interaction
        mCountDownTimer.cancel();
        mCountDownTimer.start(STATE_TIME);
    }

    @Override
    protected void onStart() {
        super.onStart();

        LocalBroadcastManager.getInstance(this).registerReceiver(employeeDataReceiver,
                new IntentFilter(DatabaseService.EMPLOYEE_DATA_RESPONSE));
    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            if (employeeDataReceiver != null) {
                unregisterReceiver(employeeDataReceiver);
            }
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Failed to unregister receiver.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // TODO : Probably should have something better than this.
        employee = null;

    }

    @Override
    public void onPause() {
        super.onPause();

        if (Util.objectExists(mCountDownTimer)) {
            mCountDownTimer.cancel();
        }
    }

    public Drawable getIconFor(String key) {
        Context context = this;
        if (key == null) return null;
        switch (key) {
            case "clock-in":            return context.getDrawable(R.drawable.clock_in);
            case "clock-in_rev":        return context.getDrawable(R.drawable.clock_out_right); // TODO
            case "break_start":         return context.getDrawable(R.drawable.break_start);
            case "break_end":           return context.getDrawable(R.drawable.break_end); // TODO
            case "meal-start":          return context.getDrawable(R.drawable.meal_start); // TODO
            case "meal-end":            return context.getDrawable(R.drawable.meal_end);
            case "back":                return context.getDrawable(R.drawable.arrow_back);
            case "gtk.STOCK_SAVE":      return context.getDrawable(R.drawable.check_ok);
            case "gtk.STOCK_CANCEL":    return context.getDrawable(R.drawable.cancel_x);
            case "clock-out":           return context.getDrawable(R.drawable.clock_out_right);
            case "employee-left":       return context.getDrawable(R.drawable.employee_left);
            case "schedule-left":       return context.getDrawable(R.drawable.schedule_left);
            case "call-back":           return context.getDrawable(R.drawable.ats_icon); // TODO
            case "benefits-left":       return context.getDrawable(R.drawable.benefits_right); // TODO
            case "transfer":            return context.getDrawable(R.drawable.transfer);
            case "info":                return context.getDrawable(R.drawable.info_left);
            case "help":                return context.getDrawable(R.drawable.employee_right); // TODO
            case "manager":             return context.getDrawable(R.drawable.manager); // TODO
            case "dictionary":          return context.getDrawable(R.drawable.ats_icon); // TODO
            case "forward":             return context.getDrawable(R.drawable.arrow_forward);
            case "lan":                 return context.getDrawable(R.drawable.ats_icon); // TODO
            case "tools":               return context.getDrawable(R.drawable.ats_icon); // TODO
            case "network":             return context.getDrawable(R.drawable.ats_icon); // TODO
            case "power":               return context.getDrawable(R.drawable.ats_icon); // TODO
            case "power2":              return context.getDrawable(R.drawable.ats_icon); // TODO
            case "reset":               return context.getDrawable(R.drawable.ats_icon); // TODO
            case "edit":                return context.getDrawable(R.drawable.ats_icon); // TODO

            case ICON_PUNCH_IN:       return context.getDrawable(R.drawable.clock_in);
            case ICON_PUNCH_OUT:      return context.getDrawable(R.drawable.clock_out_right);
            case ICON_MANAGER:        return context.getDrawable(R.drawable.manager);
            case ICON_ADD_FINGERPRINT: return context.getDrawable(R.drawable.add_finger);
            case ICON_BACK:           return context.getDrawable(R.drawable.arrow_back);
        }
        return null;
    }
}