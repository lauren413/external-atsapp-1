//package com.accutime.app1.activities;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.webkit.WebChromeClient;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.accutime.app1.Application.AtsApplication;
//import com.accutime.app1.R;
//import com.accutime.app1.retrofit.AtsService;
//import com.accutime.app1.retrofit.SimpleXmlConverterFactory;
//
//import retrofit2.Retrofit;
//
//
//public class EssActivity extends Activity {
//
//    public static final String TAG = EssActivity.class.getSimpleName();
//
//    AtsApplication atsApplication = (AtsApplication) getApplication();
//    AtsService service;
//    private WebView webView;
//    private ProgressBar progressBar;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.employee_portal);
//
//        // Get the AtsApplication
//        atsApplication = (AtsApplication) getApplication();
//
//        // Create AtsService
//        createAtsService();
//
//        // Load the progress bar and web view.
//        loadWebView();
//        loadTextViews();
//    }
//
//    /**
//     * Create an ATSService using retrofit.
//     */
//    protected void createAtsService() {
//        Retrofit restAdapter = new Retrofit.Builder()
//                .baseUrl("http://atsqa-wd.accu-time.com/")
//                .addConverterFactory(SimpleXmlConverterFactory.create())
//                .build();
//        service = restAdapter.create(AtsService.class);
//    }
//
//    /**
//     * Loads the web view and progress bar to display while loading.
//     */
//    private void loadWebView() {
//        Log.i(TAG, "loadWebView");
//        progressBar = (ProgressBar) findViewById(R.id.progressBar);
//
//        webView = (WebView) findViewById(R.id.webView);
//        webView.setWebChromeClient(new WebChromeClient());
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                progressBar.setVisibility(View.VISIBLE);
//                progressBar.bringToFront();
//                super.onPageStarted(view, url, favicon);
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                Log.i(TAG, "Finished loading URL: " + url);
//                progressBar.setVisibility(View.GONE);
//                super.onPageFinished(view, url);
//            }
//        });
//
//        // Set up settings for the web view
//        WebSettings webSettings = webView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        webSettings.setUseWideViewPort(true);
//        webSettings.setLoadWithOverviewMode(true);
//
//        // Enable pinch to zoom without the zoom buttons
//        webSettings.setBuiltInZoomControls(true);
//        webSettings.setDisplayZoomControls(false);
//        webSettings.setSupportZoom(true);
//
//        // Enable remote debugging via chrome://inspect
//        WebView.setWebContentsDebuggingEnabled(true);
//
//        //webView.zoomIn();
//        //webView.zoomBy(10);
//    }
//
//    /**
//     * Load text views and define on click listeners.
//     */
//    private void loadTextViews() {
//        Log.i(TAG, "loadTextViews");
//
//        TextView greeting = (TextView) findViewById(R.id.greeting);
//        String badge = getIntent().getStringExtra("badge");
//        String name = getIntent().getStringExtra("name");
//        String greetingText = "Welcome " + name;
//        greeting.setText(greetingText);
//
//        TextView benefits = (TextView) findViewById(R.id.benefits);
//        benefits.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //webView.loadUrl("https://youtu.be/uf1pPOSjiCI");
//                webView.setVisibility(View.VISIBLE);
//                webView.loadUrl("https://impl.workday.com/ats_dpt2/d/home.htmld#selectedWorklet=501%2470");
//            }
//        });
//
//        TextView vacation = (TextView) findViewById(R.id.vacation);
//        vacation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //webView.loadUrl("https://youtu.be/uf1pPOSjiCI");
//                webView.setVisibility(View.VISIBLE);
//                webView.loadUrl("https://impl.workday.com/ats_dpt2/d/home.htmld#selectedWorklet=501%2480");
//            }
//        });
//
//        TextView logout = (TextView) findViewById(R.id.logout);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                EssActivity.this.finish();
//            }
//        });
//    }
//
//    @Override
//    public void onBackPressed() {
//
//        // Toggle web view visibility
//        if (webView.getVisibility() == View.VISIBLE)
//            webView.setVisibility(View.INVISIBLE);
//        else webView.setVisibility(View.VISIBLE);
//    }
//
//    // Is this needed?
//    private class MyWebViewClient extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if (Uri.parse(url).getHost().equals("www.google.com")) {
//                // This is my web site, so do not override; let my WebView load the page
//                return false;
//            }
//            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            startActivity(intent);
//            return true;
//        }
//    }
//}
