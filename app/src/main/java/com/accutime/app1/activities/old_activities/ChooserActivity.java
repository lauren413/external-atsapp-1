//package com.accutime.app1.activities.old_activities;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.content.res.Configuration;
//import android.os.Bundle;
//import android.os.RemoteException;
//import android.text.TextUtils;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.RelativeLayout;
//import android.widget.Toast;
//
//import com.accutime.app1.Application.AtsApplication;
//import com.accutime.app1.R;
//import com.accutime.app1.activities.EssActivity;
//import com.accutime.app1.activities.TimeTrackerActivity;
//import com.accutime.app1.util.Measurement;
//import com.accutime.app1.businessObjects.Employee;
//import com.accutime.app1.common.Constants;
//import com.accutime.app1.loader.Loader;
//import com.accutime.app1.retrofit.AtsService;
//import com.accutime.app1.retrofit.DeviceInterface;
//import com.accutime.app1.util.PreferenceUtilities;
//import com.accutime.app1.util.Util;
//import com.accutime.app1.xmlparser.TagNode;
//import com.accutime.app1.xmlparser.TreeBuilder;
//import com.accutime.app1.xmlparser.TreeNode;
//import com.accutime.app1.xmlparser.TreeToXML;
//import com.accutime.app1.xmlparser.XmlUtils;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.io.StringReader;
//import java.util.UUID;
//
//import okhttp3.ResponseBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//import static com.accutime.app1.retrofit.RestAPIs.deviceInterface;
//
////import static com.accutime.app1.activities.BaseActivity.deviceInterface;
//
//
//public class ChooserActivity extends OldBaseActivity implements View.OnClickListener {
//
//    public final String TAG = this.getClass().getSimpleName();
//
//    Context context = ChooserActivity.this;
//    AlertDialog fingerPromptDialog;
//    Measurement heartbeatMeasurement = new Measurement();
//    Measurement getEmployeeMeasurement = new Measurement();
//    Measurement storeEmployeeMeasurement = new Measurement();
//
//    public final boolean ADD_LOCAL_EMPLOYEES = true;
//
//    AlertDialog foundDialog;
//
//    // Test employees.
//    // TODO : Move to strings.xml
//    String testEmployees =
//            "<Employees>" +
//            "  <Employee badge=\"900000212\" supervisor_level=\"1\" " +
//            "        schedule_id=\"3479\" break_schedule_id=\"3479\"" +
//            "        meal_schedule_id=\"3479\" language=\"\"" +
//            "        >\n" +
//            "    <Name>William Bailey</Name>\n" +
//            "    <WeekToDateHours>0</WeekToDateHours>\n" +
//            "    <SecondPunchTimeRestrict>1</SecondPunchTimeRestrict>\n" +
//            "  </Employee>\n" +
//            "</Employees>";
//    String localEmployee1 =
//            "<Employee badge=\"33445566\" supervisor_level=\"2\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                    "<Name>Jagat Brahma</Name> " +
//                    "<WeekToDateHours>0</WeekToDateHours> "+
//                    "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                    "</Employee> " ;
//    String localEmployee2 =
//            "<Employee badge=\"22334455\" supervisor_level=\"9\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                    "<Name>Carlos Bernal</Name> " +
//                    "<WeekToDateHours>0</WeekToDateHours> "+
//                    "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                    "</Employee> " ;
//    String localEmployee3 =
//            "<Employee badge=\"11223344\" supervisor_level=\"9\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                "<Name>Jean-Pierre Van de Capelle</Name> " +
//                "<WeekToDateHours>0</WeekToDateHours> "+
//                "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//            "</Employee> " ;
//    String localEmployee4 =
//            "<Employee badge=\"44556677\" supervisor_level=\"3\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                "<Name>Asha Hemingway</Name> " +
//                "<WeekToDateHours>0</WeekToDateHours> "+
//                "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//            "</Employee> " ;
//    String localEmployee5 =
//            "<Employee badge=\"55667788\" supervisor_level=\"4\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                "<Name>Ryan McColgan</Name> " +
//                "<WeekToDateHours>0</WeekToDateHours> "+
//                "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//            "</Employee> " ;
//
//    String[] localEmployees = {
//            localEmployee1,
//            localEmployee2,
//            localEmployee3,
//            localEmployee4,
//            localEmployee5
//    };
//
//    public static String txmlString;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//
//        Log.d(TAG, "Chooser Activity onCreate() started");
//
//        // Adjust display
//        updateDisplayConfigs();
//
//        // Inflate custom layout to screen
//        final LayoutInflater inflater = getLayoutInflater();
//        rootView = (RelativeLayout) inflater.inflate(R.layout.home, null);
//        setContentView(rootView);
//        super.onCreate(savedInstanceState);
//
////        TextClock currentTime = (TextClock) findViewById(R.id.currentTime);
////        TextClock currentDate = (TextClock) findViewById(R.id.currentDate);
////        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
//
//        txmlString = "No employees loaded yet";
//
//        // Set on-click listeners to each view.
//        findViewById(R.id.employeePortal)   .setOnClickListener(this);
//        findViewById(R.id.managerPortal)    .setOnClickListener(this);
//        findViewById(R.id.clockIn)          .setOnClickListener(this);
//        findViewById(R.id.clockOut)         .setOnClickListener(this);
//        findViewById(R.id.mealStart)        .setOnClickListener(this);
//        findViewById(R.id.mealEnd)          .setOnClickListener(this);
//        findViewById(R.id.breakStart)       .setOnClickListener(this);
//        findViewById(R.id.breakEnd)         .setOnClickListener(this);
//        findViewById(R.id.btnToSetup)       .setOnClickListener(this);
//
//        // Set up a fingerprint prompt
//        fingerPromptDialog = new AlertDialog.Builder(this).setTitle("").create();
//        fingerPromptDialog.setMessage("Place Your Finger on the Finger Print Reader");
//        fingerPromptDialog.setCancelable(false);
//
//        Log.d(TAG, "Chooser Activity onCreate() finished");
//    }
//
//    /**
//     * Adjust display for different size screens?
//     * Commenting this out doesn't affect anything.
//     */
//    private void updateDisplayConfigs() {
//        Log.d(TAG, "updateDisplayConfigs");
//
//        DisplayMetrics metrics = getResources().getDisplayMetrics();
////        Log.e(TAG, "device dpi="+metrics.densityDpi);
////        Log.e(TAG, "device display size= "+metrics.widthPixels+" x "+metrics.heightPixels);
//
//        Configuration config = new Configuration();
//        if (metrics.heightPixels < 500)
//            config.smallestScreenWidthDp = 444;
//        else
//            config.smallestScreenWidthDp = 764;
//
////        Log.e(TAG, "smallestScreenWidthDp= "+config.smallestScreenWidthDp);
////        Log.e(TAG, "resouces are taken from '"+getResources().getString(R.string.smallestWidth) + "' folder");
//        getBaseContext().getResources().updateConfiguration(config,metrics);
//    }
//
//    @Override
//    public void onClick(View v) {
//        int tag = -1;
//        switch (v.getId()) {
//            case R.id.employeePortal:   { tag = ESS_TAG;            break; }
//            case R.id.managerPortal:    { tag = MANAGER_TAG;        break; }
//            case R.id.timeTrackButton:  { tag = TIME_TRACKING_TAG;  break; }
//            case R.id.clockIn:          { tag = CLOCK_IN_TAG;       break; }
//            case R.id.clockOut:         { tag = CLOCK_OUT_TAG;      break; }
//            case R.id.mealStart:        { tag = MEAL_START_TAG;     break; }
//            case R.id.mealEnd:          { tag = MEAL_END_TAG;       break; }
//            case R.id.breakStart:       { tag = BREAK_START_TAG;    break; }
//            case R.id.breakEnd:         { tag = BREAK_END_TAG;      break; }
//            case R.id.btnToSetup:       { openSetup();              break; }
//        }
//
//        badgeView           .setText("");
//        badgeEmployeeView   .setText(" ");
//        badgeDialog         .show();
//        badgeDialogView     .setTag(tag);
//    }
//
//    /**
//     * Open Clock Setup application
//     */
//    private void openSetup() {
//        Log.d(TAG, "openSetup");
//
//        PackageManager manager = ChooserActivity.this.getPackageManager();
//        if (manager == null) {
//            Log.e(TAG, "Package manager is null");
//            return;
//        }
//
//        try {
//            Intent intent = manager.getLaunchIntentForPackage("com.accutime.clocksetup");
//            if (intent == null) {
//                Log.e(TAG, "Cannot open Clock Setup - null intent");
//                return;
//            }
//
//            stopLockTask();
//            intent.addCategory(Intent.CATEGORY_LAUNCHER);
//            context.startActivity(intent);
//
//        } catch (Exception e) {
//            Log.e(TAG, e.toString());
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//        Log.d(TAG, "onBackPressed");
//        stopLockTask();
//        finish();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        Log.d(TAG, "Chooser Activity onResume() started");
//
//        startLockTask();
//        startLoaders();
//
//        Log.d(TAG, "Chooser Activity onResume() finished");
//    }
//
//    /**
//     * Start loading loaders.
//     */
//    private void startLoaders() {
//        Log.i(TAG, "startLoaders");
//
//        // Start loading
//        loader.startLoading(new EmployeesLoadable(), null);
//        loader.startLoading(new DeviceDiscoverLoadable(), new Loader.Callback() {
//            @Override
//            public void onLoadError(Loader.Loadable loadable, IOException exception) {
//                Log.e(TAG, "Discover devices got error : ", exception);
//            }
//            @Override public void onLoadCanceled(Loader.Loadable loadable) {}
//            @Override public void onLoadCompleted(Loader.Loadable loadable) {}
//        });
//    }
//
//    @Override
//    public void performBadgeProcessing(int tag, String badge, Bundle extra) {
//
//    }
//
//    /**
//     * This method is declared in BaseActivity.
//     * When a READ_EMPLOYEE_FROM_DATABASE message is handled by the IncomingHandler,
//     * the data gets sent here to be processed. The data is packaged into a bundle.
//     */
//    public void  performEmployeeProcessing(final Bundle extra) {
//        Log.d(TAG, "performEmployeeProcessing");
//
//        // Get the employee from the bundle.
//        final Employee emp = extra.getParcelable("employee");
//
//        // Get the tag from the bundle.
//        final String tag = extra.getString("tag");
//
//        // If the employee's badge number does not mach a name, throw an error.
//        if (emp != null && TextUtils.isEmpty(emp.name)) {
//            Log.e(TAG, "Badge number not found");
//            displayMessageToScreen("Badge number '" + emp.badge + "' not found !");
//            return;
//        }
//
//        boolean fingerPrintFound = false;
//
//        // If the employee has fingerprints assigned to them...
//        if (emp != null && emp.fingerPrints != null && emp.fingerPrints.length > 0) {
//
//            Log.i(TAG, "Employee " + emp.name + " has " + emp.fingerPrints.length + " fingerprint(s).");
//
//            for (Employee.FingerPrint f : emp.fingerPrints) {
//
//                Log.d(TAG, "Badge number '" + emp.badge + "' has a fingerprint userId = " + f.userId);
//                if (f.userId != -1) {
//                    fingerPrintFound = true;
//                    break;
//                } else {
//                    Log.d(TAG, "User ID is -1");
//                }
//                if (!TextUtils.isEmpty(f.template)) {
//                    fingerPrintFound = true;
//                    break;
//                } else {
//                    Log.d(TAG, "Template is empty");
//                }
//            }
//        } else {
//            if (emp.fingerPrints == null)
//                Log.d(TAG, "This employee has null fingerprints");
//            else if (emp.fingerPrints.length <= 0)
//                Log.d(TAG, "This employee has no fingerprints");
//            else
//                Log.d(TAG, "Huh?");
//
//        }
//
//        if (fingerPrintFound) {
//            Log.d(TAG, "Fingerprint found");
//
//            fingerPromptDialog.show();
//            rootView.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (fingerPromptDialog.isShowing())
//                        fingerPromptDialog.dismiss();
//                }
//            }, 6000);
//
//            // Run the REST API to verify this fingerprint.
//            verifyAgainstTemplateList(tag, emp);
//
//        } else {
//            processBadgeEnter(tag, emp.badge, emp.name);
//        }
//    }
//
//    /**
//     * This method is declared in BaseActivity.
//     * It is called when the user enters a badge number and clicks OK (or enter).
//     */
////    @Override
//    public void performBadgeProcessing(final String tag, final String badge, Bundle extra) {
//        Log.d(TAG, "performBadgeProcessing");
//
//        // If no badge number was entered, stop here.
//        if (TextUtils.isEmpty(badge)) {
//            Log.i(TAG, "Badge field is empty.");
//            return;
//        }
//
//        // If no bundle was passed here, make one and add data to it.
//        if (extra == null) { extra = new Bundle(); }
//
//        // TODO : Should I put a name into this bundle?
//        extra.putString("badge", badge);
//        extra.putString("tag", tag);
//
//        try {
//            // Send message to read this employee from the database.
//            sendMessageToDatabaseService(Constants.ServiceActions.MSG_READ_EMPLOYEE_FROM_DATABASE.ordinal(), extra);
//        } catch (RemoteException e) { e.printStackTrace(); }
//    }
//
//    /**
//     * When a badge is entered, go here to process it.
//     */
//    private String processBadgeEnter(String tag, String badge, String name) {
//        Log.i(TAG, "processBadgeEnter");
//        Log.i(TAG, "Tag = " + tag);
//        Log.i(TAG, "Badge = " + badge);
//        Log.i(TAG, "Name = " + name);
//
//        Intent in;
//        if (tag.equals(ESS_TAG)) {
//            in = new Intent(ChooserActivity.this, EssActivity.class);
//            in.putExtra("badge", badge);
//            in.putExtra("name", name);
//            startActivity(in);
//            return null;
//        }
//
//        if (tag.equals(TIME_TRACKING_TAG)) {
//            in = new Intent(ChooserActivity.this, TimeTrackerActivity.class);
//            in.putExtra("badge", badge);
//            in.putExtra("name", name);
//            startActivity(in);
//            return null;
//        }
//
//        if (tag.equals(MANAGER_TAG)) {
//            String supervisorLevel = atsApplication.getEmployeeAttribute(badge, AtsApplication.badgeNodes, "supervisor_level");
//            if (TextUtils.isEmpty(supervisorLevel)) {
//                displayMessageToScreen("Badge number = " + badge + " not found !");
//                return null;
//            }
//            int sl = Integer.parseInt(supervisorLevel);
//            if (sl < 4) {
//                displayMessageToScreen(name + ", you are not a manager !");
//                return null;
//            }
//            in = new Intent(ChooserActivity.this, OldManagerActivity.class);
//            in.putExtra("badge", badge);
//            in.putExtra("name", name);
//            startActivity(in);
//            return null;
//        }
//        sendPunch(badge, name, tag);
//        return name;
//    }
//
//    /**
//     * Employee has punched the clock. Do something about it.
//     */
//    public void sendPunch(final String badge, final String name, final String tag) {
//        Log.d(TAG, "sendPunch");
//
//        String punchStringToShow = "";
//
//        // Badge has been processed and no errors have occurred.
//        Log.d(TAG, "Badge " + badge + " is found");
//
//        final AtsService.Punches p = new AtsService.Punches();
//        p.mode = "buffered";
//        p.terminal = "1439144";
//        p.tz = "America/New_York";
//        p.uid = UUID.randomUUID().toString();
////        p.punch = new AtsService.Punches.Punch();
////        if (tag.equals(CLOCK_IN_TAG)) {
////            p.punch.Action = "IN";
////            punchStringToShow = "Clock In";
////        }
////        if (tag.equals(CLOCK_OUT_TAG)) {
////            p.punch.Action = "OUT";
////            punchStringToShow = "Clock Out";
////        }
////        if (tag.equals(MEAL_START_TAG)) {
////            p.punch.Action = "MEAL";
////            punchStringToShow = "Meal Start";
////        }
////        if (tag.equals(MEAL_END_TAG)) {
////            p.punch.Action = "IN";
////            punchStringToShow = "Meal End";
////        }
////        if (tag.equals(BREAK_START_TAG)) {
////            p.punch.Action = "BREAK";
////            punchStringToShow = "Break Start";
////        }
////        if (tag.equals(BREAK_END_TAG)) {
////            p.punch.Action = "IN";
////            punchStringToShow = "Break End";
////        }
////        p.punch.Badge = badge;
////        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
////        p.punch.Timestamp = sdf.format(new Date());
//
//        // Send the punches and get a response.
//        Call<ResponseBody> sendPunch = atsService.sendPunch(p);
//        final String punchStr = punchStringToShow;
//        sendPunch.enqueue( new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                Log.d(TAG, "Punch is sent for badge #" + badge);
//                displayMessageToScreen("Sent punch action '"+ punchStr + "'  for " + name + " (badge #"+ badge + ")");
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.d(TAG, "Failed while sending punch for badge #"+badge);
//                displayMessageToScreen("Failed to send punch '"+ punchStr + "' for badge #"+ badge + "");
//            }
//        });
//    }
//
//    // Encode/Decode b64encoded strings - code lifted direct from Wikipedia
//    // with addd handling for added NUL byte
//    private String CODES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
//
//    /**
//     * Use REST API VerifyAgainstTemplateList to verify the given fingerprint.
//     */
//    private void verifyAgainstTemplateList(final String tag, final Employee employee) {
//        Log.i(TAG, "verifyAgainstTemplateList");
//
//        // If the employee has no fingerprints, return.
//        if (employee.fingerPrints.length <= 0) {
//            Log.i(TAG, "No fingerprints found for employee " + employee.name);
//            displayMessageToScreen("This employee has no fingerprints registered.");
//            processBadgeEnter(tag, employee.badge, employee.name);
//            return;
//        }
//
//        // Get all fingerprints for this employee.
//        byte[] templateArray = buildTemplateArray(employee);
//
//        // How many are there?
//        String countString = String.valueOf(employee.fingerPrints.length);
//        int count = Integer.valueOf(countString);
//        Log.i(TAG, employee.name + " has " + count + " fingerprints.");
//
//        // Packet to read from.
//        String b64packet = base64Encode( templateArray, ( count * 2352 ) );
//        logBigBuff(b64packet);
//
//        // Pass the count and packet to the Rest API.
//        deviceInterface.VerifyAgainstTemplateList(countString, b64packet).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                Log.i(TAG, "Response received");
//
//                Log.i(TAG, "Message: \t" + response.message()); // ok
//                Log.i(TAG, "Code   : \t" + response.code()); // 200
//                Log.i(TAG, "Success: \t" + response.isSuccessful() + ""); // true
//
//                String result = null;
//                try {
//
//                    if ( response.code() == 200 )
//                        result = processResponse(response.body().string());
//
//                    else {
//                        String error = response.errorBody().string();
//                        JSONObject jsonObject = new JSONObject(error);
//                        String errorCode = jsonObject.getString("errorCode");
//                        result = "Error [" + errorCode + "]";
//                        Log.i(TAG, "Error response: \t" + result);
//                        Log.e(TAG, "No response body available to be processed!");
//
//                        result = "Failure";
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    Log.e(TAG, "Error processing the response");
//                    displayMessageToScreen("Error processing the response");
//                }
//
//                if (result != null && !result.equals("Failure")) {
//
//                    // Response was successful. Go ahead and process the badge.
//                    Log.i(TAG, "Verification : Success");
//                    processBadgeEnter(tag, employee.badge, employee.name);
//
//                } else {
//                    // Don't do anything else.
//                    Log.i(TAG, "Verification : Failure");
//                    displayMessageToScreen("Fingerprint does not match");
//                }
//            }
//
//            private String processResponse(String response) {
//
//                Log.i(TAG, "processResponse");
//
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//
//                    String theBadge = jsonObject.getString("badge");
//                    String theScore = jsonObject.getString("score");
//
//                    String result = "Verify Found Badge [" + theBadge + "], Score [" + theScore + "]";
//                    Log.i(TAG, result);
//                    return result;
//
//                } catch (JSONException e) {
//                    Log.e(TAG, "Error processing the response");
//                    Toast.makeText(context, "Error processing the response", Toast.LENGTH_SHORT).show();
//                }
//
//                return "Failure";
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.e(TAG, "onFailure");
//                displayMessageToScreen("Scan Failed");
//            }
//        });
//    }
//
//    private byte[] base64Decode(String input) {
//        Log.i(TAG, "base64Decode gets input string of " + input.length() + " chars");
//        int len = input.length();
//        int last = (int) input.charAt(len-1);
//        if ( last < 33 ) {
//            len--;
//            Log.i(TAG, "Found a trailing non-printable char, only processing " + len + " chars");
//        }
//        if (len % 4 == 0)    {
//            byte decoded[] = new byte[((len * 3) / 4) - (input.indexOf('=') > 0 ? (len - input.indexOf('=')) : 0)];
//            char[] inChars = input.toCharArray();
//            Log.i(TAG, "B64 Decode from " + len + " to " + decoded.length + " bytes");
//            int j = 0;
//            int b[] = new int[4];
//            for (int i = 0; i < len; i += 4)     {
//                b[0] = CODES.indexOf(inChars[i]);
//                b[1] = CODES.indexOf(inChars[i + 1]);
//                b[2] = CODES.indexOf(inChars[i + 2]);
//                b[3] = CODES.indexOf(inChars[i + 3]);
//                decoded[j++] = (byte) ((b[0] << 2) | (b[1] >> 4));
//                if (b[2] < 64)      {
//                    decoded[j++] = (byte) ((b[1] << 4) | (b[2] >> 2));
//                    if (b[3] < 64)  {
//                        decoded[j++] = (byte) ((b[2] << 6) | b[3]);
//                    }
//                }
//            }
//            return decoded;
//        } else {
//            throw new IllegalArgumentException("Invalid base64 input");
//        }
//    }
//    private String base64Encode(byte[] in, int len) {
//        Log.i(TAG, "base64Encode gets to use " + len + " from an input array of " + in.length + " bytes");
//        StringBuilder out = new StringBuilder((len * 4) / 3);
//        int b;
//        for (int i = 0; i < len; i += 3)  {
//            b = (in[i] & 0xFC) >> 2;
//            out.append(CODES.charAt(b));
//            b = (in[i] & 0x03) << 4;
//            if (i + 1 < in.length)      {
//                b |= (in[i + 1] & 0xF0) >> 4;
//                out.append(CODES.charAt(b));
//                b = (in[i + 1] & 0x0F) << 2;
//                if (i + 2 < len)  {
//                    b |= (in[i + 2] & 0xC0) >> 6;
//                    out.append(CODES.charAt(b));
//                    b = in[i + 2] & 0x3F;
//                    out.append(CODES.charAt(b));
//                } else  {
//                    out.append(CODES.charAt(b));
//                    out.append('=');
//                }
//            } else      {
//                out.append(CODES.charAt(b));
//                out.append("==");
//            }
//        }
//
//        Log.i(TAG, "B64 Encode from " + len + " bytes to " + out.length() + " chars" );
//        return out.toString();
//    }
//    private void logBigBuff (String buf) {
//        int blen = 128;
//        if (buf.length() > blen) {
//            Log.i(TAG, buf.substring(0, blen));
//            logBigBuff(buf.substring(blen));
//        } else {
//            Log.i(TAG, buf);
//        }
//    }
//
//    /**
//     * Build a byte[] of templates to pass into the Rest API.
//     */
//    private byte[] buildTemplateArray(Employee emp) {
//        Log.e(TAG, "buildTemplateArray");
//
//        // Set up internal data
//        int numCapturedTemplates = 0;
//        byte[] templateArray  = new byte[ 10 * 2352 ];
//
//        // An employee has a bunch of fingerprints associated with them. Get them.
//        Employee.FingerPrint[] fingerPrints = emp.fingerPrints;
//
//        // Go through each fingerprint and build a template array.
//        for (Employee.FingerPrint fingerPrint : fingerPrints) {
//
//            // Get template
//            String templateString = fingerPrint.template;
//
//            // Decode to bytes
//            byte[] templateBytes = base64Decode(templateString);
//
//            // Where in memory to start writing
//            int s = ( numCapturedTemplates * 2352 );
//
//            try {
//                for (int i = 0; i < 2352; i++) {
//                    templateArray[s + i] = templateBytes[i];
//                }
//            } catch (ArrayIndexOutOfBoundsException ex) {
//                Log.e(TAG, ex.toString());
//            }
//
//            numCapturedTemplates++;
//        }
//
//        return templateArray;
//    }
//
//    /**
//     * Loadable to interact with employee data.
//     * This starts loading as soon as onResume is called.
//     */
//    private class EmployeesLoadable implements Loader.Loadable {
//
//        // TODO : Start in debug mode.
//        boolean DEBUG = true;
//
//        @Override public void cancelLoad() { }
//        @Override public boolean isLoadCanceled() {
//            return false;
//        }
//        @Override public void load() throws IOException, InterruptedException {
//            Log.d(TAG, "Downloading Employee list...");
//
//            // Invoke atsService.heartbeatRawCallback()
//            startHeartbeatCallback();
//
//            // Invoke atsService.getEmployeesRaw1() - if we don't already have them
//            // TODO
////            if (!PreferenceUtilities.Preferences.getHasEmployees(context)) {
//                getEmployeeList();
////            }
//        }
//
//        /**
//         * Get heartbeatRawCallback from Engine XML.
//         */
//        private void startHeartbeatCallback() {
//            Log.d(TAG, "startHeartbeatCallback : start time = " + getEmployeeMeasurement.start);
//
//            // Get start time to measure how long this takes.
//            heartbeatMeasurement.start();
//
//            // Get a heartbeat.
//            Call<TagNode> heartbeatCall = atsService.heartbeatRawCallback();
//            heartbeatCall.enqueue(new Callback<TagNode>() {
//                @Override
//                public void onResponse(Call<TagNode> call, Response<TagNode> response) {
//                    heartbeatMeasurement.end();
//                    Log.d(TAG, "Got heartbeat." +
//                            " Time = " + heartbeatMeasurement.end +
//                            ", Diff = " + heartbeatMeasurement.diff() +
//                            ", Avg = " + heartbeatMeasurement.avg);
//                }
//
//                @Override
//                public void onFailure(Call<TagNode> call, Throwable t) {
//                    heartbeatMeasurement.end();
//                    Log.e(TAG, "Failed to get heartbeat." +
//                            " Time = " + heartbeatMeasurement.end +
//                            ", Diff = " + heartbeatMeasurement.diff() +
//                            ", Avg = " + heartbeatMeasurement.avg);
//                    Log.e(TAG, "Error = ", t);
//                }
//            });
//        }
//
//        /**
//         * Get employees from Engine XML.
//         */
//        private void getEmployeeList() {
//            Log.d(TAG, "getEmployeeList : start time = " + getEmployeeMeasurement.start);
//
//            getEmployeeMeasurement.start();
//
//            Call<TagNode> employeeListCall = atsService.getEmployeesRaw1();
//            employeeListCall.enqueue (new Callback<TagNode>() {
//                @Override
//                public void onResponse(Call<TagNode> call, final Response<TagNode> response) {
//                    getEmployeeMeasurement.end();
//                    Log.d(TAG, "Got employee list. Times = " +
//                            getEmployeeMeasurement.start + " - " + getEmployeeMeasurement.end +
//                            ", Diff = " + getEmployeeMeasurement.diff() +
//                            ", Avg =" + getEmployeeMeasurement.avg);
//
//                    // Start a thread to store employee list.
//                    Runnable runnable = new StoreEmployeesRunnable(response);
//                    new Thread(runnable).start();
//                }
//
//                //public void failure(RetrofitError error) {
//                @Override
//                public void onFailure(Call<TagNode> call, Throwable t) {
//                    Log.e(TAG, "Failed getting employee list." +
//                            " Finish time = " + getEmployeeMeasurement.end +
//                            ", Diff = " + getEmployeeMeasurement.diff() +
//                            ", Avg = " + getEmployeeMeasurement.avg +
//                            ", Error = ", t);
//
//                    PreferenceUtilities.Preferences.setHasEmployees(context, false);
//                }
//            });
//        }
//
//        /**
//         * This class starts a new thread to get/add employee data.
//         */
//        class StoreEmployeesRunnable implements Runnable {
//
//            private final Response<TagNode> response;
//
//            StoreEmployeesRunnable(Response<TagNode> response) {
//                this.response = response;
//            }
//
//            @Override
//            public void run() {
//
//                // Start measuring time again.
//                storeEmployeeMeasurement.start();
//
//                XmlUtils.ValueTuple rootNode = null;
//                try {
//
//                    // We're in debug mode.
//                    if (DEBUG) {
//                        Log.d(TAG, "In Debug mode");
//
//                        TreeBuilder builder = new TreeBuilder();
//                        StringReader stringReader = new StringReader(testEmployees);
//                        TreeNode localEmployeesNode = builder.parseXML(stringReader);
//
//                        if (localEmployeesNode instanceof TagNode) {
//                            Log.d(TAG, "localEmployeesNode is instanceof TagNode");
//
//                            // Store employee list and record time.
//                            rootNode = atsApplication.storeObject(
//                                    (TagNode) localEmployeesNode, ""
//                            );
//
//                            rootNode.commitRelation(atsApplication.getDbRelations());
//                            atsApplication.storeRootNodeName(rootNode);
//                            storeEmployeeMeasurement.end();
//                            Log.d(TAG, "Stored employee list. Time taken : " +
//                                    storeEmployeeMeasurement.show()
//                            );
//
//                            TreeNode tnode = XmlUtils.retrieveObject(rootNode.nodeId,
//                                    atsApplication.getDbIdToPath(),
//                                    atsApplication.getDbPathToId(),
//                                    atsApplication.getDbRelations()
//                            );
//
//                            if (tnode != null) {
//                                TreeToXML txml = new TreeToXML(tnode);
//                                Util.Log.d(TAG, "Back to xml: " + txml);
//                            }
//                        }
//                    } //end debug
//
//
//                    // Not in debug mode.
//                    else {
//                        // Store employee list and record time.
//                        rootNode = atsApplication.storeObject(response.body(), "");
//                        rootNode.commitRelation(atsApplication.getDbRelations());
//                        atsApplication.storeRootNodeName(rootNode);
//                        storeEmployeeMeasurement.end();
//                        Log.d(TAG, "Stored employee list. " + storeEmployeeMeasurement.show());
//                    }
//
//
//                    // Right now this is always true.
//                    if (ADD_LOCAL_EMPLOYEES) {
//
//                        // Store local employees.
//                        for (int i = 1; i < localEmployees.length; i++) { // TODO: Was <=
//                            StringReader thisEmployee = new StringReader(localEmployees[i]);
//                            TreeNode localEmp = new TreeBuilder().parseXML(thisEmployee);
//
//                            if (localEmp instanceof TagNode) {
//                                XmlUtils.ValueTuple newEmp = atsApplication.storeObject(
//                                        (TagNode) localEmp,
//                                        "/Employees"
//                                );
//                                XmlUtils.ValueTuple parent = XmlUtils.addChild(
//                                        rootNode,
//                                        newEmp,
//                                        atsApplication.getDbRelations()
//                                );
//                            }
//                        }
//
//                        if (DEBUG) {
//
//                            // Print all keys.
//                            String keyslog = XmlUtils.printAllKeys(
//                                    atsApplication.getDbRelations(),
//                                    atsApplication.getDbIdToPath()
//                            );
//                            Log.d(TAG, "Keys Log: \n" + keyslog);
//
//                            // Root node should be employee list?
//                            TreeNode tnode = null;
//                            if (rootNode != null) {
//                                tnode = XmlUtils.retrieveObject(
//                                        rootNode.nodeId,
//                                        atsApplication.getDbIdToPath(),
//                                        atsApplication.getDbPathToId(),
//                                        atsApplication.getDbRelations()
//                                );
//                            }
//
//                            if (tnode != null) {
//                                TreeToXML txml = new TreeToXML(tnode);
//                                txmlString = txml.toString();
//                                Util.Log.d(TAG, "Back to xml:\n" + txmlString);
//
//                            }
//                        }
//                    }
//
//                    AtsApplication.badgeNodes = atsApplication.getKeyValuePairsForNode(
//                            "/Employees/Employee/badge"
//                    );
//
//                    // Success
//                    PreferenceUtilities.Preferences.setHasEmployees(context, true);
//
//                    // Print all keys.
//                    if (DEBUG) {
//                        String str = XmlUtils.printAllKeys(
//                                atsApplication.getDbRelations(),
//                                atsApplication.getDbIdToPath()
//                        );
//                        Log.d("Print All Keys:\n", str);
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    storeEmployeeMeasurement.end();
//                    Log.d(TAG, "Got exception while storing employee list. Finish time = " + storeEmployeeMeasurement.end +
//                            ", Diff = " + storeEmployeeMeasurement.diff() +
//                            ", Avg = " + storeEmployeeMeasurement.avg);
//                }
//            }
//        }
//    }
//
//    /**
//     * Looks for fingerprint scanners, and sets them to the atsApplication.
//     */
//    public class DeviceDiscoverLoadable implements Loader.Loadable {
//
//        private final String TAG = DeviceDiscoverLoadable.class.getSimpleName();
//
//        private Callback<DeviceInterface.DiscoverResponse> discoverDevicesCallback =
//                new Callback<DeviceInterface.DiscoverResponse>() {
//
//            @Override
//            public void onResponse(Call<DeviceInterface.DiscoverResponse> call,
//                    Response<DeviceInterface.DiscoverResponse> response) {
//
//                Log.i(TAG, "Got a response: " + response.message());
//
//                atsApplication.setDiscoverConfig(response.body());
//                DeviceInterface.DiscoverResponse.Device[] devices =
//                        response.body().devices;
//
//                Log.i(TAG, "Number of devices: " + response.body().devices.length);
//
//                for (DeviceInterface.DiscoverResponse.Device d : devices) {
//                    Log.d(TAG, "Found device = " + d.device);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<DeviceInterface.DiscoverResponse> call,
//                                  Throwable t) {
//                Log.e(TAG, "Discover devices got an error. Throwable = ", t);
//            }
//        };
//
//        @Override public void cancelLoad() { }
//        @Override public boolean isLoadCanceled() { return false; }
//        @Override public void load() throws IOException, InterruptedException {
//            Log.i(TAG, "Connecting to Discover service");
//
//            Log.d(TAG, "Fingerprint Service : " + deviceInterface.toString());
//            Log.d(TAG, "Discover Devices Callback : " + discoverDevicesCallback.toString());
//
//
//            deviceInterface.discoverDevices().enqueue(discoverDevicesCallback);
//        }
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
////    /**
////     * This is called when a fingerprint is found and
////     * atsApplication.getLumidigmConfig() is not null. A lumidigm reader has been found.
////     * Now deal with the response.
////     */
////    private void lumidigmVerify(final int tag, final Employee emp) {
////        final FingerprintLumidigmScanLoadable fLodable = new FingerprintLumidigmScanLoadable();
////        final Loader.Callback fLoaderCallback = new Loader.Callback() {
////            @Override public void onLoadError(Loader.Loadable loadable, IOException exception) { }
////            @Override public void onLoadCanceled(Loader.Loadable loadable) { }
////            @Override public void onLoadCompleted(Loader.Loadable loadable) {
////                fingerPromptDialog.dismiss();
////                fingerPromptDialog.setMessage("Place Your Finger on the Finger Print Reader");
////                FingerprintLumidigmScanLoadable fl = (FingerprintLumidigmScanLoadable) loadable;
////                Log.d(TAG, "Device = " + fl.response.device + ", Status = " + fl.response.status);
////
////                Log.d(TAG, "Fingerprint Count = " + emp.fingerPrints.length);
////                ArrayList<String> templates = new ArrayList<>();
////                for(Employee.FingerPrint fingerPrint : emp.fingerPrints)
////                    templates.add(fingerPrint.template);
////
////                final FingerprintLumidigmVerifyLoadable fLodable1 = new FingerprintLumidigmVerifyLoadable(templates, fl.template);
////                Loader.Callback fLoaderCallback1 = new Loader.Callback() {
////                    @Override public void onLoadError(Loader.Loadable loadable, IOException exception) { }
////                    @Override public void onLoadCanceled(Loader.Loadable loadable) { }
////                    @Override public void onLoadCompleted(Loader.Loadable loadable) {
////                        FingerprintLumidigmVerifyLoadable fl = (FingerprintLumidigmVerifyLoadable) loadable;
////                        Log.d(TAG, "Device = " + fl.response.device);
////                        if (fl.maxMatchScore > 30000) {
////                            if (fl.maxMatchScore > 100000) { fl.maxMatchScore=100000; }
////                            displayToast("Fingerprints match with match score = " + fl.maxMatchScore/1000 + "%");
////                            processBadgeEnter(tag, emp.badge, emp.name);
////                        } else {
////                            displayToast("FingerPrint didn't match. Match score = " + fl.maxMatchScore/1000 + "%");
////                        }
////                    }
////                };
////                loader.startLoading(fLodable1, fLoaderCallback1);
////            }
////        };
////        loader.startLoading(fLodable, fLoaderCallback);
////    }
////
////    /**
////     * This is called when a fingerprint is found and
////     * atsApplication.getSupremaConfig() is not null. A suprema reader has been found.
////     * Now deal with the response.
////     */
////    private void supremaIdentify_new(final int tag, final Employee emp) {
////        deviceInterface.supremaIdentify().enqueue(new Callback<ResponseBody>() {
////            @Override
////            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
////                Log.i(TAG, "Got a response for suprema.");
////                boolean matchFound = false;
////                if (fingerPromptDialog.isShowing())
////                    fingerPromptDialog.dismiss();
////                DeviceInterface.SupremaResponseIdentify supremaResponse=null;
////                if (response != null) {
////                    try {
////                        //String responseStr = StringUtilities.convertStreamToString(response.body().byteStream());
////                        String responseStr;
////                        if (response.code() >= 200 && response.code() < 300) {
////                            responseStr = response.body().string();
////                        }
////                        else {
////                            responseStr = response.errorBody().string();
////                        }
////                        // TODO: THIISS
////                        Log.d(TAG, "response.code=" + response.code());
////                        Log.d(TAG, "responseStr=" + responseStr);
////                        Gson gson = new Gson();
////                        supremaResponse = gson.fromJson(responseStr, DeviceInterface.SupremaResponseIdentify.class);
////                        Log.d(TAG, "supremaResponse=" + supremaResponse);
////                    } catch (IOException e) {
////                        e.printStackTrace();
////                    }
////                } else {
////                    displayToast("Timed out waiting for terminal");
////                }
////
////                if (supremaResponse != null) {
////                    if (supremaResponse.packets != null && supremaResponse.packets.length > 0)
////                        Log.d(TAG, "response.packets[0]=" + supremaResponse.packets[0]);
////                    if (supremaResponse.packets != null && supremaResponse.packets.length > 1) {
////                        Log.d(TAG, "response.packets[1]=" + supremaResponse.packets[1]);
////                        String bytes[] = supremaResponse.packets[1].split(" ");
////                        int fingerprintUserId = Integer.parseInt(bytes[2], 16);
////                        Log.d(TAG, "fingerprintUserId=" + fingerprintUserId);
////                    }
////                    if (!TextUtils.isEmpty(supremaResponse.errorCode) && !supremaResponse.errorCode.equalsIgnoreCase("0")) {
////                        Log.d(TAG, "response.errorCode=" + supremaResponse.errorCode);
////                        Log.d(TAG, "response.errorString=" + supremaResponse.errorString);
////                        displayToast(supremaResponse.errorString);
////                    }
////                    Log.d(TAG, "fingerprintUserId=" + supremaResponse.enrolledUserId);
////                }
////
////                if ((response.code() >=200 && response.code()<300) &&
////                        !TextUtils.isEmpty(supremaResponse.enrolledUserId)) {
////                    for(Employee.FingerPrint fingerPrint: emp.fingerPrints) {
////                        int userId = Integer.parseInt(supremaResponse.enrolledUserId);
////                        Log.d(TAG, "scanned on device=" + supremaResponse.device);
////                        Log.d(TAG, "stored fingerprint device=" + fingerPrint.device);
////                        Log.d(TAG, "scanned userID=" + supremaResponse.enrolledUserId);
////                        Log.d(TAG, "stored fingerprint userId=" + fingerPrint.userId);
////                        if (userId == fingerPrint.userId) {
////                            processBadgeEnter(tag, emp.badge, emp.name);
////                            matchFound = true;
////                        }
////                    }
////                    if (!matchFound) {
////                        displayToast("FingerPrint didnt match");
////                    } else {
////                        displayToast("FingerPrints match");
////                    }
////                }
////            }
////
////            @Override
////            public void onFailure(Call<ResponseBody> call, Throwable t) {
////                Log.d(TAG, "response.errored. throwable=" + t);
////            }
////        });
////    }
////
////
////    /////////////////////////////////////////////
////
////
////
////    /// These aren't used... ///////
////    FingerPrintSupremaIdentifyLoadable sLodable = new FingerPrintSupremaIdentifyLoadable();
////    private void supremaIdentify_old(Loader.Callback sLoaderCallback) {
////        loader.startLoading(sLodable, sLoaderCallback);
////    }
////
////private class FingerPrintSupremaIdentifyLoadable implements Loader.Loadable {
////    public Response<ResponseBody> response;
////    public DeviceInterface.SupremaResponseIdentify supremaResponse=null;
////    public boolean DEBUG = true;
////
////    @Override
////    public void cancelLoad() {
////    }
////
////    @Override
////    public boolean isLoadCanceled() {
////        return false;
////    }
////
////    // TODO : NOT THIS...
////    @Override
////    public void load() throws IOException, InterruptedException {
////        Log.d(TAG, "Connecting to Suprema FingerPrint service");
////        response = deviceInterface.supremaIdentify().execute();
////        if (response != null) {
////            try {
////                //String responseStr = StringUtilities.convertStreamToString(response.body().byteStream());
////                String responseStr;
////                if (response.code() >= 200 && response.code() < 300) {
////                    responseStr = response.body().string();
////                }
////                else {
////                    responseStr = response.errorBody().string();
////                }
////                Log.d(TAG, "response=" + response);
////                Log.d(TAG, "responseStr=" + responseStr);
////                Gson gson = new Gson();
////                supremaResponse = gson.fromJson(responseStr, DeviceInterface.SupremaResponseIdentify.class);
////                Log.d(TAG, "supremaResponse=" + supremaResponse);
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
////        } else {
////            displayToast("Timed out waiting for terminal");
////        }
////
////        if (supremaResponse != null) {
////            if (supremaResponse.packets != null && supremaResponse.packets.length > 0)
////                Log.d(TAG, "response.packets[0]=" + supremaResponse.packets[0]);
////            if (supremaResponse.packets != null && supremaResponse.packets.length > 1) {
////                Log.d(TAG, "response.packets[1]=" + supremaResponse.packets[1]);
////                String bytes[] = supremaResponse.packets[1].split(" ");
////                Log.d(TAG, "fingerprintId .packets[1]=" + supremaResponse.packets[1]);
////            }
////            if (!TextUtils.isEmpty(supremaResponse.errorCode) && !supremaResponse.errorCode.equalsIgnoreCase("0")) {
////                Log.d(TAG, "response.errorCode=" + supremaResponse.errorCode);
////                Log.d(TAG, "response.errorString=" + supremaResponse.errorString);
////                displayToast(supremaResponse.errorString);
////            }
////        }
////    }
////}
////
////
////
////
////
////    private void getBadge(String name, boolean in) {
////        String[] parts = name.split(" ");
////        if (foundDialog == null) {
////            foundDialog = new AlertDialog.Builder(this).create();
////            foundDialog.setCanceledOnTouchOutside(true);
////        }
////        if (in) {
////            foundDialog.setMessage("Hi " + parts[0] + "\nWelcome to AccuTime Systems");
////        } else {
////            foundDialog.setMessage("Bye " + parts[0] + "\nSee you tomorrow");
////        }
////        if (foundDialog.isShowing()) {
////            foundDialog.dismiss();
////        }
////        foundDialog.show();
////
////        new Handler().postDelayed(new Runnable() {
////            public void run() {
////                if (foundDialog != null && foundDialog.isShowing())
////                    foundDialog.dismiss();
////            }
////        }, 9000);
////    }
////
////    // This isn't used...
////    ArrayList<String> getFingerprintId(String badge) {
////        ArrayList<String> fingerprintUserIds = new ArrayList<>();
////        try {
////            XmlUtils.ValueTuple empVt = atsApplication.getEmployee(badge, AtsApplication.badgeNodes);
////            TagNode employeeNode = (TagNode) XmlUtils.retrieveObject(empVt.nodeId, atsApplication.getDbIdToPath(),
////                    atsApplication.getDbPathToId(),
////                    atsApplication.getDbRelations());
////
////            TreeToXML txml = new TreeToXML(employeeNode);
////            Util.Log.d(TAG, "employeenode to xml=\n" + txml);
////            Collection<NamedEntity> namedEntities = employeeNode.findChildren("FingerPrint");
////            for(NamedEntity n: namedEntities) {
////                if (n instanceof TagNode) {
////                    Attribute a = ((TagNode) n).findAttribute("userId");
////                    if (a != null)
////                        fingerprintUserIds.add(a.getValue());
////                }
////            }
////        } catch (UnsupportedEncodingException e) {
////            e.printStackTrace();
////        }
////        return fingerprintUserIds;
////    }