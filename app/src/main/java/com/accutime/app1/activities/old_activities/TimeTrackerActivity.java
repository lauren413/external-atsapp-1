//package com.accutime.app1.activities;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.view.MenuItem;
//import android.widget.TextView;
//
//import com.accutime.app1.Application.AtsApplication;
//import com.accutime.app1.R;
//import com.accutime.app1.xmlparser.Attribute;
//import com.accutime.app1.xmlparser.NamedEntity;
//import com.accutime.app1.xmlparser.TagNode;
//import com.accutime.app1.xmlparser.TextNode;
//import com.accutime.app1.xmlparser.TreeNode;
//import com.accutime.app1.xmlparser.XmlUtils;
//import com.snappydb.SnappydbException;
//
//import java.io.UnsupportedEncodingException;
//import java.util.Map;
//
///**
// * An example full-screen activity that shows and hides the system UI (i.e.
// * status bar and navigation/system bar) with user interaction.
// */
//public class TimeTrackerActivity extends Activity {
//
//    AtsApplication atsApplication = (AtsApplication) getApplication();
//    Map<String, byte[]> badgeNodes;
//    String [] badgeValues;
//
//    TextView welcome;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_time_tracker);
//        atsApplication = (AtsApplication) getApplication();
//        welcome = (TextView) findViewById(R.id.tv_label);
//        badgeNodes = atsApplication.getKeyValuePairsForNode("/Employees/Employee/badge");
//        badgeValues = new String[badgeNodes.keySet().size()];
//        badgeNodes.keySet().toArray(badgeValues);
//        final String badge = getIntent().getStringExtra("badge");
//        String name = getIntent().getStringExtra("name");
//        name = getEmployeeName(badge);
//        if (name != null) {
//            welcome.setText("Welcome\n" + name);
//        }
//    }
//
//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == android.R.id.home) {
//            // This ID represents the Home or Up button.
//            //NavUtils.navigateUpFromSameTask(this);
//            //TODO
//            onBackPressed();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    public String getEmployeeName(String badge) {
//        if (!TextUtils.isEmpty(badge)) {
//            byte[] nodeId = badgeNodes.get(badge);
//            XmlUtils.ValueTuple employeeVt = null;
//            TreeNode employeeNode = null;
//
//            if (nodeId != null) {
//                try {
//                    employeeVt = XmlUtils.getParent(nodeId, atsApplication.getDbRelations());
//                    employeeNode = XmlUtils.retrieveObject(employeeVt.nodeId, atsApplication.getDbIdToPath(), atsApplication.getDbPathToId(), atsApplication.getDbRelations());
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                } catch (SnappydbException e) {
//                    e.printStackTrace();
//                }
//                if (employeeVt == null || employeeNode == null)
//                    return null;
//
//                if (!(employeeNode instanceof TagNode)) {
//                    return null;
//                }
//                final TagNode employee = (TagNode) employeeNode;
//                // find the name of the badge and show a welcome message
//
//                Attribute a = employee.findAttribute("name");
//                if (a != null)
//                    return (a.getValue());
//                else {
//                    NamedEntity n = employee.findChild("name");
//                    if (n != null) {
//                        TreeNode nc = n.getChild();
//                        if (nc instanceof TextNode) {
//                            return (((TextNode) nc).getText());
//                        }
//                    }
//                }
//            }
//        }
//        return null;
//    }
//
//    public  void updateWelcome(String name, String badge) {
//        String[] parts = name.split(" ");
//        Intent intent = new Intent(this, EssActivity.class);
//        intent.putExtra("name", name);
//        intent.putExtra("badge", badge);
//        startActivity(intent);
//    }
//}
