//package com.accutime.app1.activities;
//
///**
// * Created by mkemp on 2/26/18.
// */
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Context;
//import android.os.Bundle;
//
//import com.accutime.app1.R;
//import com.accutime.app1.businessObjects.Employee;
//
//// TODO : Clean this up
//// TODO : Make a new UML
//// TODO : This class isn't actually needed. This is a menu state.
//// Instead, add fingerprint should be its own state.
////public class ManagerActivity extends BaseActivity implements View.OnClickListener {
//public class ManagerActivity extends Activity {
//
//    public final String TAG = this.getClass().getSimpleName();
//    private Context context = this;
//    private Employee employee;
//    private AlertDialog fingerprintDialog;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.manager_portal);

//        initializeEmployee();
//
//        // Setup views
//        final TextView greeting = (TextView) findViewById(R.id.greeting);
//        findViewById(R.id.enroll).setOnClickListener(this);
//        findViewById(R.id.addFinger).setOnClickListener(this);
//        final String badge = getIntent().getStringExtra("badge");
//        String name = getIntent().getStringExtra("name");
//
//        if (name != null) {
//            final String[] parts = name.split(" ");
//            String welcome = "Welcome " + parts[0];
//            greeting.setText(welcome);
//        }
//    }
//
//    private void initializeEmployee() {
//        employee = getIntent().getParcelableExtra(getString(R.string.key_employee));
//
//        String welcomeMessage = "Welcome, \n" + employee.name;
//        TextView tvWelcome = (TextView) findViewById(R.id.greeting);
//        tvWelcome.setText(welcomeMessage);
//    }
//
//    @Override
//    public void onClick(View v) {
//
//        switch (v.getId()) {
//            // TODO : This will eventually be replaced with keys and be told what to do by the .conf file.
//            case R.id.enroll:
//                addFingerprint();
//                break;
//
//            case R.id.addFinger:
//                addFingerprint();
//                break;
//
//            case R.id.logout:
//                onBackPressed();
//
//            default:
//                break;
//        }
//
//    }
//
//    /**
//     * Add new fingerprint to employee with given badge number.
//     * TODO : What to do here should depend on the action.
//     */
//    public void addFingerprint() {
//        LayoutInflater layoutInflater = LayoutInflater.from(context);
//        View view = layoutInflater.inflate(R.layout.dialog_badge_number, null);
//        fingerprintDialog = new AlertDialog.Builder(context).create();
//        fingerprintDialog.setTitle(getString(R.string.new_fingerprint));
//        fingerprintDialog.setMessage(getString(R.string.enter_employee_badge_number));
//        fingerprintDialog.setCancelable(false);
//
//        if (fingerprintDialog != null && fingerprintDialog.isShowing()) {
//            fingerprintDialog.dismiss();
//        }
//
//        final EditText etBadgeNumber = (EditText) view.findViewById(R.id.et_badge_number);
//
//        fingerprintDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                performBadgeProcessing(etBadgeNumber.getText().toString(), null);
//            }
//        });
//
//        fingerprintDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                fingerprintDialog.dismiss();
//            }
//        });
//
//        fingerprintDialog.setView(view);
//        fingerprintDialog.show();
//
//        TextView textView = (TextView) fingerprintDialog.findViewById(android.R.id.message);
//        textView.setTextSize(context.getResources().getDimension(R.dimen.large_text));
//        textView.setScroller(new Scroller(context));
//        textView.setVerticalScrollBarEnabled(true);
//        textView.setMovementMethod(new ScrollingMovementMethod());
//    }
//
//    /**
//     * Database incoming handler from base activity points here.
//     * Start a fingerprint capture scan for the given employee.
//     */
//    public void performEmployeeProcessing(Bundle extra) {
//        Log.d(TAG, "performEmployeeProcessing: " + extra.toString());
//
//        Employee employee = extra.getParcelable("employee");
//        if (employee != null && TextUtils.isEmpty(employee.name)) {
//            Util.displayToast(context, "Badge number '" + employee.badge + "' not found!");
//        }
//
//        else if (employee != null) {
//            biometricObject.startCaptureScan(employee.badge, atsApplication);
//        }
//    }
//}