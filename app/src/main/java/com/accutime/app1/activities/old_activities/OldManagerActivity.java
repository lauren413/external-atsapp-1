//package com.accutime.app1.activities.old_activities;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.RemoteException;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.accutime.app1.R;
//import com.accutime.app1.businessObjects.Employee;
//import com.accutime.app1.common.Constants;
//import com.accutime.app1.retrofit.DeviceInterface;
//import com.accutime.app1.retrofit.RestInterface;
//import com.accutime.app1.retrofit.GsonConverterFactory;
//import com.accutime.app1.retrofit.ScalarsConverterFactory;
//import com.accutime.app1.util.Util;
//import com.accutime.app1.xmlparser.Attribute;
//import com.accutime.app1.xmlparser.AttributeList;
//import com.accutime.app1.xmlparser.TagNode;
//import com.accutime.app1.xmlparser.XmlUtils;
//import com.google.gson.Gson;
//import com.snappydb.SnappydbException;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.util.concurrent.TimeUnit;
//
//import okhttp3.OkHttpClient;
//import okhttp3.ResponseBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//
//
//public class OldManagerActivity extends OldBaseActivity
//        implements View.OnClickListener {
//
//    public final String TAG = this.getClass().getSimpleName();
//
//    Context context = this;
//    AlertDialog fingerPrompt;
//
//    public static DeviceInterface fingerprintService = null;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//
//        // Set screen
//        final LayoutInflater inflater = getLayoutInflater();
//        rootView = (RelativeLayout) inflater.inflate(R.layout.manager_portal, null);
//        setContentView(rootView);
//        super.onCreate(savedInstanceState);
//
//        // Setup views
//        final TextView greeting = (TextView) findViewById(R.id.greeting);
//        findViewById(R.id.enroll).setOnClickListener(this);
//        findViewById(R.id.addFinger).setOnClickListener(this);
//        final String badge = getIntent().getStringExtra("badge");
//        String name = getIntent().getStringExtra("name");
//
//        if (name != null) {
//            final String[] parts = name.split(" ");
//            String welcome = "Welcome " + parts[0];
//            greeting.setText(welcome);
//        }
//
//        // Setup prompt
//        fingerPrompt = new AlertDialog.Builder(this).setTitle("").create();
//        fingerPrompt.setMessage("Place Your Finger on the Finger Print Reader");
//        fingerPrompt.setCancelable(false);
//        final TextView logout = (TextView) findViewById(R.id.logout);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                OldManagerActivity.this.finish();
//            }
//        });
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        createFingerprintService();
//    }
//
//    /**
//     * Creates a fingerprint service using OkHttpClient and Retrofit.
//     */
//    public void createFingerprintService() {
//        if (fingerprintService == null) {
//            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                    .readTimeout(15000, TimeUnit.MILLISECONDS)
//                    .writeTimeout(15000, TimeUnit.MILLISECONDS)
//                    .build();
//            Retrofit fingerprintAdapter = new Retrofit.Builder()
//                    .baseUrl(Constants.getDeviceIpAddress())
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(okHttpClient)
//                    .build();
//
//            Log.d(TAG, "creating deviceInterface");
//            fingerprintService = fingerprintAdapter.create(DeviceInterface.class);
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//
//        // Fist dismiss and dialogs currently open.
//        if (badgeDialog.isShowing()) {
//            badgeEmployeeView.setText(" ");
//            badgeDialog.dismiss();
//        }
//
//        String forEmployee = "for the employee whose finger is being scanned";
//
//        // Then do something based on the view tapped.
//        switch(v.getId()) {
//            case R.id.enroll:
//                badgeEmployeeView.setText(forEmployee);
//                badgeDialogView.setTag(ENROLL_TAG);
//                badgeDialog.show();
//                break;
//            case R.id.addFinger:
//                badgeEmployeeView.setText(forEmployee);
//                badgeDialogView.setTag(ADD_FINGER_TAG);
//                badgeDialog.show();
//                break;
//            default:
//                break;
//        }
//
//    }
//
//    /**
//     * This overrides BaseActivity's abstract method.
//     * It gets called when the user clicks OK on the badge dialog.
//     * If the badge file is not empty, tell the database service to get employee info.
//     */
//    public void performBadgeProcessing(final int tag, final String badge, Bundle extra) {
//        Log.d(TAG, "performBadgeProcessing");
//
//        if (TextUtils.isEmpty(badge)) { Log.d(TAG, "Empty badge"); }
//
//        else {
//            if (extra == null) { extra = new Bundle(); }
//            extra.putString("badge", badge);
//            extra.putInt("tag", tag);
//            try {
//
//                // Send message to database service to get employee info.
//                sendMessageToDatabaseService(Constants.ServiceActions.MSG_READ_EMPLOYEE_FROM_DATABASE.ordinal(), extra);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * This overrides BaseActivity's abstract method.
//     * It gets called when the service receives a readEmployeeAction.
//     */
//    public void performEmployeeProcessing(final Bundle extra) {
//        Log.d(TAG, "performEmployeeProcessing");
//        String tag = extra.getString("tag");
//        String badge = extra.getString("badge");
//        Employee emp = extra.getParcelable("employee");
//        String name = null;
//        if (emp != null) {
//            Log.i(TAG, emp.badge);
//            name = performManagerBadgeProcessing(tag, badge,  emp.name);
//        }
//        Log.d(TAG, "Tag = " + tag);
//        Log.d(TAG, "Badge = " + badge);
//        Log.d(TAG, "Name = " + name);
//    }
//
//    /**
//     * Called by performBadgeProcessing()
//     * This method performs badge processing locally.
//     * Namely, for enrolling and adding a fingerprint.
//     */
//    public String performManagerBadgeProcessing(String tag, String badge, String name) {
//        if (TextUtils.isEmpty(badge)) {
//            Log.i(TAG, "Badge is empty");
//            return null;
//        }
//        else {
//            if (tag.equals(ENROLL_TAG) || tag.equals(ADD_FINGER_TAG)) {
//                if (!TextUtils.isEmpty(name)) {
//                    fingerPrompt.show();
//                    fingerPrompt.setMessage("Adding Finger for " + name +". " +
//                            "Place your finger on the Finger Print Reader \n\n" +
//                            "Hold it there for a few seconds...");
//                    rootView.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            fingerPrompt.dismiss();
//                            fingerPrompt.setMessage("Place your finger on the Finger Print Reader. \n\n" +
//                                    "Hold it there for a few seconds...");
//                        }
//                    }, 10000);
//
//                    // Use new Capture Rest API (2017)
//                    startCaptureScan(badge);
//                }
//                else { Log.d(TAG, "We got the enroll/add finger tag but... name is empty"); }
//                return name;
//            }
//            else { Log.d(TAG, "Not enrolling"); }
//            return null;
//        }
//    }
//
//    /**
//     * Use the Rest API CaptureTemplate() to
//     * Scan the user's fingerprint using the fingerprint sensor.
//     */
//    private void startCaptureScan(final String badgeNumber) {
//        Log.i(TAG, "Starting capture scan.");
//
//        // Pass the badge number into the Rest Api Capture Template
//        fingerprintService.CaptureTemplate(badgeNumber).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                Log.i(TAG, "Response received");
//
//                Log.i(TAG, "Badge #: \t" + badgeNumber); // 1234
//                Log.i(TAG, "Message: \t" + response.message()); // ok
//                Log.i(TAG, "Code   : \t" + response.code()); // 200
//                Log.i(TAG, "Success: \t" + response.isSuccessful() + ""); // true
//                Log.i(TAG, "Raw response: \t" + response.raw().toString());
//
//                String result = null;
//                try {
//                    if ( response.code() == 200 ) {
//                        result = processResponse(response.body().string());
//                    }
//                    else {
//                        String error = response.errorBody().string();
//                        JSONObject jsonObject = new JSONObject(error);
//                        String errorCode = jsonObject.getString("errorCode");
//                        result = "Error [" + errorCode + "]";
//                        Log.i(TAG, "Error response: \t" + result);
//                        Log.e(TAG, "No response body available to be processed!");
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    Log.e(TAG, "Error processing the response");
//                    displayMessageToScreen("Error processing the response");
//                }
//
//                if ((result != null && !result.equals("Failure"))) {
//                    Log.d(TAG, "Adding badge " + badgeNumber);
//                    atsApplication.addTemplateToUser(badgeNumber, result);
//                }
//
//                else {
//                    Log.i(TAG, "Fingerprint scan failed");
//                    displayMessageToScreen("Scan failed");
//                }
//            }
//
//            /**
//             * Process the response body string.
//             * @return the fingerprint packet as a string.
//             */
//            private String processResponse(String response) {
//
//                Log.i(TAG, "Processing response");
//
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    JSONArray packets = jsonObject.getJSONArray("packets");
//
//                    String b64packet = (String) packets.get(0);
//                    Log.i(TAG, "Resulting b64encoded packet: \n" + b64packet);
//
//                    String theQuality = jsonObject.getString("Quality");
//                    displayMessageToScreen("New Fingerprint Template Stored : Quality [" + theQuality + "]");
//
//                    return b64packet;
//
//                } catch (JSONException e) {
//                    Log.e(TAG, "Error processing the response");
//                    Toast.makeText(context, "Error processing the response", Toast.LENGTH_SHORT).show();
//                }
//
//                return "Failure";
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.e(TAG, "Response failed");
//                displayMessageToScreen("Response failed");
//            }
//        });
//    }
//
//
//
//
//
//
//
////    private void lumidigmEnrollOnce(final String badge) {
////        final FingerprintLumidigmScanLoadable fLodable = new FingerprintLumidigmScanLoadable();
////        Loader.Callback fLoaderCallback = new Loader.Callback() {
////            @Override public void onLoadError(Loader.Loadable loadable, IOException exception) { }
////            @Override public void onLoadCanceled(Loader.Loadable loadable) { }
////            @Override
////            public void onLoadCompleted(Loader.Loadable loadable) {
////                fingerPrompt.dismiss();
////                fingerPrompt.setMessage("Place Your Finger on the Finger Print Reader");
////                FingerprintLumidigmScanLoadable fl = (FingerprintLumidigmScanLoadable) loadable;
////                Log.d(TAG, "device=" + fl.response.device + ",status=" + fl.response.status);
////                if (!TextUtils.isEmpty(badge)) {
////                    Log.d(TAG, "Adding badge " + badge + " to template " + fLodable.template.toString());
////                    atsApplication.addTemplateToUser(badge, fLodable.template);
////                }
////            }
////        };
////        loader.startLoading(fLodable, fLoaderCallback);
////    }
//
//
//    private void supremaReadImage() {
//        fingerprintService.supremaReadImage().enqueue(new Callback<DeviceInterface.SupremaResponse>() {
//            @Override public void onResponse(Call<DeviceInterface.SupremaResponse> call, Response<DeviceInterface.SupremaResponse> response) { }
//            @Override public void onFailure(Call<DeviceInterface.SupremaResponse> call, Throwable t) { }
//        });
//    }
//    private void lumidigmEnrollOnce_new(final String badge) {
//        Log.d(TAG, "Connecting to Suprema FingerPrint service for enrollOnce");
//        fingerprintService.lumidigmArmTrigger("trigger_on").enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                DeviceInterface.LumidigmResponse lumidigmResponse=null;
//                if (response != null) {
//                    try {
//                        //String responseStr = StringUtilities.convertStreamToString(response.body().byteStream());
//                        String responseStr;
//                        if (response.code() >= 200 && response.code() < 300) {
//                            responseStr = response.body().string();
//                        } else {
//                            responseStr = response.errorBody().string();
//                        }
//                        Log.d(TAG, "response=" + response);
//                        Log.d(TAG, "responseStr=" + responseStr);
//                        Gson gson = new Gson();
//                        lumidigmResponse = gson.fromJson(responseStr, DeviceInterface.LumidigmResponse.class);
//                        Log.d(TAG, "lumidigmResponse=" + lumidigmResponse);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                fingerPrompt.dismiss();
//                fingerPrompt.setMessage("Place Your Finger on the Finger Print Reader");
//                if (lumidigmResponse != null) {
//                    try {
//                        response = fingerprintService.lumidigmGetAcqStatus().execute();
//                        String responseStr;
//                        if (response.code() >= 200 && response.code() < 300) {
//                            responseStr = response.body().string();
//                        } else {
//                            responseStr = response.errorBody().string();
//                        }
//                        Log.d(TAG, "response2=" + response);
//                        Log.d(TAG, "responseStr2=" + responseStr);
//                        Gson gson = new Gson();
//                        lumidigmResponse = gson.fromJson(responseStr, DeviceInterface.LumidigmResponse.class);
//                        Log.d(TAG, "lumidigmResponse2=" + lumidigmResponse);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.d(TAG, "this call cfailed. throwable=" + t);
//            }
//        });
//
//    }
//    private void supremaEnrollOnce_new(final String badge) {
//        Log.d(TAG, "Connecting to Suprema FingerPrint service for enrollOnce");
//        fingerprintService.supremaEnrollOnce().enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call,
//                                   Response<ResponseBody> response) {
//                DeviceInterface.SupremaResponse supremaResponse=null;
//                if (response != null) {
//                    try {
//                        //String responseStr = StringUtilities.convertStreamToString(response.body().byteStream());
//                        String responseStr;
//                        if (response.code() >= 200 && response.code() < 300) {
//                            responseStr = response.body().string();
//                        } else {
//                            responseStr = response.errorBody().string();
//                        }
//                        Log.d(TAG, "response=" + response);
//                        Log.d(TAG, "responseStr=" + responseStr);
//                        Log.d(TAG, "supremaResponse=" + supremaResponse);
//                        Gson gson = new Gson();
//                        supremaResponse = gson.fromJson(responseStr, DeviceInterface.SupremaResponse.class);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                fingerPrompt.dismiss();
//                fingerPrompt.setMessage("Place Your Finger on the Finger Print Reader");
//                if (supremaResponse != null) {
//                    if (supremaResponse.packets != null && supremaResponse.packets.length > 0)
//                        Log.d(TAG, "response.packets[0]=" + supremaResponse.packets[0]);
//                    if (supremaResponse.packets != null && supremaResponse.packets.length > 1) {
//                        Log.d(TAG, "response.packets[1]=" + supremaResponse.packets[1]);
//
//                        // TODO : THis is like byte[] templateArray
//                        String bytes[] = supremaResponse.packets[1].split(" ");
//
//                        Log.d(TAG, "bytes[10]=" + bytes[10]);
//
//                        if (bytes[10].equalsIgnoreCase("61")) {
//                            if (supremaResponse.device.equalsIgnoreCase("suprema")) {
//                                int userId = Integer.parseInt(bytes[2], 16);
//                                int scanQuality = Integer.parseInt(bytes[6], 16);
//                                TagNode fingerPrint = new TagNode("FingerPrint");
//                                AttributeList attribList = new AttributeList();
//                                attribList.append(new Attribute("device", "superma"));
//                                attribList.append(new Attribute("userId", String.format("%d", userId)));
//                                attribList.append(new Attribute("scanQuality", String.format("%d", scanQuality)));
//                                fingerPrint.setAttrList(attribList);
//                                byte[] nodeId = atsApplication.badgeNodes.get(badge);
//                                if (nodeId != null) {
//                                    try {
//                                        XmlUtils.ValueTuple employeeVt = XmlUtils.getParent(nodeId,
//                                                atsApplication.getDbRelations());
//                                        XmlUtils.ValueTuple newEmp = XmlUtils.addChild(employeeVt, fingerPrint,
//                                                atsApplication.getDbIdToPath(),
//                                                atsApplication.getDbPathToId(),
//                                                atsApplication.getDbRelations());
//                                        Util.Log.d(TAG, "new node = " + XmlUtils.printNode(employeeVt.nodeId, atsApplication.getDbIdToPath(),
//                                                atsApplication.getDbPathToId(),
//                                                atsApplication.getDbRelations()));
//                                    } catch (SnappydbException e) {
//                                        e.printStackTrace();
//                                    } catch (UnsupportedEncodingException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                                //TODO : Removed 4/4 :
//                                displayMessageToScreen("Scan successfull scanQuality was "+scanQuality+"%");
//                            }
//                        }
//
//                        Log.d(TAG, "fingerprintId .packets[1]=" + supremaResponse.packets[1]);
//                    }
//                    if (!TextUtils.isEmpty(supremaResponse.errorCode) && !supremaResponse.errorCode.equalsIgnoreCase("0")) {
//                        Log.d(TAG, "response.errorCode=" + supremaResponse.errorCode);
//                        Log.d(TAG, "response.errorString=" + supremaResponse.errorString);
//                        //TODO : Removed 4/4 :
//                        displayMessageToScreen(supremaResponse.errorString);
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.d(TAG, "this call cfailed. throwable=" + t);
//            }
//        });
//
//    }
//
//    RestInterface deviceService;
//    public boolean connectToHardware() {
//        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().readTimeout(Integer.MAX_VALUE, TimeUnit.MILLISECONDS).build();
//        Retrofit deviceAdapter = new Retrofit.Builder()
//                .baseUrl(Constants.getDeviceIpAddress())
//                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(ScalarsConverterFactory.create())
//                .client(okHttpClient)
//                .build();
//
//        deviceService = deviceAdapter.create(RestInterface.class);
//        return true;
//    }
//
//    private class MyWebViewClient extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if (Uri.parse(url).getHost().equals("www.google.com")) {
//                // This is my web site, so do not override; let my WebView load the page
//                return false;
//            }
//            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            startActivity(intent);
//            return true;
//        }
//    }
//
//}
