//package com.accutime.app1.activities.old_activities;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.Intent;
//import android.content.ServiceConnection;
//import android.content.SharedPreferences;
//import android.content.res.Configuration;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.IBinder;
//import android.os.Looper;
//import android.os.Message;
//import android.os.Messenger;
//import android.os.RemoteException;
//import android.preference.PreferenceManager;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.accutime.app1.Application.AtsApplication;
//import com.accutime.app1.R;
//import com.accutime.app1.common.Constants;
//import com.accutime.app1.loader.Loader;
//import com.accutime.app1.retrofit.AtsService;
//import com.accutime.app1.retrofit.DeviceInterface;
//import com.accutime.app1.retrofit.RestInterface;
//import com.accutime.app1.retrofit.FlatteningXmlConverterFactory;
//import com.accutime.app1.retrofit.GsonConverterFactory;
//import com.accutime.app1.retrofit.ScalarsConverterFactory;
//import com.accutime.app1.services.DatabaseService;
//import com.accutime.app1.services.DeviceService;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.concurrent.TimeUnit;
//
//import okhttp3.OkHttpClient;
//import retrofit2.Retrofit;
//
//public abstract class OldBaseActivity extends Activity {
//
//    public static final String TAG = OldBaseActivity.class.getSimpleName();
//
//    // Shared preferences
//    SharedPreferences _sharedPrefs;
//    SharedPreferences.Editor _prefsEditor;
//
//    // Services
//    AtsService atsService;
//    public static DeviceInterface fingerprintService = null;
//    public static RestInterface deviceService = null;
//    boolean boundToDeviceService = false;
//    boolean boundToDatabaseService = false;
//
//    // This is a loader
//    Loader loader = new Loader("LoaderThread");
//
//    public abstract void performBadgeProcessing(int tag, String badge, Bundle extra);
//    public abstract void performEmployeeProcessing(Bundle extra);
//
//    // Get THIS application and typecast it to an ATSApplication
//    AtsApplication atsApplication = (AtsApplication) getApplication();
//
//    AlertDialog badgeDialog;
//    View badgeDialogView;
//    EditText badgeView;
//    TextView badgeEmployeeView;
//
//    RelativeLayout rootView;
//
//    public final int ESS_TAG = 1;
//    public final int TIME_TRACKING_TAG = 2;
//    public final int CLOCK_IN_TAG = 3;
//    public final int CLOCK_OUT_TAG = 4;
//    public final int MEAL_START_TAG = 5;
//    public final int MEAL_END_TAG = 6;
//    public final int BREAK_START_TAG = 7;
//    public final int BREAK_END_TAG = 8;
//    public final int MANAGER_TAG = 9;
//    public final int ENROLL_TAG = 10;
//    public final int ADD_FINGER_TAG = 11;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        updateDisplayConfigs();
//        super.onCreate(savedInstanceState);
//
//        Log.i(TAG, "BaseActivity onCreate() started");
//
//        if (rootView == null)
//            throw new RuntimeException("please call BaseActivity.onCreate() after initializing the ContentView");
//
//        atsApplication = (AtsApplication) getApplication();
//        _sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
//        _prefsEditor = _sharedPrefs.edit();
//
//        Log.i(TAG, "BaseActivity onCreate() finished");
//    }
//
//    /**
//     * Adjust display for different size screens?
//     * Commenting this out doesn't affect anything.
//     */
//    private void updateDisplayConfigs() {
//        Log.i(TAG, "updateDisplayConfigs");
//
//        DisplayMetrics metrics = getResources().getDisplayMetrics();
////        Log.e(TAG, "device dpi="+metrics.densityDpi);
////        Log.e(TAG, "device display size= "+metrics.widthPixels+" x "+metrics.heightPixels);
//        Configuration config = new Configuration();
//        if (metrics.heightPixels < 500)
//            config.smallestScreenWidthDp = 444;
//        else
//            config.smallestScreenWidthDp = 764;
//
////        Log.e(TAG, "smallestScreenWidthDp= "+config.smallestScreenWidthDp);
////        Log.e(TAG, "resouces are taken from '"+getResources().getString(R.string.smallestWidth) + "' folder");
//        getBaseContext().getResources().updateConfiguration(config,metrics);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        Log.d(TAG, "onPause");
//
//        // Clear all dialog fields.
//        badgeEmployeeView = null;
//        badgeDialog.dismiss();
//        badgeDialogView = null;
//        badgeDialog = null;
//        badgeView = null;
//
//        try { doUnbindService(); }
//        catch (Throwable t) { Log.e(TAG, "Failed to unbind from the service", t); }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        Log.i(TAG, "Base Activity onResume() started");
//
//        // Create services
//        createAtsService();
//        createDeviceService();
//        createFingerprintService();
//
//        // Build the badge dialog
//        buildBadgeDialog();
//
//        // Start and bind to services
//        bindToDeviceService();
//        bindToDatabaseService();
//
//        Log.i(TAG, "Base Activity onResume() finished");
//    }
//
//    /**
//     * Creates an AtsService using Retrofit.
//     */
//    public boolean createAtsService() {
//        Log.d(TAG, "createAtsService");
//
//        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                .readTimeout(15000, TimeUnit.MILLISECONDS)
//                .writeTimeout(15000, TimeUnit.MILLISECONDS)
//                .build();
//
//        Retrofit restAdapterRaw = new Retrofit.Builder()
//                .baseUrl(Constants.HOST_URL)
//                //.addConverterFactory(SimpleXmlConverterFactory.create())
//                .addConverterFactory(FlatteningXmlConverterFactory.create())
//                .client(okHttpClient)
//                .build();
//
//        atsService = restAdapterRaw.create(AtsService.class);
//        return true;
//    }
//
//    /**
//     * Creates a device service using OkHttpClient and Retrofit.
//     */
//    public void createDeviceService() {
//        Log.d(TAG, "createDeviceService");
//
//        if (deviceService == null) {
//            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                    .readTimeout(Integer.MAX_VALUE, TimeUnit.MILLISECONDS)
//                    .writeTimeout(Integer.MAX_VALUE, TimeUnit.MILLISECONDS)
//                    .build();
//            Retrofit deviceAdapter = new Retrofit.Builder()
//                    .baseUrl(Constants.getDeviceIpAddress())
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .addConverterFactory(ScalarsConverterFactory.create())
//                    .client(okHttpClient)
//                    .build();
////            Log.d(TAG, "creating deviceService");
//            deviceService = deviceAdapter.create(RestInterface.class);
//        }
//    }
//
//    /**
//     * Creates a fingerprint service using OkHttpClient and Retrofit.
//     */
//    public void createFingerprintService() {
//        Log.d(TAG, "createFingerprintService");
//
//        if (fingerprintService == null) {
//            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                    .readTimeout(15000, TimeUnit.MILLISECONDS)
//                    .writeTimeout(15000, TimeUnit.MILLISECONDS)
//                    .build();
//            Retrofit retrofit = new Retrofit.Builder()
//                    .baseUrl(Constants.getDeviceIpAddress())
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(okHttpClient)
//                    .build();
//
//            Log.d(TAG, "Creating FingerprintService");
//            fingerprintService = retrofit.create(DeviceInterface.class);
//        }
//    }
//
//    /**
//     * Builds the dialog that will be used by each button so the user
//     * can enter their badge number.
//     */
//    public void buildBadgeDialog() {
//        Log.d(TAG, "buildBadgeDialog");
//
//        // Build the layout of the badge dialog.
//        LayoutInflater inflator = getLayoutInflater();
//        badgeDialogView = inflator.inflate(R.layout.enter_badge, null);
//        badgeView = (EditText) badgeDialogView.findViewById(R.id.badge);
//        badgeEmployeeView = (TextView) badgeDialogView.findViewById(R.id.employeeName);
//
//        // Setup OK button.
//        badgeDialogView.findViewById(R.id.buttonOk).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i(TAG, "OK is clicked");
//                int tag = (Integer) badgeDialogView.getTag();
//                String badge = badgeView.getText().toString();
//                Log.i(TAG, "Tag is " + tag);
//                Log.i(TAG, "Badge is " + badge);
//
//                badgeView.setText("");
//                badgeEmployeeView.setText("");
//                badgeDialog.dismiss();
//                performBadgeProcessing(tag, badge, null);
//            }
//        });
//
//        // Setup Cancel button.
//        badgeDialogView.findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i(TAG, "Cancel is clicked");
//                badgeEmployeeView.setText("");
//                badgeView.setText("");
//                if (getMainLooper() == Looper.myLooper())
//                { Log.d(TAG, "This is in the UI thread"); }
//                else
//                { Log.d(TAG, "This is NOT in the UI thread"); }
//                badgeDialog.dismiss();
//            }
//        });
//
//        badgeDialog = new AlertDialog.Builder(this, R.style.PauseDialog).create();
//        badgeDialog.setView(badgeDialogView, 0, 0, 0, 0);
//        badgeEmployeeView.setText(" ");
//        badgeDialog.dismiss();
//        badgeDialog.setCanceledOnTouchOutside(false);
//    }
//
//    /**
//     * Display message to the user.
//     * @param text : the message to show
//     */
//    public void displayMessageToScreen(String text) {
//        Log.d(TAG, "displayMessageToScreen");
//
//        Toast toast = Toast.makeText(this, text, Toast.LENGTH_LONG);
//        LinearLayout toastLayout = (LinearLayout) toast.getView();
//        TextView toastTV = (TextView) toastLayout.getChildAt(0);
//        toastTV.setTextSize(28);
//        toast.show();
//    }
//
//    /**
//     * Bind to the device service.
//     * If it is not already running, start it.
//     */
//    private void bindToDeviceService() {
//        Log.d(TAG, "bindToDeviceService");
//
//        if (DeviceService.isRunning()) {
//            bindService(new Intent(this, DeviceService.class), mDeviceServiceConnection, Context.BIND_AUTO_CREATE);
//            boundToDeviceService = true;
//        } else {
//            startService(new Intent(this, DeviceService.class));
//            bindService(new Intent(this, DeviceService.class), mDeviceServiceConnection, Context.BIND_AUTO_CREATE);
//            boundToDeviceService = true;
//        }
//    }
//
//    /**
//     * Bind to the database service.
//     * If it is not already running, start it.
//     */
//    private void bindToDatabaseService() {
//        Log.d(TAG, "bindToDatabaseService");
//
//        if (DatabaseService.isRunning()) {
////            Log.i(TAG, "DatabaseService is running. Now binding to it.");
//            bindService(new Intent(this, DatabaseService.class), mDatabaseServiceConnection, Context.BIND_AUTO_CREATE);
//            boundToDatabaseService = true;
//        } else {
////            Log.i(TAG, "DatabaseService is not running. Now starting and binding to it.");
//            startService(new Intent(this, DatabaseService.class));
//            bindService(new Intent(this, DatabaseService.class), mDatabaseServiceConnection, Context.BIND_AUTO_CREATE);
//            boundToDatabaseService = true;
//        }
//    }
//
//    Messenger mDeviceService = null;
//    Messenger mDatabaseService = null;
//    final Messenger mMessenger = new Messenger(new IncomingHandler());
//    int readEmployeeAction = Constants.ServiceActions.MSG_READ_EMPLOYEE_FROM_DATABASE.ordinal();
//    int keyboardEvent = Constants.ServiceActions.MSG_NEW_KEYBOARD_EVENT.ordinal();
//
//    /**
//     * Handle incoming messages
//     */
//    class IncomingHandler extends Handler {
//        @Override
//        public void handleMessage(Message msg) {
//            Log.d(TAG, "handleMessage:");
//            Log.d(TAG, "\t" + msg);
//
//            // Read employee from database
//            if (msg.what == readEmployeeAction) {
//                performEmployeeProcessing(msg.getData());
//            }
//
//            // Keyboard event
//            else if (msg.what == keyboardEvent) {
//                try {
//                    processKeyboardEvent(msg.getData().getString("keyboardEvent"));
//                } catch (JSONException e) { e.printStackTrace(); }
//            }
//
//            // Anything else
//            else { super.handleMessage(msg); }
//        }
//    }
//
//    /**
//     * Send a message for a specific action to the device service.
//     * @param id : the action to send
//     * @param extra : bundle with any additional data
//     * @throws RemoteException
//     */
//    public void sendMessageToDeviceService(int id, Bundle extra) throws RemoteException {
//        Log.d(TAG, "sendMessageToDeviceService");
//
//        // Obtain the action
//        Message msg = Message.obtain(null, id);
//
//        // Set data if any exists
//        if (extra != null) { msg.setData(extra); }
//
//        // Specify the receiver
//        msg.replyTo = mMessenger;
//
//        // Send the message
//        if (boundToDeviceService && mDeviceService != null) { mDeviceService.send(msg); }
//    }
//
//    /**
//     * Send a message for a specific action to the database service.
//     * @param action : the action to send
//     * @param extra : bundle with any additional data
//     * @throws RemoteException
//     */
//    public void sendMessageToDatabaseService(int action, Bundle extra) throws RemoteException {
//        Log.d(TAG, "sendMessageToDatabaseService");
//
//        // Obtain the action
//        Message msg = Message.obtain(null, action);
//
//        // Specify the receiver
//        msg.replyTo = mMessenger;
//
//        // Set data if any exists
//        if (extra != null) { msg.setData(extra); }
//
//        // Send the message
//        if (boundToDatabaseService && mDatabaseService != null) {
//            mDatabaseService.send(msg);
//            Log.i(TAG, "Message sent.");
//        }
//        else {
//            Log.i(TAG, "Message to database service failed to send.");
//            Log.i(TAG, "Bound to database service: " + boundToDatabaseService);
//            Log.i(TAG, "Database service exists: " + (mDatabaseService != null));
//        }
//    }
//
//    private ServiceConnection mDeviceServiceConnection = new ServiceConnection() {
//
//        public void onServiceConnected(ComponentName className, IBinder service) {
//            Log.d(TAG,"Service Attached. service="+className.getShortClassName());
//            if (className.getShortClassName().equalsIgnoreCase(".services.DatabaseService")) {
//                mDatabaseService = new Messenger(service);
//                try {
//                    sendMessageToDatabaseService(Constants.ServiceActions.MSG_REGISTER_CLIENT.ordinal(), null);
//                } catch (RemoteException e) {
//                    // In this case the service has crashed before we could even do anything with it
//                }
//            }
//            if (className.getShortClassName().equalsIgnoreCase(".services.DeviceService")) {
//                mDeviceService = new Messenger(service);
//                try {
//                    sendMessageToDeviceService(Constants.ServiceActions.MSG_REGISTER_CLIENT.ordinal(), null);
//                } catch (RemoteException e) {
//                    // In this case the service has crashed before we could even do anything with it
//                }
//            }
//        }
//
//        public void onServiceDisconnected(ComponentName className) {
//            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
//            Log.d(TAG, className.getShortClassName() + " serviceDisconnected.");
//            if (className.getShortClassName().equalsIgnoreCase(".services.DatabaseService")) {
//                mDatabaseService = null;
//            }
//            if (className.getShortClassName().equalsIgnoreCase(".services.DeviceService")) {
//                mDeviceService = null;
//            }
//        }
//    };
//    private ServiceConnection mDatabaseServiceConnection = new ServiceConnection() {
//        public void onServiceConnected(ComponentName className, IBinder service) {
//            Log.d(TAG,"Service Attached. service="+service+", component.getShortName="+className.getShortClassName());
//            if (className.getShortClassName().equalsIgnoreCase(".services.DatabaseService")) {
//                mDatabaseService = new Messenger(service);
//                try {
//                    sendMessageToDatabaseService(Constants.ServiceActions.MSG_REGISTER_CLIENT.ordinal(), null);
//                } catch (RemoteException e) {
//                    // In this case the service has crashed before we could even do anything with it
//                }
//            }
//            if (className.getShortClassName().equalsIgnoreCase(".services.DeviceService")) {
//                mDeviceService = new Messenger(service);
//                try {
//                    sendMessageToDeviceService(Constants.ServiceActions.MSG_REGISTER_CLIENT.ordinal(), null);
//                } catch (RemoteException e) {
//                    // In this case the service has crashed before we could even do anything with it
//                }
//            }
//        }
//
//        public void onServiceDisconnected(ComponentName className) {
//            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
//            Log.d(TAG, className.getShortClassName() + " serviceDisconnected.");
//            if (className.getShortClassName().equalsIgnoreCase(".services.DatabaseService")) {
//                mDatabaseService = null;
//            }
//            if (className.getShortClassName().equalsIgnoreCase(".services.DeviceService")) {
//                mDeviceService = null;
//            }
//        }
//    };
//
//    /**
//     * Unbind the device and database services for cleanup.
//     */
//    void doUnbindService() {
//        Log.d(TAG, "doUnbindService");
//
//        // If we have received the service, and hence registered with it, then now is the time to unregister.
//        if (boundToDeviceService) {
//            if (mDeviceService != null) {
//                Message msg = Message.obtain(null, Constants.ServiceActions.MSG_UNREGISTER_CLIENT.ordinal());
//                msg.replyTo = mMessenger;
//                try {
//                    mDeviceService.send(msg);
//                } catch (RemoteException e) {
//                    // There is nothing special we need to do if the service has crashed.
//                }
//            }
//            // Detach our existing connection.
//            unbindService(mDeviceServiceConnection);
//            boundToDeviceService = false;
//        }
//        if (boundToDatabaseService) {
//            if (mDatabaseService != null) {
//                Message msg = Message.obtain(null, Constants.ServiceActions.MSG_UNREGISTER_CLIENT.ordinal());
//                msg.replyTo = mMessenger;
//                try {
//                    mDatabaseService.send(msg);
//                } catch (RemoteException e) {
//                    // There is nothing special we need to do if the service has crashed.
//                }
//            }
//            // Detach our existing connection.
//            unbindService(mDatabaseServiceConnection);
//            boundToDatabaseService = false;
//        }
//    }
//
//    /**
//     * Processes a key or set of keys from keyboard.
//     * @param data
//     * @throws JSONException
//     */
//    protected synchronized void processKeyboardEvent(String data) throws JSONException {
//        Log.d(TAG, "processKeyboardEvent:" + data);
//
//        JSONObject event = new JSONObject(data);
//        String pressedKeys = event.getString("pressedKeycodes");
//        Log.d(TAG, "pressedKeycodes:" + pressedKeys);
//
//        String[] keys = pressedKeys.split(" ");
//
//        if (keys.length > 1) { return; }
//
//        if (badgeDialog != null) {
//            badgeEmployeeView.setText(" ");
//            if (badgeDialog.isShowing()) {
//                for (String k : keys) {
//                    final int scanCode = Integer.valueOf(k.trim());
//                    final String key = Constants.scanCodeToKeyVal.get(scanCode);
//                    Log.d(TAG, "key = " + key);
//                    switch (key) {
//                        case "c":
//                            String a = badgeView.getText().toString();
//                            badgeView.setText("");
//                            break;
//                        case "e":
//                            int tag = (Integer) badgeDialogView.getTag();
//                            String badge = badgeView.getText().toString();
//                            badgeDialog.dismiss();
//                            performBadgeProcessing(tag, badge, null);
//                            break;
//                        default:
//                            String ss = badgeView.getText().toString() + key;
//                            badgeView.setText(ss);
//                            break;
//                    }
//                }
//            } else {
//                Log.d(TAG, "badgeDialog is not shown:" + pressedKeys);
//            }
//        } else {
//            Log.d(TAG, "badgeDialog is null:" + pressedKeys);
//        }
//    }
//}
