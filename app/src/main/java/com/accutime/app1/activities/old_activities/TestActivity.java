//package com.accutime.app1.activities.old_activities;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.widget.TextView;
//
//import com.accutime.app1.R;
//
//public class TestActivity extends Activity {
//
//    String testEmployees=
//            "<Employees>" +
//                    "  <Employee badge=\"900000212\" supervisor_level=\"1\" " +
//                    "        schedule_id=\"3479\" break_schedule_id=\"3479\"" +
//                    "        meal_schedule_id=\"3479\" language=\"\"" +
//                    "        >\n" +
//                    "    <Name>William Bailey</Name>\n" +
//                    "    <WeekToDateHours>0</WeekToDateHours>\n" +
//                    "    <SecondPunchTimeRestrict>1</SecondPunchTimeRestrict>\n" +
//                    "  </Employee>\n" +
//                    "</Employees>";
//    String localEmployee3 =
//            "<Employee badge=\"11223344\" supervisor_level=\"4\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                    "<Name>Jean-Pierre Van de Capelle</Name> " +
//                    "<WeekToDateHours>0</WeekToDateHours> "+
//                    "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                    "</Employee> " ;
//    String localEmployee2 =
//            "<Employee badge=\"22334455\" supervisor_level=\"4\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                    "<Name>Carlos Bernal</Name> " +
//                    "<WeekToDateHours>0</WeekToDateHours> "+
//                    "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                    "</Employee> " ;
//    String localEmployee1 =
//            "<Employee badge=\"33445566\" supervisor_level=\"4\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                    "<Name>Jagat Brahma</Name> " +
//                    "<WeekToDateHours>0</WeekToDateHours> "+
//                    "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                    "</Employee> " ;
//    String localEmployee4 =
//            "<Employee badge=\"44556677\" supervisor_level=\"4\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                    "<Name>Asha Hemingway</Name> " +
//                    "<WeekToDateHours>0</WeekToDateHours> "+
//                    "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                    "</Employee> " ;
//    String localEmployee5 =
//            "<Employee badge=\"55667788\" supervisor_level=\"4\"" +
//                    " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                    "meal_schedule_id=\"12123\" language=\"english\"" +
//                    " > " +
//                    "<Name>Ryan McColgan</Name> " +
//                    "<WeekToDateHours>0</WeekToDateHours> "+
//                    "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                    "</Employee> " ;
//
//    String[] localEmployees = {
//            localEmployee1,
//            localEmployee2,
//            localEmployee3,
//            localEmployee4,
//            localEmployee5
//    };
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_test);
//
//        TextView employees = (TextView) findViewById(R.id.tv_txml);
//        employees.setText(ChooserActivity.txmlString);
//    }
//}
