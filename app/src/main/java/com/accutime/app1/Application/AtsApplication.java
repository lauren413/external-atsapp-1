package com.accutime.app1.Application;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.accutime.app1.retrofit.DeviceInterface;
import com.accutime.app1.util.Util;
import com.accutime.app1.xmlparser.Attribute;
import com.accutime.app1.xmlparser.AttributeList;
import com.accutime.app1.xmlparser.TagNode;
import com.accutime.app1.xmlparser.TreeNode;
import com.accutime.app1.xmlparser.XmlUtils;
import com.snappydb.DBLocal;
import com.snappydb.SnappyDBLocal;
import com.snappydb.SnappydbException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * According to the manifest, this gets loaded first instead of Application.
 * onCreate() calls super.onCrate(), so all the functionality of a typical
 * application is there, but it also loads the dbHelper and badgeNodes.
 *
 */
public class AtsApplication extends Application {

    // Tag for logging
    public static final String TAG = AtsApplication.class.getSimpleName();
    private Context context = AtsApplication.this;

    // For database
    private DbHelper dbHelper;

    // TODO: Question: What are these?
    DeviceInterface.DiscoverResponse discoverResponse;
//    DeviceInterface.DiscoverResponse.Device supremaDevice = null;
//    DeviceInterface.DiscoverResponse.Device lumidigmDevice = null;

    // Maps an array of byte[] values to a String
    public static Map<String, byte[]> badgeNodes;

    private static Application sApplication;

    public static Application getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "AtsApplication onCreate() started");

        sApplication = this;

        // Create databases.
        dbHelper = new DbHelper();
        dbHelper.getInstance();

        // TODO : Is a thread okay?
        new Thread(new Runnable() {
            @Override
            public void run() {
                // Passes a path from which to get key value pairs for a node
                badgeNodes = getKeyValuePairsForNode("/Employees/Employee/badge");
            }
        }).start();


        Log.d(TAG, "AtsApplication onCreate() finished");
    }

    public DBLocal getSnappyDBlocal() {
        return dbHelper.mSnappyDBlocal;
    }
    public DBLocal getDbPathToId() {
        return dbHelper.mDbPathToId;
    }
    public DBLocal getDbIdToPath() {
        return dbHelper.mDbIdToPath;
    }
    public DBLocal getDbRelations() {
        return dbHelper.mDbRelations;
    }

    /**
     * From the given path, get a map of values for a badge node.
     * @param path : path to get key value pairs from
     * @return : a map with values assigned to keys
     */
    public Map<String, byte[]> getKeyValuePairsForNode(String path) {
        Log.v(TAG, "getKeyValuePairsForNode : " + path);
        return dbHelper.getKeyValuePairsForNode(path);
    }

    public void setDiscoverConfig(DeviceInterface.DiscoverResponse discoverResponse) {
        this.discoverResponse = discoverResponse;
    }

    /**
     * DbHelper class for accessing database
     */
    public class DbHelper {

        /**
         * Each of these are strings pointing to the absolute path of the directory where files
         * created with openFileOutput(String, int). The additional folders they point to are then
         * appended on.
         */
        String dbFilename = getFilesDir() + "/snappy";
        String dbFilenameLocal = getFilesDir() + "/snappyLocal";
        String dbPathToId = getFilesDir() + "/snappyLocalPathToId";
        String dbIdToPath = getFilesDir() + "/snappyLocalIdToPath";
        String dbRelations = getFilesDir() + "/snappyLocalRelations";

        /**
         * DBLocal is an interface defined in the snappyLibrary.
//         * Here we are creating instances to implement this interface.
         * Instances of these variables can be any class that implements DBLocal.
         */
        DBLocal mSnappyDBlocal;
        DBLocal mDbPathToId;
        DBLocal mDbIdToPath;
        DBLocal mDbRelations;

        // Constructor. Do not instantiate this class through a constructor.
        private DbHelper() {}
        private DbHelper instance = null;

        /**
         * Get instance of DBHelper.
         * Create four databases.
         */
        public DbHelper getInstance() {
            Log.d(TAG, "getInstance");

            // If an instance already exists, use that one.
            if (instance != null) {
                return instance;
            }

            // If not, make a new instance.
            try {
                Log.d(TAG, "getCacheDir()   : " + getCacheDir());
                Log.d(TAG, "dbFilename      : " + dbFilename + ", dbFilenameLocal : " + dbFilenameLocal);
                Log.d(TAG, "dbPathToId      : " + dbPathToId + ", dbIdToPath : " + dbIdToPath);
                Log.d(TAG, "dbRelations     : " + dbRelations);

                // Instantiate these four variables of type DBLocal, which is an interface.
                // Basically just create these four databases in the given directories with the given names.
                mSnappyDBlocal  = new SnappyDBLocal.Builder(context).directory(dbFilenameLocal).name("employees").build();
                mDbPathToId     = new SnappyDBLocal.Builder(context).directory(dbPathToId).name("pathToId").build();
                mDbIdToPath     = new SnappyDBLocal.Builder(context).directory(dbIdToPath).name("idToPath").build();
                mDbRelations    = new SnappyDBLocal.Builder(context).directory(dbRelations).name("relations").build();

                instance = new DbHelper();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return instance;
        }

        /**
         * Pass the specific path, as well as two others, to the XmlUtils class
         * Invoke method getValuesAsMap(), passing in the above parameters
         * @param path : path to a specific employee's badge
         * @return a map to that badge
         */
        public Map<String, byte[]> getKeyValuePairsForNode(String path) {
            Log.v(TAG, "getKeyValuePairsForNode : " + path);

            return XmlUtils.getValuesAsMap(path, mDbRelations, mDbPathToId);
        }
    }

    public XmlUtils.ValueTuple storeObject(TagNode tagNode, String parentNode) throws SnappydbException {
        Log.v(TAG, "storeObject : " + tagNode + ", Parent Node: " + parentNode);

        XmlUtils.ValueTuple rootNode;
        rootNode = XmlUtils.storeObject(tagNode, parentNode, getDbIdToPath(),
                                                 getDbPathToId(), getDbRelations());
        return rootNode;
    }

    public void storeRootNodeName(XmlUtils.ValueTuple rootNode) throws SnappydbException {
        Log.v(TAG, "storeRootNodeName : " + rootNode);

        getDbIdToPath().put(Base64.encodeToString(rootNode.nodeId, Base64.DEFAULT), "/");
        getDbPathToId().put("/", rootNode.nodeId);
    }


    /**
     * This is never used right now...
     */
    public XmlUtils.ValueTuple getEmployee(String badge, Map<String, byte[]> badgeNodes) {
        Log.d(TAG, "getEmployee : " + badge);

        if (!TextUtils.isEmpty(badge)) {
            if (badgeNodes == null || badgeNodes.keySet().size() == 0) {
                return null;
            }
            byte[] nodeId = badgeNodes.get(badge);
            if (nodeId != null) {
                try {
                    return XmlUtils.getParent(nodeId, getDbRelations());
                } catch (SnappydbException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * Used by ManagerActivity.
     * Adds a fingerprint template to the user with the given badge.
     */
    public void addTemplateToUser(String badge, String template) {
        Log.d(TAG, "addTemplateToUser : " + badge + ", " + template);

        TagNode fingerPrint = new TagNode("FingerPrint");
        AttributeList attribList = new AttributeList();
        attribList.append(new Attribute("template", template));
        fingerPrint.setAttrList(attribList);

        byte[] nodeId = badgeNodes.get(badge);

        if (nodeId != null) {
            try {
                XmlUtils.ValueTuple employeeVt = XmlUtils.getParent(nodeId, getDbRelations());
                XmlUtils.ValueTuple newEmp = XmlUtils.addChild(employeeVt, fingerPrint,
                        getDbIdToPath(), getDbPathToId(), getDbRelations());

                Util.Log.d(TAG, "new node=" + XmlUtils.printNode(employeeVt.nodeId,
                        getDbIdToPath(), getDbPathToId(), getDbRelations()));

            } catch (SnappydbException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Used by ChooserActivity, when attempting to log in as Manager.
     *
     */
    public String getEmployeeAttribute(String badge, Map<String, byte[]> badgeNodes, String attributeName) {
        Log.d(TAG, "getEmployeeAttribute : " + badge);

        // Make sure a badge with a number was passed.
        if (TextUtils.isEmpty(badge)) { return null; }

        // If badge nodes are null... return null.
        if (badgeNodes == null || badgeNodes.keySet().size() == 0) { return null; }

        // Get the byte[] value for the given badge number.
        byte[] nodeId = badgeNodes.get(badge);
        XmlUtils.ValueTuple employeeVt = null;
        TreeNode employeeNode = null;

        // If there actually is a value for that given badge number...
        if (nodeId != null) {

            try {

                employeeVt = XmlUtils.getParent(nodeId, getDbRelations());
                employeeNode = XmlUtils.retrieveObject(employeeVt.nodeId, getDbIdToPath(), getDbPathToId(), getDbRelations());

            } catch (UnsupportedEncodingException | SnappydbException e) {
                e.printStackTrace();
            }

            if (employeeVt == null || employeeNode == null) { return null; }

            if (!(employeeNode instanceof TagNode)) { return null; }

            // If everything goes smoothly,
            // return the attribute for this node from this employee.
            // In the Manager's supervisor_level case,
            // this should be an int as a string, representing the supervisor level.
            final TagNode employee = (TagNode) employeeNode;
            return XmlUtils.getNodeAttribute(employee, attributeName);
        }

        return null;
    }
}