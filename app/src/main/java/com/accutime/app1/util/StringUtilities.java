package com.accutime.app1.util;

import com.accutime.app1.xmlparser.XmlUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Misc String Utilities
 */
public class StringUtilities {
	public static final String TAG=StringUtilities.class.getSimpleName();
	public static String encode(String s) {
		try {
			if (s == null) {
				return "";
			}
			return URLEncoder.encode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// utf-8 always available
		}
		return null;
	}

	public static String decode(String s) {
		try {
			if (s == null) {
				return null;
			}
			return URLDecoder.decode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// utf-8 always available
		}
		return null;
	}

	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	public static String bytesToString(byte[] bytes) {
		return  new String(bytes, StandardCharsets.UTF_8);
	}


    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes, int firstCount, char firstSep, int secondCount, char secondSep) {
		if (bytes == null || bytes.length == 0)
			return null;

        char[] hexChars = new char[bytes.length*2 + 2];
		int jj=0;
		for (; jj < hexChars.length; jj++ ) {
			hexChars[jj]=' ';
		}
		jj=0;

		if (firstCount > bytes.length)
			firstCount = bytes.length;
		for (; jj < firstCount; jj++ ) {
			int v = bytes[jj] & 0xFF;
			hexChars[jj*2] = hexArray[v >>> 4];
			hexChars[jj*2 + 1] = hexArray[v & 0x0F];
		}
		if (jj >= bytes.length)
			return new String(hexChars);

		hexChars[jj*2] = firstSep;

		secondCount = jj+secondCount;
		if (secondCount > bytes.length)
			secondCount = bytes.length;
		for (; jj < secondCount; jj++ ) {
			int v = bytes[jj] & 0xFF;
			hexChars[jj*2 + 1] = hexArray[v >>> 4];
			hexChars[jj*2 + 2] = hexArray[v & 0x0F];
		}
		if (jj >= bytes.length)
			return new String(hexChars);

		hexChars[jj*2+2] = secondSep;

		for (; jj < bytes.length; jj++ ) {
            int v = bytes[jj] & 0xFF;
            hexChars[jj*2+2] = hexArray[v >>> 4];
            hexChars[jj*2+3] = hexArray[v & 0x0F];
        }

		/*
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
			hexChars[j * 3 + 2] = ' ';
        }
        */
        return new String(hexChars);
    }

	public static String bytesToHex(byte[] bytes, ByteFormat format) {
		if (bytes == null || bytes.length == 0)
			return null;

		if (format == ByteFormat.NODE_TYPE){
			return bytesToHex(bytes, XmlUtils.CLASS_ID_BYTE_LENGTH, '.', XmlUtils.INSTANCE_ID_BYTE_LENGTH, ' ');
		}

		if (format == ByteFormat.VALUE_TYPE){
			return bytesToHex(bytes, XmlUtils.NODE_ID_BYTE_LENGTH, ':', XmlUtils.NODE_ID_BYTE_LENGTH, ':');
		}

		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public enum ByteFormat {
		NODE_TYPE,
		VALUE_TYPE,
		RAW_TYPE
	};

	public static byte[] hexToBytes(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}

}
