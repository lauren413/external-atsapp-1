package com.accutime.app1.util;

import java.util.Calendar;

/**
* Created by jpbrahma on 3/24/15.
*/
public class Measurement {
    public long avg=0;
    public long start,end;
    public void start() {
        start = Calendar.getInstance().getTimeInMillis();
    }
    public void end() {
        end = Calendar.getInstance().getTimeInMillis();
        if (avg==0) {
            avg = avg + (end - start);
        } else {
            avg = avg + (end - start);
            avg = avg / 2;
        }
    }
    public long diff() {
        return end-start;
    }
    public String show() {
        StringBuilder sb = new StringBuilder("times:");
        sb.append(start).append("-").append(end)
          .append(" ,diff=").append(diff())
          .append(", avg=").append(avg);
        return sb.toString();
    }
}
