package com.accutime.app1.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mkemp on 3/11/17.
 */

public class PreferenceUtilities {

    private static final String TAG = PreferenceUtilities.class.getSimpleName();

    public static final String PREFERENCES = "preferences";

    public static class Preferences {

        public static final String HAS_EMPLOYEES_KEY = "hasEmployees";
        public static final String WORKING_BATCH_KEY = "workingBatch";
        public static final String BATCH_TO_SEND_KEY = "batchToSend";

        public static final String EMPLOYEE_VERSION_KEY = "employeeVersion";
        public static final String BIOMETRICS_VERSION_KEY = "biometricsVersion";
        public static final String CONFIG_VERSION_KEY = "configVersion";
        public static final String DLD_VERSION_KEY = "dldVersion";
        public static final String FLASH_VERSION_KEY = "flashVersion";


        public static boolean getHasEmployees(Context context) {

            // Get access to all shared preferences for this activity
            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Return the specific preference we want
            return preferences.getBoolean(HAS_EMPLOYEES_KEY, false);

        }

        public static void setHasEmployees(Context context, boolean value) {

            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Create an editor to edit this preference to our new value
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(HAS_EMPLOYEES_KEY, value).apply();
        }

        public static String getWorkingBatch(Context context) {

            // Get access to all shared preferences for this activity
            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Return the specific preference we want
            return preferences.getString(WORKING_BATCH_KEY, "");

        }

        public static void setWorkingBatch(Context context, String value) {

            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Create an editor to edit this preference to our new value
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(WORKING_BATCH_KEY, value).apply();
        }

        public static String getBatchToSend(Context context) {

            // Get access to all shared preferences for this activity
            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Return the specific preference we want
            return preferences.getString(BATCH_TO_SEND_KEY, "");

        }

        public static void setBatchToSend(Context context, String value) {

            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Create an editor to edit this preference to our new value
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(BATCH_TO_SEND_KEY, value).apply();
        }

        public static String getEmployeeVersion(Context context) {
            // Get access to all shared preferences for this activity
            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Return the specific preference we want
            return preferences.getString(EMPLOYEE_VERSION_KEY, "");
        }

        public static void setEmployeeVersion(Context context, String value) {

            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Create an editor to edit this preference to our new value
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(EMPLOYEE_VERSION_KEY, value).apply();
        }

        public static String getBiometricsVersion(Context context) {
            // Get access to all shared preferences for this activity
            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Return the specific preference we want
            return preferences.getString(BIOMETRICS_VERSION_KEY, "");
        }

        public static void setBiometricsVersion(Context context, String value) {

            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Create an editor to edit this preference to our new value
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(BIOMETRICS_VERSION_KEY, value).apply();
        }

        public static String getConfigVersion(Context context) {
            // Get access to all shared preferences for this activity
            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Return the specific preference we want
            return preferences.getString(CONFIG_VERSION_KEY, "");
        }

        public static void setConfigVersion(Context context, String value) {

            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Create an editor to edit this preference to our new value
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(CONFIG_VERSION_KEY, value).apply();
        }

        public static String getDldVersion(Context context) {
            // Get access to all shared preferences for this activity
            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Return the specific preference we want
            return preferences.getString(DLD_VERSION_KEY, "");
        }

        public static void setDldVersion(Context context, String value) {

            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Create an editor to edit this preference to our new value
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(DLD_VERSION_KEY, value).apply();
        }

        public static String getFlashVersion(Context context) {
            // Get access to all shared preferences for this activity
            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Return the specific preference we want
            return preferences.getString(FLASH_VERSION_KEY, "");
        }

        public static void setFlashVersion(Context context, String value) {

            SharedPreferences preferences = context.getSharedPreferences
                    (PREFERENCES, Context.MODE_PRIVATE);

            // Create an editor to edit this preference to our new value
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(FLASH_VERSION_KEY, value).apply();
        }
    }
}
