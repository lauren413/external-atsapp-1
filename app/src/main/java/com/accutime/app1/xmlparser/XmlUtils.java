package com.accutime.app1.xmlparser;

import android.text.TextUtils;
import android.util.Log;

import com.accutime.app1.util.StringUtilities;
import com.snappydb.DBLocal;
import com.snappydb.KeyIterator;
import com.snappydb.SnappydbException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Conversion;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Random;

/**
 * Created by jpbrahma on 4/29/15.
 */
public class XmlUtils {
    public static final String TAG = XmlUtils.class.getSimpleName();

    public final static int CLASS_ID_BYTE_LENGTH = 2;
    public final static int INSTANCE_ID_BYTE_LENGTH = 3;
    public final static int NODE_ID_BYTE_LENGTH = CLASS_ID_BYTE_LENGTH + INSTANCE_ID_BYTE_LENGTH;
    public final static int CHILD_START_BYTE = 0;
    public final static int CHILD_END_BYTE = CHILD_START_BYTE + NODE_ID_BYTE_LENGTH - 1;
    public final static int PARENT_START_BYTE = CHILD_END_BYTE + 1;
    public final static int PARENT_END_BYTE = PARENT_START_BYTE + NODE_ID_BYTE_LENGTH - 1;
    public final static int PREV_SIBLING_START_BYTE = PARENT_END_BYTE + 1;
    public final static int PREV_SIBLING_END_BYTE = PREV_SIBLING_START_BYTE + NODE_ID_BYTE_LENGTH - 1;
    public final static int NEXT_SIBLING_START_BYTE = PREV_SIBLING_END_BYTE + 1;
    public final static int NEXT_SIBLING_END_BYTE = NEXT_SIBLING_START_BYTE + NODE_ID_BYTE_LENGTH - 1;
    public final static int VALUE_START_BYTE = NEXT_SIBLING_END_BYTE + 1;
    public final static int VALUE_END_BYTE = VALUE_START_BYTE + NODE_ID_BYTE_LENGTH - 1;
    public final static byte[] zeros = new byte[NODE_ID_BYTE_LENGTH];
    public final static byte[] emptyValue = new byte[0];
    public final static boolean DEBUG = false;
    static final Random sr = new Random();


    public final static byte[] attribMarkerId = new byte[NODE_ID_BYTE_LENGTH];
    public final static byte[] attribMarkerInstanceId = new byte[INSTANCE_ID_BYTE_LENGTH];
    public static long attribMarkerInstanceHash=0, attribMarkerIdHash=0;
    public static long zerosHash=0;
    public static byte ATTRIB_MARKER_BYTE_VALUE=1;

    static {
        for(int ii=0; ii< CLASS_ID_BYTE_LENGTH; ii++) {
            attribMarkerId[ii] = 0;
        }
        for(int ii=0; ii< attribMarkerInstanceId.length; ii++) {
            attribMarkerInstanceId[ii] = ATTRIB_MARKER_BYTE_VALUE;
            attribMarkerId[CLASS_ID_BYTE_LENGTH+ii] = ATTRIB_MARKER_BYTE_VALUE;
        }
        attribMarkerInstanceHash = ArrayUtils.hashCode(attribMarkerInstanceId);
        attribMarkerIdHash = ArrayUtils.hashCode(attribMarkerId);
        zerosHash = ArrayUtils.hashCode(zeros);
    }

    public static byte[] getNewInstanceId() {
        byte[] output = new byte[INSTANCE_ID_BYTE_LENGTH];
        sr.nextBytes(output);

        while (ArrayUtils.hashCode(output) == attribMarkerInstanceHash) {
            if (DEBUG) Log.d("getNewInstanceId", "instance id collision with attribMarker");
            sr.nextBytes(output);
        }

        return output;
    }

    private static byte[] createNewInstanceId(String nodePath, DBLocal idToPathDatabase, DBLocal pathToIdDatabase) {
        byte[] nodeClassId = storePath(nodePath, idToPathDatabase, pathToIdDatabase);
        return combineClassAndInstanceId(nodeClassId);
    }

    private static byte[] combineClassAndInstanceId(byte[] nodeClassId) {
        byte[] aInstanceId = XmlUtils.getNewInstanceId();
        return combineClassAndInstanceId(aInstanceId, nodeClassId);
    }
    private static byte[] combineClassAndInstanceId(byte[] nodeInstanceId, byte[] nodeClassId) {
        return ArrayUtils.addAll(nodeClassId, nodeInstanceId);
    }

    public static byte[] storePath(String path, DBLocal idToPathDatabase, DBLocal pathToIdDatabase) {
        byte[] digest;
        byte[] pathId = null;
        try {
            byte[][] pathIds = pathToIdDatabase.getBytes(path);
            if (pathIds.length > 0) {
                pathId = pathIds[0];
            }
            int pathIdInt = 0;
            if (pathId != null) {
                pathIdInt = Conversion.byteArrayToInt(pathId, 0, 0, 0, pathId.length);
            }
            if (pathIdInt == 0) {
                digest = DigestUtils.md5(path);
                pathId = ArrayUtils.subarray(digest, 0, XmlUtils.CLASS_ID_BYTE_LENGTH);
                String pathIdStr = StringUtilities.bytesToHex(pathId, StringUtilities.ByteFormat.RAW_TYPE);
                idToPathDatabase.put(new String(Hex.encodeHex(pathId)), path);
                pathToIdDatabase.put(path, pathId);
                if (DEBUG) Log.d("storePath", "path=" + path + ",pathID=" + pathIdStr);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return pathId;
    }

    public static byte[] fromStringToUTF8Bytes(String str) {
        return  str.getBytes(Charset.forName("UTF-8"));
    }

    public static String fromUTF8BytesToString(byte[] utf8Array) throws UnsupportedEncodingException {
        if (utf8Array.length != 0)
            return new String(utf8Array, "UTF8");
        else
            return "";
    }

    public static class ValueTuple {
        public byte[] nodeId;
        public byte[] parentId;
        public byte[] childId;
        public byte[] prevSiblingId;
        public byte[] nextSiblingId;
        public byte[] value;

        public static ValueTuple fromValue(byte[] value) {
            ValueTuple v = new ValueTuple();
            v.childId = ArrayUtils.subarray(value, CHILD_START_BYTE, CHILD_END_BYTE+1);
            v.parentId = ArrayUtils.subarray(value, PARENT_START_BYTE, PARENT_END_BYTE+1);
            v.prevSiblingId = ArrayUtils.subarray(value, PREV_SIBLING_START_BYTE, PREV_SIBLING_END_BYTE+1);
            v.nextSiblingId = ArrayUtils.subarray(value, NEXT_SIBLING_START_BYTE, NEXT_SIBLING_END_BYTE+1);
            v.value = ArrayUtils.subarray(value, VALUE_START_BYTE, value.length);
            return v;
        }

        public static ValueTuple fromNodeId(byte[] nodeId, DBLocal relationsDatabase) throws UnsupportedEncodingException, SnappydbException {
            if (ArrayUtils.hashCode(nodeId) == zerosHash)
                return null;

            byte[][] value = relationsDatabase.getBytes(new String(Hex.encodeHex(nodeId)));
            byte[] finalValue = value[0];
            ValueTuple v = fromValue(finalValue);
            v.nodeId = nodeId;
            return v;
        }

        public String toString(DBLocal id2Name) {
            StringBuilder sbi = new StringBuilder();
            String nodePath=null, nodeIdStr=null;
            String childPath=null, childIdStr=null;
            String parentPath=null, parentIdStr=null;
            String prevSiblingPath=null, prevSiblingIdStr=null;
            String nextSiblingPath=null, nextSiblingIdStr=null;
            String valueStr=null;

            if (nodeId != null) {
                nodeIdStr = StringUtilities.bytesToHex(nodeId, StringUtilities.ByteFormat.NODE_TYPE);
                if (id2Name != null) {
                    nodePath = idToPath(nodeId, id2Name);
                }
            }

            if (childId != null) {
                childIdStr = StringUtilities.bytesToHex(childId, StringUtilities.ByteFormat.NODE_TYPE);
                if (id2Name != null) {
                    childPath = idToPath(childId, id2Name);
                }
            }

            if (parentId != null) {
                parentIdStr = StringUtilities.bytesToHex(parentId, StringUtilities.ByteFormat.NODE_TYPE);
                if (id2Name != null) {
                    parentPath = idToPath(parentId, id2Name);
                }
            }

            if (prevSiblingId != null) {
                prevSiblingIdStr = StringUtilities.bytesToHex(prevSiblingId, StringUtilities.ByteFormat.NODE_TYPE);
                if (id2Name != null) {
                    prevSiblingPath = idToPath(prevSiblingId, id2Name);
                }
            }

            if (nextSiblingId != null) {
                nextSiblingIdStr = StringUtilities.bytesToHex(nextSiblingId, StringUtilities.ByteFormat.NODE_TYPE);
                if (id2Name != null) {
                    nextSiblingPath = idToPath(nextSiblingId, id2Name);
                }
            }

            if (value != null)
                valueStr = StringUtilities.bytesToHex(value, StringUtilities.ByteFormat.VALUE_TYPE);
            sbi.append("nodeId=").append(nodeIdStr).append("(").append(nodePath).append(")")
               .append(",child=").append(childIdStr).append("(").append(childPath).append(")")
               .append(",parent=").append(parentIdStr).append("(").append(parentPath).append(")")
               .append(",prevSibling=").append(prevSiblingIdStr).append("(").append(prevSiblingPath).append(")")
               .append(",nextSibling=").append(nextSiblingIdStr).append("(").append(nextSiblingPath).append(")")
               .append(",value=").append(valueStr);
            return sbi.toString();
        }

        public void commitRelation(DBLocal relationsDatabase)  throws SnappydbException {
           XmlUtils.makeRelation(nodeId, parentId, childId, prevSiblingId, nextSiblingId, value, relationsDatabase);
        }
    }

    public static byte[] pathToId(String path, DBLocal pathToIdDatabase) throws SnappydbException {
        byte[][] pathIds = pathToIdDatabase.getBytes(path);
        if (pathIds.length > 0) {
            return pathIds[0];
        }
        return null;
    }
    public static String idToPath(byte[] id, DBLocal idToPathDatabase) {
        try {
            byte[] clsId = id;
            if (id.length > CLASS_ID_BYTE_LENGTH) {
                clsId = ArrayUtils.subarray(id, 0, CLASS_ID_BYTE_LENGTH);
            }
            String[] paths = idToPathDatabase.get(new String(Hex.encodeHex(clsId)));
            if (paths.length > 0)
                return paths[0];
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return null;
    }

    ArrayDeque<ValueTuple> relationsCache = new ArrayDeque<>(16);

    private static void makeRelation(byte[] nodeId, byte[] parentId, byte[] childId, byte[] prevSiblingId, byte[] nextSiblingId,
                                     byte[] value, DBLocal relationsDatabase) throws SnappydbException {
        if (parentId == null || parentId.length==0)
            parentId = XmlUtils.zeros;

        if (childId == null || childId.length==0)
            childId = XmlUtils.zeros;

        if (prevSiblingId == null || prevSiblingId.length==0)
            prevSiblingId = XmlUtils.zeros;

        if (nextSiblingId == null || nextSiblingId.length==0)
            nextSiblingId = XmlUtils.zeros;

        if (value == null || value.length==0)
            value = XmlUtils.emptyValue;

        byte[] siblings = ArrayUtils.addAll(prevSiblingId, nextSiblingId);
        byte[] family = ArrayUtils.addAll(parentId, siblings);
        byte[] finalValue = ArrayUtils.addAll(childId, ArrayUtils.addAll(family, value));

        if (DEBUG) {
            String nodeIdStr = StringUtilities.bytesToHex(nodeId, StringUtilities.ByteFormat.NODE_TYPE);
            String parentIdStr = StringUtilities.bytesToHex(parentId, StringUtilities.ByteFormat.NODE_TYPE);
            String childIdStr = StringUtilities.bytesToHex(childId, StringUtilities.ByteFormat.NODE_TYPE);
            String nextSiblingIdStr = StringUtilities.bytesToHex(nextSiblingId, StringUtilities.ByteFormat.NODE_TYPE);
            String prevSiblingIdStr = StringUtilities.bytesToHex(prevSiblingId, StringUtilities.ByteFormat.NODE_TYPE);
            String valueStr = StringUtilities.bytesToHex(value, StringUtilities.ByteFormat.RAW_TYPE);
            Log.d("commitRelation", "commitRelation(nodeId=" + nodeIdStr + ",parentId=" + parentIdStr + ",childId=" + childIdStr +
                    ",nextSiblindId=" + nextSiblingIdStr + ",prevSiblindId=" + prevSiblingIdStr + ",value=" + valueStr);
            String finalValueStr = StringUtilities.bytesToHex(finalValue, StringUtilities.ByteFormat.RAW_TYPE);
            Log.d("commitRelation", "put(nodeId=" + nodeIdStr + ",finalValue=" + finalValueStr);
        }

        relationsDatabase.put(new String(Hex.encodeHex(nodeId)), finalValue);
    }

    public static String retrieveString(byte[] nodeId,
                                     DBLocal idToPathDatabase,
                                     DBLocal pathToIdDatabase,
                                     DBLocal relationsDatabase) {
        StringBuilder sb = new StringBuilder();

        String nodePath = FilenameUtils.getBaseName(idToPath(nodeId, idToPathDatabase));
        if (!TextUtils.isEmpty(nodePath)) {
            sb.append("<"+nodePath);
        } else {
            return null;
        }

        ValueTuple v = null ;
        try {
            v = ValueTuple.fromNodeId(nodeId, relationsDatabase);
        } catch (SnappydbException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (v != null) {
            AttributeList  attribList = new AttributeList();

            //traverse the siblings
            byte[] siblingId = v.nextSiblingId;
            while ((siblingId != null) && (ArrayUtils.hashCode(siblingId) != zerosHash)) {
                ValueTuple sibling = null ;
                try {
                    sibling = ValueTuple.fromNodeId(siblingId, relationsDatabase);
                    if (ArrayUtils.hashCode(sibling.childId) == attribMarkerIdHash) {
                        //when the childId of a node is equal to attribMarkerId, that node is an attribute node,
                        //the parent is the tag for that attribute.
                        // Note that it can have a siblingId.
                        String path = idToPath(v.childId, idToPathDatabase);
                        String basename = FilenameUtils.getBaseName(path);
                        //String extension = FilenameUtils.getExtension(path);
                        Attribute attrib = new Attribute(basename);
                        attrib.setValue(fromUTF8BytesToString(sibling.value));
                        attribList.append(attrib);
                    } else {
                        if (attribList.hasAttributes()) {
                            Iterator iter = attribList.getIterator();
                            while (iter.hasNext()) {
                                Attribute attr = (Attribute)iter.next();
                                sb.append(' ');
                                String attrName = attr.toString();
                                sb.append( attrName );
                                sb.append("=\"");
                                String attrVal = attr.getValue();
                                sb.append( attrVal );
                                sb.append('"');
                            }
                        }
                        sb.append(">").append(v.value);
                        sb.append(retrieveString(siblingId, idToPathDatabase, pathToIdDatabase, relationsDatabase));
                    }
                } catch (SnappydbException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            //traverse the child
            if ((v.childId != null) && (ArrayUtils.hashCode(v.childId) != zerosHash)) {
                sb.append(retrieveString(v.childId, idToPathDatabase, pathToIdDatabase, relationsDatabase));
            }
        }
        return sb.toString();
    }


    public static TreeNode retrieveObject(byte[] nodeId,
                                     DBLocal idToPathDatabase,
                                     DBLocal pathToIdDatabase,
                                     DBLocal relationsDatabase) throws UnsupportedEncodingException {

        String nodePath = idToPath(nodeId, idToPathDatabase);
        if (TextUtils.isEmpty(nodePath)) {
            return null;
        }
        TagNode tag = null;
        try {
            ValueTuple v = ValueTuple.fromNodeId(nodeId, relationsDatabase);
            if (v == null) {
                return tag;
            }
            tag = new TagNode(FilenameUtils.getBaseName(nodePath));
            TextNode textNode = new TextNode(fromUTF8BytesToString(v.value));
            tag.setChild(textNode);

            if (v.childId == null) {
                return tag;
            }

            ArrayDeque<byte[]> childTags = new ArrayDeque<>(256);
            AttributeList  attribList = new AttributeList();
            tag.setAttrList(attribList);
            Attribute attr = new Attribute("localinstanceId");
            attr.setValue(StringUtilities.bytesToHex(v.nodeId, StringUtilities.ByteFormat.RAW_TYPE));
            attribList.append(attr);

            //Walk the nextSiblingId link list
            byte[] siblingId = v.childId;
            while ((siblingId != null) && (ArrayUtils.hashCode(siblingId) != zerosHash)) {
                ValueTuple sibling = ValueTuple.fromNodeId(siblingId, relationsDatabase);
                if (sibling == null) {
                    break;
                }
                if (ArrayUtils.hashCode(sibling.childId) == attribMarkerIdHash) {
                    String path = idToPath(siblingId, idToPathDatabase);
                    String basename = FilenameUtils.getBaseName(path);
                    String valStr = fromUTF8BytesToString(sibling.value);
                    attr = new Attribute(basename);
                    attr.setValue(valStr);
                    attribList.append(attr);
                }
                else {
                    childTags.add(siblingId);
                }
                siblingId = sibling.nextSiblingId;
                if (childTags.size()>64)
                    break;
            }


            //Walk the prevSiblingId link list
            ValueTuple sibling = ValueTuple.fromNodeId(v.childId, relationsDatabase);
            if (sibling != null) {
                siblingId = sibling.prevSiblingId;
                while ((siblingId != null) && (ArrayUtils.hashCode(siblingId) != zerosHash)) {
                    sibling = ValueTuple.fromNodeId(siblingId, relationsDatabase);
                    if (sibling == null) {
                        break;
                    }
                    if (ArrayUtils.hashCode(sibling.childId) == attribMarkerIdHash) {
                        String path = idToPath(siblingId, idToPathDatabase);
                        String basename = FilenameUtils.getBaseName(path);
                        String valStr = fromUTF8BytesToString(sibling.value);
                        attr = new Attribute(basename);
                        attr.setValue(valStr);
                        attribList.append(attr);
                    }
                    else {
                        childTags.add(siblingId);
                    }
                    siblingId = sibling.prevSiblingId;
                    if (childTags.size()>64)
                        break;
                }
            }


            TreeNode childNode = tag.getChild();
            while(!childTags.isEmpty()) {
                byte[] childId = childTags.removeFirst();
                TreeNode childNodeTemp = retrieveObject(childId, idToPathDatabase, pathToIdDatabase, relationsDatabase);
                if (childNode == null) {
                    tag.setChild(childNodeTemp);
                }
                else {
                    childNode.setSibling(childNodeTemp);
                    if (childNodeTemp != null)
                        childNodeTemp.setSibPred(childNode);
                }
                childNode = childNodeTemp;
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return tag;
    }

    public static ValueTuple addChild(ValueTuple parent, TagNode newNode, DBLocal idToPathDatabase,
                                      DBLocal pathToIdDatabase, DBLocal relationsDatabase) {
        ValueTuple newParent=null;
        try {
            String parentPath = idToPath(parent.nodeId, idToPathDatabase);
            XmlUtils.ValueTuple newVtNode = storeObject(newNode, parentPath, idToPathDatabase, pathToIdDatabase, relationsDatabase);
            newParent = addChild(parent,  newVtNode, relationsDatabase);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return newParent;
    }

    public static String printNode(byte[] nodeId, DBLocal idToPathDatabase,
                                   DBLocal pathToIdDatabase, DBLocal relationsDatabase) {
        try {
            TagNode employeeNode = (TagNode) XmlUtils.retrieveObject(nodeId, idToPathDatabase, pathToIdDatabase, relationsDatabase);
            TreeToXML txml = new TreeToXML(employeeNode);
            return  txml.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNodeAttribute(TagNode node, String attributeName) {
        Attribute a = node.findAttribute(attributeName);
        if (a != null)
            return (a.getValue());
        else {
            NamedEntity n = node.findChild(attributeName);
            if (n != null) {
                TreeNode nc = n.getChild();
                if (nc instanceof TextNode) {
                    return (((TextNode) nc).getText());
                }
            }
        }
        return null;
    }

    //returns the parent the node was added to
    public static ValueTuple addChild(String parentPath, ValueTuple newNode, DBLocal relationsDatabase, DBLocal pathToIdDatabase) {
        if (TextUtils.isEmpty(parentPath)) {
            return null;
        }

        KeyIterator iter = null;
        // parentName has to be an absolute path
        String basename = FilenameUtils.getBaseName(parentPath);
        if (TextUtils.isEmpty(basename)) {
            return null;
        }
        try {
            // parentName has to be an absolute path
            if (parentPath.charAt(0) != '/')
                return null;
            byte[] parentPathId = pathToId(parentPath, pathToIdDatabase);
            String pathIdStr = new String(Hex.encodeHex(parentPathId));
            iter = relationsDatabase.findKeysIterator(pathIdStr);


            // could not find the parent node
            if (iter == null || !iter.hasNext()) {
                if (iter != null) {
                    iter.close();
                }
                return null;
            }

            //get parent Node. There can be multiple instances of the parent returned by the iter.
            //Take the first one from the list, the child is presumed to be added to the first
            byte[] parentNodeId = StringUtilities.hexToBytes(iter.next(1)[0]);
            ValueTuple parent = addChild(parentNodeId, newNode, relationsDatabase);
            return parent;
        } catch (SnappydbException e) {
            e.printStackTrace();
        } finally {
            if (iter != null)
                iter.close();
        }
        return null;
    }


    public static ValueTuple addChild(byte[] parentNodeId, ValueTuple newNode, DBLocal relationsDatabase) {
        try {
            ValueTuple parent = ValueTuple.fromNodeId(parentNodeId, relationsDatabase);
            if (addChild(parent, newNode, relationsDatabase) == null)
                return null;
            return parent;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ValueTuple addChild(ValueTuple parent, ValueTuple newNode, DBLocal relationsDatabase)
           throws UnsupportedEncodingException, SnappydbException {
        Log.v(TAG, "addChild");

        ValueTuple firstChild = ValueTuple.fromNodeId(parent.childId, relationsDatabase);

        while (ArrayUtils.hashCode(firstChild.childId) == attribMarkerIdHash) {
            firstChild = ValueTuple.fromNodeId(firstChild.prevSiblingId, relationsDatabase);
        }

        // TODO : Change made 5/9/18 (if else)
//        if (firstChild != null && firstChild.childId != null) {
//            while (ArrayUtils.hashCode(firstChild.childId) == attribMarkerIdHash) {
//                firstChild = ValueTuple.fromNodeId(firstChild.prevSiblingId, relationsDatabase);
//            }
//        } else {
//            Log.i(TAG, "First child id was null");
//        }

        newNode.prevSiblingId = firstChild.nodeId;
        newNode.nextSiblingId = firstChild.nextSiblingId;
        if (firstChild.nextSiblingId != null && (ArrayUtils.hashCode(firstChild.nextSiblingId) != zerosHash)) {
            ValueTuple nextChild = ValueTuple.fromNodeId(firstChild.nextSiblingId, relationsDatabase);
            if (nextChild != null) {
                nextChild.prevSiblingId = newNode.nodeId;
                nextChild.commitRelation(relationsDatabase);
            }
        }
        firstChild.nextSiblingId = newNode.nodeId;
        newNode.parentId = parent.nodeId;
        parent.childId = newNode.nodeId;
        newNode.commitRelation(relationsDatabase);
        firstChild.commitRelation(relationsDatabase);
        parent.commitRelation(relationsDatabase);
        return parent;
    }


    public static ValueTuple storeObject(TreeNode node, String parent,
                                     DBLocal idToPathDatabase,
                                     DBLocal pathToIdDatabase,
                                     DBLocal relationsDatabase) {
        String nodePath;
        if (TextUtils.isEmpty(parent)) {
            nodePath = "/" + node.toString(); // Ex: /Employees
        } else {
            nodePath = parent + "/" + node.toString();
        }
        ValueTuple returnValue = new ValueTuple();
        byte[] prevSiblingId = XmlUtils.zeros;
        byte[] nextSiblingId = XmlUtils.zeros;
        byte[] childId = XmlUtils.zeros;
        returnValue.nodeId = createNewInstanceId(nodePath, idToPathDatabase, pathToIdDatabase);
        returnValue.value = XmlUtils.emptyValue;
        byte[] aFinalId= null;
        byte[] avalue=null;

        String keyslog;
        if (DEBUG) keyslog = XmlUtils.printAllKeys(relationsDatabase, idToPathDatabase);
        if (DEBUG) Log.d("storeObject", keyslog);

        try {
            // This might have the information inside /Employees...
            AttributeList attrList = ((TagNode) node).getAttrList();
            if (attrList != null && attrList.hasAttributes()) {
                childId = XmlUtils.attribMarkerId;
                //for an attribute, the childId is equal to attribMarkerId,
                //the parent is the tag for that attribute. it can still have prevSiblingId and nextSiblingId
                Iterator iter = attrList.getIterator();
                Attribute attr;
                String aPath;

                int index = 0;

                //if (iter.hasNext()) {
                    attr = (Attribute) iter.next();
//                    Log.d(TAG, "Attribute from TagNode[" + index + "]: " + attr);
//                    Log.d(TAG, "Attribute value: " + attr.getValue());
                    aPath = nodePath + "/" + attr.getName();
//                    Log.d(TAG, "Path for this attr: " + aPath);

                aFinalId = createNewInstanceId(aPath, idToPathDatabase, pathToIdDatabase);
                    returnValue.childId = aFinalId;
                    avalue = fromStringToUTF8Bytes(attr.getValue());

                    do {
                        // It seems there are two attributes: full and version

                        if (iter.hasNext()) {
//                            index++;
                            attr = (Attribute) iter.next();
//                            Log.d(TAG, "Attribute from TagNode[" + index + "]: " + attr);
//                            Log.d(TAG, "Attribute value: " + attr.getValue());
                            aPath = nodePath + "/" + attr.getName();
//                            Log.d(TAG, "Path for this attr: " + aPath);
                            nextSiblingId = createNewInstanceId(aPath, idToPathDatabase, pathToIdDatabase);
                            returnValue.childId = nextSiblingId;
                        }
                        if (DEBUG) Log.d("commitRelation", "---1---");
                        makeRelation(aFinalId, returnValue.nodeId, childId, prevSiblingId, nextSiblingId, avalue, relationsDatabase);
                        prevSiblingId = aFinalId;
                        aFinalId = nextSiblingId;
                        Log.v("XmlUtils", "attr.getValue is " + attr.getValue());
                        avalue = fromStringToUTF8Bytes(attr.getValue());
                    } while (iter.hasNext());
                //} //if (iter.hasNext()) {
            }

            TreeNode child = node.getChild();
            if (child != null) {
                if (child.getType() == TreeNodeType.TEXT) {
                    returnValue.value = child.toString().getBytes(Charset.defaultCharset());
                }

                if (child.getType() == TreeNodeType.TAG) {
                    ValueTuple tempId = storeObject(child, nodePath, idToPathDatabase, pathToIdDatabase, relationsDatabase);
                    nextSiblingId = tempId.nodeId;
                    returnValue.childId = nextSiblingId;
                    if (aFinalId != null) {
                        if (DEBUG) Log.d("commitRelation", "---2---");
                        makeRelation(aFinalId, returnValue.nodeId, childId, prevSiblingId, nextSiblingId, avalue, relationsDatabase);
                        prevSiblingId = aFinalId;
                    }
                    if (tempId.childId != null) {
                        childId = tempId.childId;
                    }
                    else {
                        childId = XmlUtils.zeros;
                    }
                    aFinalId = nextSiblingId;
                    avalue = tempId.value;
                }

                do {
                    child = child.getSibling();
                    if (child != null) {
                        if (child.getType() == TreeNodeType.TEXT) {
                            returnValue.value = child.toString().getBytes(Charset.defaultCharset());
                        }
                        if (child.getType() == TreeNodeType.TAG) {
                            ValueTuple tempId = storeObject(child, nodePath, idToPathDatabase, pathToIdDatabase, relationsDatabase);
                            nextSiblingId = tempId.nodeId;
                            returnValue.childId = nextSiblingId;
                            if (aFinalId != null) {
                                if (DEBUG) Log.d("commitRelation", "---3---");
                                makeRelation(aFinalId, returnValue.nodeId, childId, prevSiblingId, nextSiblingId, avalue, relationsDatabase);
                                prevSiblingId = aFinalId;
                            }
                            if (tempId.childId != null) {
                                childId = tempId.childId;
                            }
                            else {
                                childId = XmlUtils.zeros;
                            }
                            aFinalId = nextSiblingId;
                            avalue = tempId.value;
                        }
                    }
                } while (child != null);
            }

            if (DEBUG)  {
                keyslog = XmlUtils.printAllKeys(relationsDatabase, idToPathDatabase);
                Log.d("storeObject1", keyslog);
            }
            if (aFinalId != null) {
                if (DEBUG) Log.d("commitRelation", "---4---");
                makeRelation(aFinalId, returnValue.nodeId, childId, prevSiblingId, zeros, avalue, relationsDatabase);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    //TODO this is not working... debug
    public static ArrayList<ValueTuple> getValueTuples_usingIterator(String path, DBLocal relationsDatabase, DBLocal pathToIdDatabase) {
        ArrayList<ValueTuple> valueTuples = new ArrayList<ValueTuple>();
        try {
            byte[] pathId = pathToId(path, pathToIdDatabase);
            String pathIdStr = new String(Hex.encodeHex(pathId));
            Log.d("getValueTuples", "pathIdStr=" + pathIdStr);
            KeyIterator iter = relationsDatabase.findKeysIterator(pathIdStr);
            LinkedList<String> keys = new LinkedList<>();
            while (iter.hasNext()) {
                String key=iter.next(1)[0];
                Log.d("getValueTuples", "key="+key);
                keys.add(key);
            }

            for(String k:keys) {
                byte[] finalId = relationsDatabase.getBytes(k)[0];
                String finalIdStr = new String(Hex.encodeHex(finalId));
                Log.d("getValueTuples", "finalId="+finalIdStr);
                ValueTuple v = ValueTuple.fromValue(finalId);
                v.nodeId = Hex.decodeHex(k.toCharArray());
                valueTuples.add(v);
            }
        } catch (NoSuchElementException e) {
                //we have no keys. which is ok. dont do anything
        } catch (SnappydbException e1) {
            e1.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        }
        return valueTuples;
    }

    public static ArrayList<ValueTuple> getValueTuples(String path, DBLocal relationsDatabase, DBLocal pathToIdDatabase) {
        ArrayList<ValueTuple> valueTuples = new ArrayList<ValueTuple>();
        try {
            byte[] pathId = pathToId(path, pathToIdDatabase);
            String pathIdStr = new String(Hex.encodeHex(pathId));
            //Log.d("getValueTuples", "pathIdStr="+pathIdStr);
            String[] keys = relationsDatabase.findKeys(pathIdStr);

            for(String k:keys) {
                //Log.d("getValueTuples", "key="+k);
                byte[] finalId = relationsDatabase.getBytes(k)[0];
                String finalIdStr = new String(Hex.encodeHex(finalId));
                //Log.d("getValueTuples", "finalId="+finalIdStr);
                ValueTuple v = ValueTuple.fromValue(finalId);
                v.nodeId = Hex.decodeHex(k.toCharArray());
                valueTuples.add(v);
            }
        } catch (NoSuchElementException e) {
                //we have no keys. which is ok. dont do anything
        } catch (SnappydbException e1) {
            e1.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        }
        return valueTuples;
    }

    public static ValueTuple getParent(byte[] nodeId, DBLocal relationsDatabase) throws UnsupportedEncodingException, SnappydbException {
        ValueTuple vt = ValueTuple.fromNodeId(nodeId, relationsDatabase);
        ValueTuple parent = null;
        if (vt != null && vt.parentId != null && ArrayUtils.hashCode(vt.parentId) != XmlUtils.zerosHash) {
            parent = ValueTuple.fromNodeId(vt.parentId, relationsDatabase);
        }
        return parent;
    }

    /**
     *
     * @param path : specific path to employee badge
     * @param relationsDatabase
     * @param pathToIdDatabase
     * @return map
     */
    public static HashMap<String, byte[]> getValuesAsMap(String path, DBLocal relationsDatabase, DBLocal pathToIdDatabase) {
        Log.v(TAG, "getValuesAsMap");

        try {
            Log.v(TAG, "Here");

            byte[] pathId = pathToId(path, pathToIdDatabase);
            if (pathId == null || pathId.length == 0) {
                return null;
            }
            String pathIdStr = new String(Hex.encodeHex(pathId));
            //Log.d("getValueTubles", "pathIdStr="+pathIdStr);
            String[] keys = relationsDatabase.findKeys(pathIdStr);

            if (keys == null || keys.length ==0) {
                return null;
            }

            Log.v(TAG, "Now Here");

//            Log.d(TAG, "Keys in relations database:");
//            for (String key : keys) {
//                Log.d(TAG, key);
//            }

            HashMap<String, byte[]> values = new HashMap<>(keys.length);

            // TODO : Are values getting duplicated here? Yes. Why?
            for(int ii=0; ii<keys.length; ii++) {
                Log.v(TAG, "Length of keys: " + keys.length);

                String k = keys[ii];

                Log.d("getValueTubles", "key="+k);

                byte[] finalId = relationsDatabase.getBytes(k)[0];

                String finalIdStr = new String(Hex.encodeHex(finalId));
//                Log.d("getValueTubles", "finalId="+finalIdStr);

                // TODO : I think I can use this block to convert from byte[] to string...
                ValueTuple v = ValueTuple.fromValue(finalId);
                v.nodeId = Hex.decodeHex(k.toCharArray());
                String val = fromUTF8BytesToString(v.value);

                Log.d("getValueTubles", "val="+val);

                values.put(val, v.nodeId);
            }

            Log.v(TAG, "getValuesAsMap finished?");

            return values;
        } catch (NoSuchElementException e) {
                //we have no keys. which is ok. dont do anything
        } catch (SnappydbException e1) {
            e1.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] getValues(String path, DBLocal relationsDatabase, DBLocal pathToIdDatabase) {
        try {
            byte[] pathId = pathToId(path, pathToIdDatabase);
            String pathIdStr = new String(Hex.encodeHex(pathId));
            Log.v("getValueTubles", "pathIdStr="+pathIdStr);
            String[] keys = relationsDatabase.findKeys(pathIdStr);
            String[] values = new String[keys.length];

            for(int ii=0; ii<keys.length; ii++) {
                String k = keys[ii];
                Log.v("getValueTubles", "key="+k);
                byte[] finalId = relationsDatabase.getBytes(k)[0];
                String finalIdStr = new String(Hex.encodeHex(finalId));
                //Log.d("getValueTubles", "finalId="+finalIdStr);
                ValueTuple v = ValueTuple.fromValue(finalId);
                v.nodeId = Hex.decodeHex(k.toCharArray());
                String val = fromUTF8BytesToString(v.value);
                Log.v("getValueTubles", "val="+val);
                values[ii] = val;
            }
            return values;
        } catch (NoSuchElementException e) {
                //we have no keys. which is ok. dont do anything
        } catch (SnappydbException e1) {
            e1.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }



    public static String printAllKeys(DBLocal db, DBLocal id2Name) {
        StringBuilder sb = new StringBuilder();
        try {
            KeyIterator iter = db.allKeysIterator();
            String[] keys = new String[0];
            try {
                keys = iter.next(100);
            } catch (NoSuchElementException e) {
                //we have no keys. which is ok. dont do anything
            }
            iter.close();
            for(String k:keys) {
                StringBuilder sbi = new StringBuilder();
                byte[] key = Hex.decodeHex(k.toCharArray());
                String keyPath = idToPath(key, id2Name);
                String keyStr = StringUtilities.bytesToHex(key, StringUtilities.ByteFormat.NODE_TYPE);
                byte[] finalId =db.getBytes(k)[0];

                byte[] childId = ArrayUtils.subarray(finalId, CHILD_START_BYTE, CHILD_END_BYTE+1);
                String childPath = idToPath(childId, id2Name);
                String childStr = StringUtilities.bytesToHex(childId, StringUtilities.ByteFormat.NODE_TYPE);

                byte[] parentId = ArrayUtils.subarray(finalId, PARENT_START_BYTE, PARENT_END_BYTE+1);
                String parentPath = idToPath(parentId, id2Name);
                String parentIdStr = StringUtilities.bytesToHex(parentId, StringUtilities.ByteFormat.NODE_TYPE);

                byte[] prevSiblingId = ArrayUtils.subarray(finalId, PREV_SIBLING_START_BYTE, PREV_SIBLING_END_BYTE+1);
                String prevSiblingPath = idToPath(prevSiblingId, id2Name);
                String prevSiblingStr = StringUtilities.bytesToHex(prevSiblingId, StringUtilities.ByteFormat.NODE_TYPE);

                byte[] nextSiblingId = ArrayUtils.subarray(finalId, NEXT_SIBLING_START_BYTE, NEXT_SIBLING_END_BYTE+1);
                String nextSiblingPath = idToPath(nextSiblingId, id2Name);
                String nextSiblingStr = StringUtilities.bytesToHex(nextSiblingId, StringUtilities.ByteFormat.NODE_TYPE);

                byte[] value = ArrayUtils.subarray(finalId, VALUE_START_BYTE, finalId.length);
                String valueStr = StringUtilities.bytesToHex(value, StringUtilities.ByteFormat.VALUE_TYPE);
                sbi.append("key=").append(keyStr).append("(").append(keyPath).append(")")
                        .append(",value=").append(childStr).append("(").append(childPath)      .append(")")
                        .append("|").append(parentIdStr)   .append("(").append(parentPath)     .append(")")
                        .append("|").append(prevSiblingStr).append("(").append(prevSiblingPath).append(")")
                        .append("|").append(nextSiblingStr).append("(").append(nextSiblingPath).append(")")
                        .append("|").append(valueStr);
                sb.append(sbi).append("\n");
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
