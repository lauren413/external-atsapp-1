package com.accutime.app1.businessObjects;

import android.content.Context;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.retrofit.AtsService;
import com.accutime.app1.util.PreferenceUtilities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by mkemp on 5/30/18.
 */

public class ATSPunches {

    private static final String TAG = ATSPunches.class.getSimpleName();

    private ATSPunches() {}

    static {
        // Get current values from shared prefs, if they exist.

        String workingString = PreferenceUtilities.Preferences.getWorkingBatch(AtsApplication.getContext());
        if (workingString != null && !workingString.isEmpty()) {
            workingBatch = createBatchFromString(workingString);
        }

        String toSendString = PreferenceUtilities.Preferences.getBatchToSend(AtsApplication.getContext());
        if (toSendString != null && !toSendString.isEmpty()) {
            batchToSend = createBatchFromString(toSendString);
        }
    }

    private static AtsService.Punches workingBatch;
    public static AtsService.Punches batchToSend;

    private static AtsService.Punches createNewBatch() {
//        Log.v(TAG, "createNewBatch");

        AtsService.Punches punchBatch = new AtsService.Punches();
        // TODO : These need to be customizable
        punchBatch.mode = "buffered";
        punchBatch.terminal = "1439144";
        punchBatch.tz = "America/New_York";
        // TODO : Clock serial number should be part of this Batch ID
        punchBatch.uid = UUID.randomUUID().toString();
        punchBatch.punchList = new ArrayList<>();
        return punchBatch;
    }

    public static void addPunchToBatch(Context context, AtsService.Punches.Punch punch) {
//        Log.v(TAG, "addPunchToBatch");

        if (workingBatch == null) {
            workingBatch = createNewBatch();
        }
        workingBatch.punchList.add(punch);

        saveWorkingBatch(context);
    }

    public static void switchBatch(Context context) {
//        Log.v(TAG, "switchBatch");

        if (workingBatch == null) {
            workingBatch = createNewBatch();
        }

        AtsService.Punches newBatch = createNewBatch();
        batchToSend = workingBatch;
        workingBatch = newBatch;

        saveWorkingBatch(context);
        saveBatchToSend(context);
    }

    // TODO : PROBLEM -- If I try to push a batch to the wrong server, it fails and gets lost.
    // Should be able to be fixed by just saying only clear if response is ok...

    // TODO : Instead of setting batch to send to null,
    // TODO : this should look at all saved batches and delete the one with uid = given uid

    // Maybe have a map of values with key=uid, value=batch
    // Whenever I decide to create a new batch (same as now) save the old one to this map
    // Map gets saved to shared prefs each time it is updated...

    public static void clearBatchToSend(Context context) {
//        Log.v(TAG, "clearBatchToSend");

        batchToSend = null;

        saveBatchToSend(context);
    }

    private static void saveWorkingBatch(Context context) {
//        Log.v(TAG, "saveWorkingBatch");
        if (workingBatch != null) {
            PreferenceUtilities.Preferences.setWorkingBatch(context, workingBatch.toString());
        } else {
            PreferenceUtilities.Preferences.setWorkingBatch(context, null);
        }
    }

    private static void saveBatchToSend(Context context) {
//        Log.v(TAG, "saveBatchToSend");
        if (batchToSend != null) {
            PreferenceUtilities.Preferences.setBatchToSend(context, batchToSend.toString());
        } else {
            PreferenceUtilities.Preferences.setBatchToSend(context, null);
        }
    }

    public static AtsService.Punches createBatchFromString(String batchXML) {
//        Log.v(TAG, "createBatchFromString");

        AtsService.Punches punchBatch = new AtsService.Punches();

        try {
            Document doc = stringToDom(batchXML);
            doc.getDocumentElement().normalize();

            String terminal = doc.getDocumentElement().getAttribute("terminal");
            String tz = doc.getDocumentElement().getAttribute("tz");
            String uid = doc.getDocumentElement().getAttribute("uid");
            String mode = doc.getDocumentElement().getAttribute("mode");
//            Log.i(TAG, "Root element :" + doc.getDocumentElement().getNodeName());
//            Log.i(TAG, "Terminal :" + terminal);
//            Log.i(TAG, "TZ :" + tz);
//            Log.i(TAG, "UID :" + uid);
//            Log.i(TAG, "Mode :" + mode);
            punchBatch.mode = mode;
            punchBatch.terminal = terminal;
            punchBatch.tz = tz;
            punchBatch.uid = uid;
            punchBatch.punchList = new ArrayList<>();

            NodeList nodeList = doc.getElementsByTagName("Punch");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
//                System.out.println("\nCurrent Element :" + node.getNodeName());

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String Badge = element.getElementsByTagName("Badge").item(0).getTextContent();
                    String Timestamp = element.getElementsByTagName("Timestamp").item(0).getTextContent();
                    String Action = element.getElementsByTagName("Action").item(0).getTextContent();
//                    Log.i(TAG, "Badge :" + Badge);
//                    Log.i(TAG, "Timestamp :" + Timestamp);
//                    Log.i(TAG, "Action :" + Action);

                    AtsService.Punches.Punch punch = new AtsService.Punches.Punch();
                    punch.Badge = Badge;
                    punch.Timestamp = Timestamp;
                    punch.Action = Action;
                    punchBatch.punchList.add(punch);
                }
            }

        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }

        return punchBatch;
    }

    public static Document stringToDom(String xmlSource)
            throws SAXException, ParserConfigurationException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xmlSource)));
    }
}
