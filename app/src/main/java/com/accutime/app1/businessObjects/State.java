package com.accutime.app1.businessObjects;

import android.util.Log;

import com.accutime.app1.util.Util;

import org.json.JSONObject;

import static com.accutime.app1.common.Constants.TEXT_FIELD_KEY;

/**
 * Created by mkemp on 2/16/18.
 */

public class State {

    public static final String TAG = State.class.getSimpleName();

    private static final String LAYOUT_KEY = "Layout";
    private static final String LABEL_KEY = "Label";
    private static final String ACTION_KEY = "Action";
    private static final String NEXT_STATE_KEY = "NextState";
    private static final String TIME_OUT_STATE_KEY = "TimeOutState";

    private String layout;
    private String label;
    private String action;
    private String nextState;
    private String timeOutState;

    private JSONObject thisState;

    // This could prove useful...
    private boolean thisStateExists = false;

    /**
     * Holds information relevant to this current state.
     * @param desiredState specific state from entire dld file in internal storage
     */
    public State(String profile, String desiredState) {

        thisState = Util.getJsonObjectFrom(profile, desiredState);
        if (thisState != null) {

            // Get all attributes for this state.
            this.layout = Util.getStringFromJson(thisState, LAYOUT_KEY);
            this.label = Util.getStringFromJson(thisState, LABEL_KEY);
            this.action = Util.getStringFromJson(thisState, ACTION_KEY);
            this.nextState = Util.getStringFromJson(thisState, NEXT_STATE_KEY);
            this.timeOutState = Util.getStringFromJson(thisState, TIME_OUT_STATE_KEY);

            Log.i(TAG, String.format(
                    "Current state: %s, layout: %s, label: %s, action: %s, nextState: %s, timeOutState: %s",
                    desiredState, layout, label, action, nextState, timeOutState)
            );

            setThisStateExists(true);
        }
        else
        {
            Log.e(TAG, "Could not find state: " + desiredState);
            setThisStateExists(false);
        }
    }

    public boolean thisStateExists() {
        return thisStateExists;
    }

    private void setThisStateExists(boolean exists) {
        thisStateExists = exists;
    }

    public String getLayout() {
        return layout;
    }

    public String getLabel() {
        return label;
    }

    /**
     * Gets the desired attribute from this button's json.
     * Example: Label, Icon, Action, etc
     */
    public String getAttribute(String buttonKey, String attribute) {

        JSONObject jsonForThisButton = Util.getJsonObjectFrom(thisState, buttonKey);
        String attributeValue = "";

//        if (attribute.equals("PostActionMessage")) {
//            JSONObject postActionJson = Util.getJsonObjectFrom(jsonForThisButton, attribute);
//            String successMessage = Util.getStringFromJson(postActionJson, "Success");
//            String failureMessage = Util.getStringFromJson(postActionJson, "Failure");
//            attributeValue = successMessage + ";" + failureMessage;
//        } else {
            attributeValue = Util.getStringFromJson(jsonForThisButton, attribute);
//        }

        Log.d(TAG, String.format("Got attribute %s for button %s : %s",
                attribute, buttonKey, attributeValue));

        return attributeValue;
    }

    public String getAction() {
        return action;
    }

    public String getNextState() {
        return nextState;
    }

    public String getTimeOutState() {
        return timeOutState;
    }

    public boolean containsTextField() {
        try {
            return thisState.getJSONObject(TEXT_FIELD_KEY) != null;
        } catch (Exception e) {
            return false;
        }
    }
}
