package com.accutime.app1.businessObjects;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by jpbrahma on 9/18/16.
 */
@Root
public class Employee implements Parcelable {
        @Attribute(name="badge", required=false) public String badge;
        @Attribute(name="schedule_id", required=false) public String schedule_id;
        @Attribute(name="break_schedule_id", required=false) public String break_schedule_id;
        @Attribute(name="meal_schedule_id", required=false) public String meal_schedule_id;
        @Attribute(name="supervisor_level", required=false) public String supervisor_level;
        @Attribute(name="language", required=false) public String language;
        @Attribute(name="pin_number", required=false) public String pin_number;
        @Attribute(name="fp_level", required=false) public String fp_level;
        @Element(name ="Name", required=false) public String name;
        @Element(name ="WeekToDateHours", required=false) public String weekToDateHours;
        @Element(name ="SecondPunchTimeRestrict", required=false) public int SecondPunchTimeRestrict;
        @Element(name ="FingerPrint", required=false) public FingerPrint[] fingerPrints;

    public Employee(Parcel in) {
        badge = in.readString();
        schedule_id = in.readString();
        break_schedule_id = in.readString();
        meal_schedule_id = in.readString();
        supervisor_level = in.readString();
        language = in.readString();
        pin_number = in.readString();
        fp_level = in.readString();
        name = in.readString();
        weekToDateHours = in.readString();
        SecondPunchTimeRestrict = in.readInt();
    }


    public Employee() {
    }

    public static final Creator<Employee> CREATOR = new Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel in) {
            return new Employee(in);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(badge);
        parcel.writeString(schedule_id);
        parcel.writeString(break_schedule_id);
        parcel.writeString(meal_schedule_id);
        parcel.writeString(supervisor_level);
        parcel.writeString(language);
        parcel.writeString(pin_number);
        parcel.writeString(fp_level);
        parcel.writeString(name);
        parcel.writeString(weekToDateHours);
        parcel.writeInt(SecondPunchTimeRestrict);
        parcel.writeParcelableArray(fingerPrints, 0);
    }

    public static class FingerPrint implements Parcelable {
        public int scanQuality;
        public int userId;
        public String device;
        public String template;
        public FingerPrint (int scanQuality, int userId, String device, String template){
            this.device=device;
            this.scanQuality=scanQuality;
            this.userId = userId;
            this.template = template;
        }

        protected FingerPrint(Parcel in) {
            scanQuality = in.readInt();
            userId = in.readInt();
            device = in.readString();
            template = in.readString();
        }

        public final Creator<FingerPrint> CREATOR = new Creator<FingerPrint>() {
            @Override
            public FingerPrint createFromParcel(Parcel in) {
                return new FingerPrint(in);
            }

            @Override
            public FingerPrint[] newArray(int size) {
                return new FingerPrint[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(scanQuality);
            parcel.writeInt(userId);
            parcel.writeString(device);
            parcel.writeString(template);
        }
    }
}
