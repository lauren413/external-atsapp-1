package com.accutime.app1.loadables;

/**
 * Created by mkemp on 2/19/18.
 */

import android.util.Log;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.activities.BaseActivity;
import com.accutime.app1.loader.Loader;
import com.accutime.app1.retrofit.DeviceInterface;
import com.accutime.app1.retrofit.RestAPIs;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeviceDiscoverLoadable implements Loader.Loadable {

    private final String TAG = this.getClass().getSimpleName();
    private BaseActivity baseActivity;

    /**
     * Finds connected hardware devices. Not just fingerprint readers!
     */
    public DeviceDiscoverLoadable(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Override public void cancelLoad() { }
    @Override public boolean isLoadCanceled() { return false; }
    @Override
    public void load() throws IOException, InterruptedException {
        Log.v(TAG, "Connecting to Discover service");
//        DeviceInterface deviceInterface = baseActivity.getDeviceInterface();
        DeviceInterface deviceInterface = RestAPIs.deviceInterface;
        deviceInterface.discoverDevices().enqueue(discoverDevicesCallback);
    }

    // TODO : Why is this in the DeviceInterface interface?
    private Callback<DeviceInterface.DiscoverResponse> discoverDevicesCallback =
            new Callback<DeviceInterface.DiscoverResponse>() {

        @Override
        public void onResponse(Call<DeviceInterface.DiscoverResponse> call,
                               Response<DeviceInterface.DiscoverResponse> response) {

//            AtsApplication atsApplication = baseActivity.getAtsApplication();
//            AtsApplication atsApplication = baseActivity.getAtsApplication();
            AtsApplication atsApplication = (AtsApplication) baseActivity.getApplicationContext();
            atsApplication.setDiscoverConfig(response.body());
            DeviceInterface.DiscoverResponse.Device[] devices = response.body().devices;

            Log.v(TAG, "Number of devices: " + devices.length);
            for (DeviceInterface.DiscoverResponse.Device d : devices) {
                Log.d(TAG, "Found device: " + d.device);
            }
        }

        @Override
        public void onFailure(Call<DeviceInterface.DiscoverResponse> call, Throwable t) {
            Log.e(TAG, "Discover devices got an error. Throwable = ", t);
        }
    };


}