//package com.accutime.app1.loadables;
//
///**
// * Created by mkemp on 2/19/18.
// */
//
//import android.content.Context;
//import android.util.Log;
//
//import com.accutime.app1.Application.AtsApplication;
//import com.accutime.app1.util.Measurement;
//import com.accutime.app1.loader.Loader;
//import com.accutime.app1.retrofit.RestAPIs;
//import com.accutime.app1.util.PreferenceUtilities;
//import com.accutime.app1.util.Util;
//import com.accutime.app1.xmlparser.Attribute;
//import com.accutime.app1.xmlparser.AttributeList;
//import com.accutime.app1.xmlparser.TagNode;
//import com.accutime.app1.xmlparser.TreeBuilder;
//import com.accutime.app1.xmlparser.TreeNode;
//import com.accutime.app1.xmlparser.TreeToXML;
//import com.accutime.app1.xmlparser.XmlUtils;
//
//import java.io.IOException;
//import java.io.StringReader;
//import java.util.Iterator;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//
//public class EmployeesLoadable implements Loader.Loadable {
//
//    private final String TAG = this.getClass().getSimpleName();
//
//    // TODO : Start in debug mode.
//    private boolean DEBUG = false;
//    private final boolean ADD_LOCAL_EMPLOYEES = false;
//
//    private Context context;
//
//    /**
//     * Loads employee data from Engine XML
//     */
//    public EmployeesLoadable(Context context) {
//        this.context = context;
//    }
//
//    @Override public void cancelLoad() { }
//    @Override public boolean isLoadCanceled() {
//        return false;
//    }
//    @Override public void load() throws IOException, InterruptedException {
//        Log.d(TAG, "Downloading Employee list...");
//
//        // Invoke atsService.heartbeatRawCallback()
//        startHeartbeatCallback();
//
//        // Invoke atsService.getEmployeesRaw1() - if we don't already have them
//        // TODO
////            if (!PreferenceUtilities.Preferences.getHasEmployees(context)) {
//        getEmployeeList();
////            }
//    }
//
//    /**
//     * Get heartbeatRawCallback from Engine XML.
//     */
//    private void startHeartbeatCallback() {
//        Log.v(TAG, "startHeartbeatCallback");
//
//        final Measurement heartbeatMeasurement = new Measurement();
//
//        // Get start time to measure how long this takes.
//        heartbeatMeasurement.start();
//
//        // Get a heartbeat.
////        Call<TagNode> heartbeatCall = BaseActivity.getAtsService().heartbeatRawCallback();
//        Call<TagNode> heartbeatCall = RestAPIs.atsService.heartbeatRawCallback();
//
//        heartbeatCall.enqueue(new Callback<TagNode>() {
//            @Override
//            public void onResponse(Call<TagNode> call, Response<TagNode> response) {
//                heartbeatMeasurement.end();
//                Log.d(TAG, "Got heartbeat." +
//                        " Time = " + heartbeatMeasurement.end +
//                        ", Diff = " + heartbeatMeasurement.diff() +
//                        ", Avg = " + heartbeatMeasurement.avg);
//
//                // TODO : Debugging
//                Log.d(TAG, "Heartbeat Response: " + response);
//                Log.d(TAG, "Heartbeat Response Body: " + response.body());
//                Log.d(TAG, "Heartbeat Response Body List: " + response.body().getAttrList());
//
//
////
////
////                // Heartbeat doesn't have any attributes
////                AttributeList attrList = response.body().getAttrList();
////                if (attrList != null && attrList.hasAttributes()) {
////                    String nodePath = "/" + response.body().toString();
////                    Iterator iter = attrList.getIterator();
////                    Attribute attr;
////                    String aPath;
////
////                    int index = 0;
////
////                    do {
////                        attr = (Attribute) iter.next();
////                        Log.d(TAG, "Attribute from TagNode[" + index + "]: " + attr);
////                        Log.d(TAG, "Attribute value: " + attr.getValue());
////                        aPath = nodePath + "/" + attr.getName();
////                        Log.d(TAG, "Path for this attr: " + aPath);
////                        index++;
////                    } while (iter.hasNext());
////                } else {
////                    Log.e(TAG, "Attribute list is empty");
////                }
////
////
////
////
////
////                AtsApplication atsApplication = baseActivity.getAtsApplication();
////                XmlUtils.ValueTuple rootNode = null;
////
////                try {
////                    // TODO : This tries to store response.body and throws a null pointer exception
////                    rootNode = atsApplication.storeObject(response.body(), "");
////                    rootNode.commitRelation(atsApplication.getDbRelations());
////                    atsApplication.storeRootNodeName(rootNode);
////
////                    TreeNode tnode = XmlUtils.retrieveObject(rootNode.nodeId,
////                            atsApplication.getDbIdToPath(),
////                            atsApplication.getDbPathToId(),
////                            atsApplication.getDbRelations()
////                    );
////
////                    // Heartbeat does have XML
////                    if (tnode != null) {
////                        TreeToXML txml = new TreeToXML(tnode);
////                        Util.Log.d(TAG, "Back to xml: " + txml);
////                    }
////                } catch (SnappydbException e) {
////                    e.printStackTrace();
////                } catch (UnsupportedEncodingException e) {
////                    e.printStackTrace();
////                }
//            }
//
//            @Override
//            public void onFailure(Call<TagNode> call, Throwable t) {
//                heartbeatMeasurement.end();
//                Log.e(TAG, "Failed to get heartbeat." +
//                        " Time = " + heartbeatMeasurement.end +
//                        ", Diff = " + heartbeatMeasurement.diff() +
//                        ", Avg = " + heartbeatMeasurement.avg);
//                Log.e(TAG, "Error = ", t);
//            }
//        });
//    }
//
//    /**
//     * Get employees from Engine XML.
//     */
//    private void getEmployeeList() {
//        Log.v(TAG, "getEmployeeList");
//
//        final Measurement getEmployeeMeasurement = new Measurement();
//
//        getEmployeeMeasurement.start();
//
////        Call<TagNode> employeeListCall = BaseActivity.getAtsService().getEmployeesRaw1();
//        Call<TagNode> employeeListCall = RestAPIs.atsService.getEmployeesRaw1();
//        employeeListCall.enqueue (new Callback<TagNode>() {
//            @Override
//            public void onResponse(Call<TagNode> call, final Response<TagNode> response) {
//                getEmployeeMeasurement.end();
//                Log.d(TAG, "Got employee list. Times = " +
//                        getEmployeeMeasurement.start + " - " + getEmployeeMeasurement.end +
//                        ", Diff = " + getEmployeeMeasurement.diff() +
//                        ", Avg =" + getEmployeeMeasurement.avg);
//
//                // TODO : Debug
//                // Not debug mode, this is null.
//                // Aso null in debug mode.
//                // TODO Conclusion : Store employees runnable must always fall through
//                // In debug mode and not it gets the employees successfully..
//                // But this employee list body is null. So is it not actually successful?
//                // I'm making the call to get employees from XML, but it fails.
//
//                Log.d(TAG, "Response: " + response);
//                if (response == null) {
//                    Log.d(TAG, "Get Employees response is null!");
//                    return;
//                } else {
//                    Log.v(TAG, "Get Employees response string is " + response.toString());
//                    Log.v(TAG, "Get Employees response body is " + response.body());
//                    Log.v(TAG, "Type: " + response.body().getClass().getName());
//                    if (response.body() == null) {
//                        Log.d(TAG, "Get Employees response body is null!");
//                        return;
//                    }
//                }
//
//                TagNode tagNode = response.body();
//
//                Log.v(TAG, "Response string: " + tagNode.toString());
//                Log.v(TAG, "Response name: " + tagNode.getName());
//                Log.v(TAG, "Response name attribute: " + tagNode.findAttribute("name"));
//                Log.v(TAG, "Response child name: " + tagNode.findChild("name"));
//                Log.v(TAG, "Response children: " + tagNode.findChildren("FingerPrint"));
////                for (NamedEntity namedEntity : tagNode.findChildren("FingerPrint")) {
////                    Log.i(TAG, "\tChild fingerprint: " + namedEntity);
////                    Log.i(TAG, "\t\tChild fingerprint string: " + namedEntity.toString());
////                    Log.i(TAG, "\t\tChild fingerprint name: " + namedEntity.getName());
////                }
////                Log.i(TAG, "Response attribute list: " + tagNode.getAttrList());
////                Log.i(TAG, "Response attribute list iterator: " + tagNode.getAttrList().getIterator());
////                while (tagNode.getAttrList().getIterator().hasNext()) {
////                    Attribute i = (Attribute) tagNode.getAttrList().getIterator().next();
//////                    Log.i(TAG, "\tNext: " + i);
////                    Log.i(TAG, "\tNext type: " + i.getAttrType());
////                    Log.i(TAG, "\tNext value: " + i.getValue());
////                }
//
//
//                AttributeList attrList = response.body().getAttrList();
//                if (attrList != null && attrList.hasAttributes()) {
//                    String nodePath = "/" + response.body().toString();
//                    Iterator iter = attrList.getIterator();
//                    Attribute attr;
//                    String aPath;
//
//                    int index = 0;
//
//                    do {
//                        attr = (Attribute) iter.next();
//                        Log.d(TAG, "Attribute from TagNode[" + index + "]: " + attr);
//                        Log.d(TAG, "Attribute value: " + attr.getValue());
//                        aPath = nodePath + "/" + attr.getName();
//                        Log.d(TAG, "Path for this attr: " + aPath);
//                        index++;
//                    } while (iter.hasNext());
//                }
//
////                Log.d(TAG, "Local Instance ID: " + tagNode.findAttribute("localinstanceId").getValue());
//                Log.d(TAG, "Full: " + tagNode.findAttribute("full").getValue());
//                Log.d(TAG, "Version: " + tagNode.findAttribute("version").getValue());
//
//
//                // Start a thread to store employee list.
//                Runnable runnable = new StoreEmployeesRunnable(response);
//                new Thread(runnable).start();
//            }
//
//            //public void failure(RetrofitError error) {
//            @Override
//            public void onFailure(Call<TagNode> call, Throwable t) {
//                getEmployeeMeasurement.end();
//                Log.e(TAG, "Failed getting employee list." +
//                        " Finish time = " + getEmployeeMeasurement.end +
//                        ", Diff = " + getEmployeeMeasurement.diff() +
//                        ", Avg = " + getEmployeeMeasurement.avg +
//                        ", Error = ", t);
//
//                PreferenceUtilities.Preferences.setHasEmployees(context, false);
//            }
//        });
//    }
//
//    /**
//     * This class starts a new thread to get/add employee data.
//     */
//    class StoreEmployeesRunnable implements Runnable {
//
//        private final Response<TagNode> response;
//
//        StoreEmployeesRunnable(Response<TagNode> response) {
//            this.response = response;
//        }
//
//        @Override
//        public void run() {
//
//            Measurement storeEmployeeMeasurement = new Measurement();
//
//            AtsApplication atsApplication = (AtsApplication) context.getApplicationContext();
//
//            // Start measuring time again.
//            storeEmployeeMeasurement.start();
//
//            XmlUtils.ValueTuple rootNode = null;
//            try {
//
//                // We're in debug mode.
//                if (DEBUG) {
//                    Log.d(TAG, "In Debug mode");
//
//                    String testEmployees =
//                            "<Employees>" +
//                                "  <Employee badge=\"900000212\" supervisor_level=\"1\" " +
//                                "        schedule_id=\"3479\" break_schedule_id=\"3479\"" +
//                                "        meal_schedule_id=\"3479\" language=\"\"" +
//                                "        >\n" +
//
//                                    "    <Name>William Bailey</Name>\n" +
//                                    "    <WeekToDateHours>0</WeekToDateHours>\n" +
//                                    "    <SecondPunchTimeRestrict>1</SecondPunchTimeRestrict>\n" +
//
//                                "  </Employee>\n" +
//                            "</Employees>";
//
//                    TreeBuilder builder = new TreeBuilder();
//                    StringReader stringReader = new StringReader(testEmployees);
//                    TreeNode localEmployeesNode = builder.parseXML(stringReader); // should be like response.body()
//
//                    if (localEmployeesNode instanceof TagNode) {
//                        Log.d(TAG, "localEmployeesNode is instanceof TagNode");
//
//                        // Store employee list and record time.
//                        // TODO : This doesn't store response.body, so it doesn't get a npe.
//                        rootNode = atsApplication.storeObject(
//                                (TagNode) localEmployeesNode, ""
//                        );
//
//                        rootNode.commitRelation(atsApplication.getDbRelations());
//                        atsApplication.storeRootNodeName(rootNode);
//                        storeEmployeeMeasurement.end();
//                        Log.d(TAG, "Stored employee list. Time taken : " +
//                                storeEmployeeMeasurement.show()
//                        );
//
//                        TreeNode tnode = XmlUtils.retrieveObject(rootNode.nodeId,
//                                atsApplication.getDbIdToPath(),
//                                atsApplication.getDbPathToId(),
//                                atsApplication.getDbRelations()
//                        );
//
//                        if (tnode != null) {
//                            TreeToXML txml = new TreeToXML(tnode);
//                            Util.Log.d(TAG, "Back to xml: " + txml);
//                        }
//                    }
//                } //end debug
//
//
//                // Not in debug mode.
//                else {
//                    // Store employee list and record time.
//                    Log.i(TAG, "Response body: " + response.body());
//                    Log.i(TAG, "\n\nSTORING EMPLOYEES FROM SERVER\n\n");
//
//                    // TODO : This tries to store response.body and throws a null pointer exception
//                    rootNode = atsApplication.storeObject(response.body(), "");
//                    rootNode.commitRelation(atsApplication.getDbRelations());
//                    atsApplication.storeRootNodeName(rootNode);
//                    storeEmployeeMeasurement.end();
//                    Log.d(TAG, "Stored employee list. Time taken : " + storeEmployeeMeasurement.show());
//
//                    TreeNode tnode = XmlUtils.retrieveObject(rootNode.nodeId,
//                            atsApplication.getDbIdToPath(),
//                            atsApplication.getDbPathToId(),
//                            atsApplication.getDbRelations()
//                    );
//
//                    if (tnode != null) {
//                        TreeToXML txml = new TreeToXML(tnode);
//                        Util.Log.d(TAG, "Back to xml: " + txml);
//                    }
//                }
//
//
//                // Debug
//                if (ADD_LOCAL_EMPLOYEES) {
//
//                    String[] localEmployees = getLocalEmployees();
//
//                    // Store local employees.
//                    // TODO : Why is this starting at 1? That skips jagat.
//                    for (int i = 1; i <= localEmployees.length; i++) { // TODO: Was <=
//                        StringReader thisEmployee = new StringReader(localEmployees[i]);
//                        Log.v(TAG, "This employee: " + thisEmployee.toString());
//
//                        TreeNode localEmp = new TreeBuilder().parseXML(thisEmployee);
//                        Log.v(TAG, "Local employee: " + localEmp);
//
//                        if (localEmp instanceof TagNode) {
//                            // TODO : This is storing under /Employees...
//                            XmlUtils.ValueTuple newEmp = atsApplication.storeObject(
//                                    (TagNode) localEmp,
//                                    "/Employees"
//                            );
//                            Log.v(TAG, "New employee: " + newEmp);
//
//                            // This gives null pointer? But not always. At least not the first time.
//                            XmlUtils.ValueTuple parent = XmlUtils.addChild(
//                                    rootNode,
//                                    newEmp,
//                                    atsApplication.getDbRelations()
//                            );
//                        }
//                    }
//
////                    if (DEBUG) {
//
//                        // Print all keys.
//                        String keyslog = XmlUtils.printAllKeys(
//                                atsApplication.getDbRelations(),
//                                atsApplication.getDbIdToPath()
//                        );
//                        Log.v(TAG, "Keys Log: \n" + keyslog);
//
//                        // Root node should be employee list?
//                        TreeNode tnode = null;
//                        if (rootNode != null) {
//                            tnode = XmlUtils.retrieveObject(
//                                    rootNode.nodeId,
//                                    atsApplication.getDbIdToPath(),
//                                    atsApplication.getDbPathToId(),
//                                    atsApplication.getDbRelations()
//                            );
//                        }
//
//                        if (tnode != null) {
//                            TreeToXML txml = new TreeToXML(tnode);
//                            String txmlString = txml.toString();
//
//                            // This shows each employee once.
//                            Util.Log.d(TAG, "Back to xml (should be all stored emps):\n" + txmlString);
//
//                        }
////                    }
//                }
//
//                AtsApplication.badgeNodes = atsApplication.getKeyValuePairsForNode(
//                        "/Employees/Employee/badge"
//                );
//
//                Log.v(TAG, "Badge Nodes: " + AtsApplication.badgeNodes.toString());
//                for (String key : AtsApplication.badgeNodes.keySet()) {
//                    Log.v(TAG, "Key: " + key);
//                }
//
//                // Success
//                PreferenceUtilities.Preferences.setHasEmployees(context, true);
//
//
//
//                // Print all keys.
////                if (DEBUG) {
////                    String str = XmlUtils.printAllKeys(
////                            atsApplication.getDbRelations(),
////                            atsApplication.getDbIdToPath()
////                    );
////                    Log.v(TAG, "Print All Keys:\n" + str);
//
////                    Log.v(TAG, "Badge? " + str.substring(str.indexOf("badge")));
////                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                storeEmployeeMeasurement.end();
//                Log.d(TAG, "Got exception while storing employee list. Finish time = " + storeEmployeeMeasurement.end +
//                        ", Diff = " + storeEmployeeMeasurement.diff() +
//                        ", Avg = " + storeEmployeeMeasurement.avg);
//            }
//        }
//    }
//
//    /**
//     * Test employees.
//     * For debugging.
//     */
//    private String[] getLocalEmployees() {
//
//         String localEmployee1 =
//                "<Employee badge=\"33445566\" supervisor_level=\"2\"" +
//                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                        "meal_schedule_id=\"12123\" language=\"english\"" +
//                        " > " +
//                        "<Name>Jagat Brahma</Name> " +
//                        "<WeekToDateHours>0</WeekToDateHours> "+
//                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                        "</Employee> " ;
//        String localEmployee2 =
//                "<Employee badge=\"22334455\" supervisor_level=\"9\"" +
//                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                        "meal_schedule_id=\"12123\" language=\"english\"" +
//                        " > " +
//                        "<Name>Carlos Bernal</Name> " +
//                        "<WeekToDateHours>0</WeekToDateHours> "+
//                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                        "</Employee> " ;
//        String localEmployee3 =
//                "<Employee badge=\"11223344\" supervisor_level=\"9\"" +
//                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                        "meal_schedule_id=\"12123\" language=\"english\"" +
//                        " > " +
//                        "<Name>Jean-Pierre Van de Capelle</Name> " +
//                        "<WeekToDateHours>0</WeekToDateHours> "+
//                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                        "</Employee> " ;
//        String localEmployee4 =
//                "<Employee badge=\"44556677\" supervisor_level=\"3\"" +
//                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                        "meal_schedule_id=\"12123\" language=\"english\"" +
//                        " > " +
//                        "<Name>Asha Hemingway</Name> " +
//                        "<WeekToDateHours>0</WeekToDateHours> "+
//                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                        "</Employee> " ;
//        String localEmployee5 =
//                "<Employee badge=\"55667788\" supervisor_level=\"4\"" +
//                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
//                        "meal_schedule_id=\"12123\" language=\"english\"" +
//                        " > " +
//                        "<Name>Ryan McColgan</Name> " +
//                        "<WeekToDateHours>0</WeekToDateHours> "+
//                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
//                        "</Employee> " ;
//
//        return new String[]{
//                localEmployee1,
//                localEmployee2,
//                localEmployee3,
//                localEmployee4,
//                localEmployee5
//        };
//    }
//}
