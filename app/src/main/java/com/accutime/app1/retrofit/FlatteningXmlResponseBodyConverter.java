package com.accutime.app1.retrofit;


import com.accutime.app1.xmlparser.TreeBuilder;
import com.accutime.app1.xmlparser.TreeNode;

import java.io.IOException;
import java.io.InputStreamReader;

import okhttp3.ResponseBody;
import retrofit2.Converter;

final class FlatteningXmlResponseBodyConverter implements Converter<ResponseBody, TreeNode> {
    private final String TAG=FlatteningXmlResponseBodyConverter.class.getSimpleName();
    private final boolean strict;

    FlatteningXmlResponseBodyConverter(boolean strict) {
        this.strict = strict;
    }

    @Override public TreeNode convert(ResponseBody value) throws IOException {
        try {
            TreeNode root;
            TreeBuilder builder = new TreeBuilder();

            //String respStr = StringUtilities.convertStreamToString(value.byteStream());
            //Util.Log.d(TAG, "respStr="+respStr);
            //StringReader stringReader = new StringReader(respStr);
            //root = builder.parseXML(stringReader);

            InputStreamReader reader = new InputStreamReader(value.byteStream());
            root = builder.parseXML(reader);

            //T read = serializer.read(cls, value.byteStream(), strict);
            if (root == null) {
                throw new IllegalStateException("Could not deserialize body as TreeNode");
            }
            return root;
        } catch (RuntimeException | IOException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            value.close();
        }
    }
}