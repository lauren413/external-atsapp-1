package com.accutime.app1.retrofit;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Streaming;

/**
 * Created by jpbrahma on 3/12/15.
 */
public interface RestInterface {
    @GET("/prox?mode=stream")
    @Streaming
    void getProxStream(Callback<Response> cb);

    @GET("/prox?mode=stream")
    void getProxOneShot(Callback<Response> cb);

    @Streaming
    @GET("/keyboard?mode=stream")
    @Headers("Accept: text/event-stream")
    Call<ResponseBody> getKeyboardStream();

    @GET("/keyboard?mode=oneshot")
    void getKeyboardOneshot(Callback<Response> cb);
}
