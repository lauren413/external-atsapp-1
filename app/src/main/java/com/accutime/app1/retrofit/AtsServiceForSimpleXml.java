package com.accutime.app1.retrofit;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by jpbrahma on 3/12/15.
 */
public interface AtsServiceForSimpleXml {


    /*
    @GET("/ats/biometrics/Lumidigm_Mercury_M300/{terminalId}/list/{versionId}/947 HTTP/1.1" 200 37435
            172.30.2.56 - - [13/May/2016:12:58:39 -0400] "GET /ats/biometrics/Lumidigm_Mercury_M300/1604247/item/400200/7 HTTP/1.1" 200 7745
            */
    @GET("/ats/heartbeat")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
         "User-Agent: UCS-XML/2.0 Universal"
    })
    void heartbeat(Callback<Heartbeat> cb);

    @GET("/ats/heartbeat")
    @Headers({
            //"Authorization: ATS 1439144:PASS",
            "Authorization: ATS 123456:PASS",
            //"Authorization: ATS 000001325575:PASS",
            "User-Agent: UCS-XML/2.0 Universal"
    })
    void heartbeatRaw(Callback<Response> cb);


    @GET("/ats/heartbeat")
    @Headers({
            //"Authorization: ATS 1439144:PASS",
            "Authorization: ATS 123456:PASS",
            //"Authorization: ATS 000001325575:PASS",
            "User-Agent: UCS-XML/2.0 Universal"
    })
    void heartbeatRaw1(Callback<byte[]> cb);

    public static class Heartbeat {
        public static class Password {
            @Element(name="PASS", required=false)
            public static String pass;
        }
        public static class Credentials {
            @Element(name="Password", required=false)
            public static Password password;
        }
        public static class Version {
            @Element(name="Dld", required=false)
            public static String Dld;
            @Element(name="Config", required = false)
            public static String Config;
            @Element(name="Flash", required = false)
            public static String Flash;
            @Element(name="Employee", required = true)
            public static String Employee;
            @Element(name="Schedule", required = true)
            public static int Schedule;
            @Element(name="AppTables")
            public static int AppTables;
            @Element(name="Biometrics", required = false)
            public static int Biometrics;
            @Element(name="Tasks", required = false)
            public static int Tasks;
        }

        @Element(name="Version")
        public Version version;

        @Element(name="Credentials", required=false)
        public Credentials credentials;
    }


    /* <?xml version="1.0" encoding="utf-8"?>
       <Punches terminal="192345" tz="America/New_York" uid="f81d4fae-7dec-11d0-a765-00a0c91e6bf6" mode=buffered>
       <!-- Simple Punch with language with supervisor override -->
       <Punch>
       <Badge>123456789</Badge>
       <Timestamp>2009-09-04T14:23:35.000000+0600</Timestamp>
       <Action>IN</Action>
       </Punch>
       </Punches> */
    @Root(name="Punches")
    public class Punches {
        @Attribute(name="terminal", required=true) public String terminal;
        @Attribute(name="tz", required=true) public String tz;
        @Attribute(name="uid", required=true) public String uid;
        @Attribute(name="mode", required=false) public String mode;
        public static class Punch {
            @Element(name="Badge", required=true)
            public String Badge;
            @Element(name="Timestamp", required = true)
            public String Timestamp;
            @Element(name="Action", required = true)
            public String Action;
        }
        @Element(name="Punch", required=true)
        public Punch punch;
    }

    @POST("/ats/punch")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
         "User-Agent: UCS-XML/2.0 Universal"
    })
    void sendPunch(@Body Punches punches, Callback<Response> cb);



    @GET("/ats/employees")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
         "User-Agent: UCS-XML/2.0 Universal"
    })
    void getEmployees(Callback<Employees> cb);


    @GET("/ats/employees")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
         "User-Agent: UCS-XML/2.0 Universal"
    })
    void getEmployeesRaw(Callback<Response> cb);


    @GET("/ats/employees")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
         "User-Agent: UCS-XML/2.0 Universal"
    })
    void getEmployeesRaw1(Callback<byte[]> cb);


    @Root
    public static class Employees {
        @Attribute(name="full", required=false) public boolean full;
        @Attribute(name="version", required=false) public String version;
        @ElementList(inline=true, entry="Employee", required=false) public List<Employee> list;
    }

    @Root
    public static class Employee {
        @Attribute(name="badge", required=false) public int badge;
        @Attribute(name="schedule_id", required=false) public int schedule_id;
        @Attribute(name="break_schedule_id", required=false) public int break_schedule_id;
        @Attribute(name="meal_schedule_id", required=false) public int meal_schedule_id;
        @Attribute(name="supervisor_level", required=false) public int supervisor_level;
        @Attribute(name="language", required=false) public String language;
        @Attribute(name="pin_number", required=false) public String pin_number;
        @Attribute(name="fp_level", required=false) public String fp_level;
        @Element(name ="Name", required=false) public String name;
        @Element(name ="WeekToDateHours", required=false) public String weekToDateHours;
        @Element(name ="SecondPunchTimeRestrict", required=false) public int SecondPunchTimeRestrict;
    }

    /*
    <Employee badge="1a003" schedule_id="11504" break_schedule_id="11504"
     meal_schedule_id="11504" supervisor_level="1" language="">
    <Name>Oliver Reynolds</Name>
    <WeekToDateHours>0</WeekToDateHours>
    <SecondPunchTimeRestrict>1</SecondPunchTimeRestrict>
    </Employee>
    */
}
