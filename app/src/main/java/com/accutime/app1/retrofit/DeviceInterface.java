package com.accutime.app1.retrofit;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by jpbrahma on 3/12/15.
 *
 * Notes from Michael:
 * What does @PUT do? Is this to communicate with the rest api?
 * And @Get?
 */
public interface DeviceInterface {

    @PUT("/suprema/identify/")
    Call<ResponseBody> supremaIdentify();
    public class SupremaResponseIdentify {
        public String device;
        public String[] packets;
        public String errorCode;
        public String errorString;
        public String enrolledUserId;
        public String enrolledSubId;
    }

    @PUT("/suprema/verify/")
    Call<ResponseBody> lumidigmVerify(@Query("templateToVerify") String template);
    public class LumidigmResponseVerify {
        public String device;
        public String[] packets;
        public String errorCode;
        public String errorString;
    }


    @PUT("/lumidigm/match")
    Call<ResponseBody> lumidigmMatchTemplates(@Query("firstTemplate") String template1, @Query("secondTemplate") String template2);


    @PUT("/suprema/readImage")
    Call<SupremaResponse> supremaReadImage();

    @PUT("/suprema/enrollOnce")
    Call<ResponseBody> supremaEnrollOnce();
    public class SupremaResponse {
        public String device;
        public String[] packets;
        public String errorCode;
        public String errorString;
    }

    @PUT("/lumidigm/armTrigger")
    Call<ResponseBody> lumidigmArmTrigger(@Query("triggerType") String triggerType);

    @PUT("/lumidigm/armTrigger")
    LumidigmResponse lumidigmArmTrigger_old(@Query("triggerType") String triggerType);
    public class LumidigmResponse {
        public String device;
        public String[] packets;
        public String status;
        public int acqStatus;
        public int score;
        public String errorCode;
        public String errorString;
    }

    @PUT("/lumidigm/getAcqStatus")
    Call<ResponseBody> lumidigmGetAcqStatus();

    @PUT("/lumidigm/getAcqStatus")
    LumidigmResponse lumidigmGetAcqStatus_old();

    @PUT("/lumidigm/getCompositeImage")
    void lumidigmGetCompositeImage();

    @PUT("/lumidigm/getTemplate")
    Call<ResponseBody> lumidigmGetTemplate();

    @PUT("/lumidigm/getTemplate")
    LumidigmResponse lumidigmGetTemplate_old();

    @PUT("/lumidigm/match")
    void lumidigmMatchTemplates_old(@Query("firstTemplate") String template1, @Query("secondTemplate") String template2);

    @PUT("/lumidigm/verify")
    void lumidigmVerifyTemplate(@Query("temp1ate") String template);

    @GET("/discover")
    Call<DiscoverResponse> discoverDevices1();

    @GET("/discover")
    Call<DiscoverResponse> discoverDevices();
    public class DiscoverResponse {
        public String device;
        public class Device {
            public String device;
        }
        public Device[] devices;
    }





    //////////// NEW API /////////////////////

    @PUT("Biometric/CaptureTemplate")
    Call<ResponseBody> CaptureTemplate(@Query("Badge") String badgeNumberString);

    @PUT("Biometric/VerifyAgainstTemplateList")
    Call<ResponseBody> VerifyAgainstTemplateList(@Query("Count") String templateCountString, @Query("Templates") String templateB64list);

    @PUT("Biometric/PushTemplates")
    Call<ResponseBody> PushTemplates(@Query("Count") String templateCountString, @Query("Templates") String templateB64list);

    @PUT("Biometric/PullTemplates")
    Call<ResponseBody> PullTemplates(@Query("Count") String templateCountString, @Query("BadgeList") String badgeList);

    @PUT("Biometric/DeleteTemplates")
    Call<ResponseBody> DeleteTemplates(@Query("Count") String templateCountString, @Query("BadgeList") String badgeList);

    @PUT("Biometric/DeleteAllTemplates")
    Call<ResponseBody> DeleteAllTemplates();

    @PUT("Biometric/SetSensorMode")
    Call<ResponseBody> SetSensorMode(@Query("Mode") String newMode);

    @PUT("Biometric/GetSensorMode")
    Call<ResponseBody> GetSensorMode();

    @PUT("Biometric/GetIdentifyEvent")
    Call<ResponseBody> GetIdentifyEvent();

    @PUT("Biometric/GetTemplateCount")
    Call<ResponseBody> GetTemplateCount();

    @PUT("Biometric/GetTemplateLimit")
    Call<ResponseBody> GetTemplateLimit();

    ////////////////////////////////////////////////
}
