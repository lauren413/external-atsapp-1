package com.accutime.app1.retrofit;

import android.util.Log;

import com.accutime.app1.common.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by mkemp on 5/17/18.
 */

public class RestAPIs {

    private static String TAG = RestAPIs.class.getSimpleName();

    public static AtsService atsService;
    public static DeviceInterface deviceInterface;
    public static RestInterface restInterface = null;

    static {
        createAtsService();
        createFingerprintService();
    }

    private static void createAtsService() {
        Log.v(TAG, "createAtsService");

        if (atsService == null) {
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .readTimeout(15000, TimeUnit.MILLISECONDS)
                    .writeTimeout(15000, TimeUnit.MILLISECONDS)
                    .build();

            Retrofit restAdapterRaw = new Retrofit.Builder()
                    .baseUrl(Constants.HOST_URL)
                    //.addConverterFactory(SimpleXmlConverterFactory.create())
                    .addConverterFactory(FlatteningXmlConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            atsService = restAdapterRaw.create(AtsService.class);
        }
    }

    private static void createFingerprintService() {
        Log.v(TAG, "createFingerprintService");

        if (deviceInterface == null) {
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .readTimeout(15000, TimeUnit.MILLISECONDS)
                    .writeTimeout(15000, TimeUnit.MILLISECONDS)
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.getDeviceIpAddress())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            deviceInterface = retrofit.create(DeviceInterface.class);
        }
    }

//    public void createDeviceRestService() {
//        Log.d(TAG, "createDeviceRestService");
//
//        if (restInterface == null) {
//            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                    .readTimeout(Integer.MAX_VALUE, TimeUnit.MILLISECONDS)
//                    .writeTimeout(Integer.MAX_VALUE, TimeUnit.MILLISECONDS)
//                    .build();
//            Retrofit deviceAdapter = new Retrofit.Builder()
//                    .baseUrl(Constants.getDeviceIpAddress())
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .addConverterFactory(ScalarsConverterFactory.create())
//                    .client(okHttpClient)
//                    .build();
////            Log.d(TAG, "creating restInterface");
//            restInterface = deviceAdapter.create(RestInterface.class);
//        }
//    }
}
