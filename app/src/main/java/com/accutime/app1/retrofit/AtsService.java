package com.accutime.app1.retrofit;

import com.accutime.app1.businessObjects.Employee;
import com.accutime.app1.xmlparser.TagNode;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

/**
 * Created by jpbrahma on 3/12/15.
 */
public interface AtsService {
    /*
    @GET("/ats/biometrics/Lumidigm_Mercury_M300/{terminalId}/list/{versionId}/947 HTTP/1.1" 200 37435
            172.30.2.56 - - [13/May/2016:12:58:39 -0400] "GET /ats/biometrics/Lumidigm_Mercury_M300/1604247/item/400200/7 HTTP/1.1" 200 7745
            */
    @GET("/ats/heartbeat")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
         "User-Agent: UCS-XML/2.0 Universal"
    })
    Call<Heartbeat> heartbeat();

    @GET("/ats/heartbeat")
    @Headers({
            //"Authorization: ATS 1439144:PASS",
            "Authorization: ATS 123456:PASS",
            //"Authorization: ATS 000001325575:PASS",
            "User-Agent: UCS-XML/2.0 Universal"
    })
    Call<Response> heartbeatRaw();


    @GET("/ats/heartbeat")
    @Headers({
            //"Authorization: ATS 1439144:PASS",
            "Authorization: ATS 123456:PASS",
            //"Authorization: ATS 000001325575:PASS",
//            "User-Agent: UCS-XML/2.0 Universal"
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<TagNode> heartbeatRawCallback();

    @GET("/ats/heartbeat")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<TagNode> heartbeatRawCallback(@Query("config") String configuration,
                                       @Query("dld") String download,
                                       @Query("flash") String flash,
                                       @Query("employees") String employees,
                                       @Query("biometrics") String biometrics);

    public static class Heartbeat {
        public static class Password {
            @Element(name="PASS", required=false)
            public static String pass;
        }
        public static class Credentials {
            @Element(name="Password", required=false)
            public static Password password;
        }
        public static class Version {
            @Element(name="Dld", required=false)
            public static String Dld;
            @Element(name="Config", required = false)
            public static String Config;
            @Element(name="Flash", required = false)
            public static String Flash;
            @Element(name="Employee", required = true)
            public static String Employee;
            @Element(name="Schedule", required = true)
            public static int Schedule;
            @Element(name="AppTables")
            public static int AppTables;
            @Element(name="Biometrics", required = false)
            public static int Biometrics;
            @Element(name="Tasks", required = false)
            public static int Tasks;
        }

        @Element(name="Version")
        public Version version;

        @Element(name="Credentials", required=false)
        public Credentials credentials;
    }

    @GET("/ats/configuration/{requestedConfiguration}")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> configurationRequest(@Path("requestedConfiguration") String requestedConfiguration);


    //https://atsqa-wd.accu-time.com/ats/dld/QA-TechCenter
    @GET("/ats/dld/{requestedVersion}")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> downloadRequest(@Path("requestedVersion") String requestedVersion);

    // TODO : Update ATS Documentation for flash
    @Streaming
    @GET("/ats/flash/{requestedFlash}/{requestedFlash}.tar.gz/")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> flashRequest(@Path("requestedFlash") String requestedFlash);

    @GET("/ats/employees/{terminalId}/core/{requestedVersion}/{currentVersion}/")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> employeeRequestCore(@Path("terminalId") String terminalId,
                                           @Path("requestedVersion") String requestedVersion,
                                           @Path("currentVersion") String currentVersion);

    @GET("/ats/employees/{terminalId}/messages/{requestedVersion}/{currentVersion}/")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> employeeRequestMessages(@Path("terminalId") String terminalId,
                                           @Path("requestedVersion") String requestedVersion,
                                           @Path("currentVersion") String currentVersion);

    @GET("/ats/employees/{terminalId}/hours/{requestedVersion}/{currentVersion}/")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> employeeRequestHours(@Path("terminalId") String terminalId,
                                               @Path("requestedVersion") String requestedVersion,
                                               @Path("currentVersion") String currentVersion);

    @GET("/ats/employees/{terminalId}/complete/{requestedVersion}/{currentVersion}/")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> employeeRequestComplete(@Path("terminalId") String terminalId,
                                            @Path("requestedVersion") String requestedVersion,
                                            @Path("currentVersion") String currentVersion);

    @GET("/ats/biometrics/{readerType}/{terminalId}/list/{requestedVersion}/{currentVersion}/")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> biometricsRequestList(@Path("readerType") String readerType,
                                             @Path("terminalId") String terminalId,
                                             @Path("requestedVersion") String requestedVersion,
                                             @Path("currentVersion") String currentVersion);

    @GET("/ats/biometrics/{readerType}/{terminalId}/item/{requestedVersion}/{currentVersion}/")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> biometricsRequestItem(@Path("readerType") String readerType,
                                             @Path("terminalId") String terminalId,
                                             @Path("requestedVersion") String requestedVersion,
                                             @Path("currentVersion") String currentVersion);

    @POST("/ats/biometrics/")
    @Headers({
            "Authorization: ATS 123456:PASS",
            "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> biometricsPost(@Body Biometrics biometrics);
    @Root(name="Biometrics") public class Biometrics {
        @Attribute(name="uid", required=true) public String uid;

        @ElementList(inline = true, entry="Templates", required=true)
        public ArrayList<Templates> templatesList;
        public static class Templates {

            @ElementList(inline = true, entry = "Template", required = true)
            public ArrayList<Template> templateList;
            public static class Template {
                @Attribute(name="badge", required=true) public String badge;
                @Attribute(name="readerType", required=true) public String readerType;
                @Attribute(name="index", required=true) public String index;
                @Attribute(name="chk", required=true) public String checksum;
                @Attribute(name="delete", required=true) public String delete;
                public String bioTemplate;

                @Override
                public String toString() {
                    return "<Template badge=\"" + badge + "\"\n" +
                            "\treaderType=\"" + readerType + "\"\n" +
                            "\tindex=\"" + index + "\"\n" +
                            "\tchk=\"" + checksum + "\"\n" +
                            "\tdelete=\"" + delete + ">\"\n" +
                                "\t\t" + bioTemplate + "\n" +
                            "</Template>";
                }
            }

            @Override
            public String toString() {
                StringBuilder sb = new StringBuilder();
                for (Template template : templateList) {
                    sb.append(template);
                    sb.append("\n");
                }

                return sb.toString();
            }
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Templates templates : templatesList) {
                sb.append(templates);
                sb.append("\n");
            }
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<Biometrics uid=\""+uid +"\">\n" +
                    "\t" + sb.toString() + "\n" +
                    "</Biometrics>";
        }
    }

    /*
        <?xml version="1.0" encoding="utf-8"?>
        <Punches
            terminal="12345"
            tz="America/New_York"
            uid="f81d4fae-7dec-11d0-a765-00a0c91e6bf6"
            mode="buffered">
            <!-- Simple Punch with language with supervisor override -->
            <Punch>
                <Badge>123456789</Badge>
                <Timestamp>2009-09-04T14:23:35.000000+0600</Timestamp>
                <Action>IN</Action>
            </Punch>
        </Punches>
    */
    @Root(name="Punches") public class Punches {
        @Attribute(name="terminal", required=true) public String terminal;
        @Attribute(name="tz", required=true) public String tz;
        @Attribute(name="uid", required=true) public String uid;
        @Attribute(name="mode", required=false) public String mode;
        @ElementList(inline = true, entry="Punch", required=true) public ArrayList<Punch> punchList;
        public static class Punch {
            @Element(name="Badge", required=true) public String Badge;
            @Element(name="Timestamp", required = true) public String Timestamp;
            @Element(name="Action", required = true) public String Action;
            @Override
            public String toString() {
                return "<Punch>\n" +
                        "\t<Badge>"+Badge+"</Badge>\n" +
                        "\t<Timestamp>"+Timestamp+"</Timestamp>\n" +
                        "\t<Action>"+Action+"</Action>\n" +
                        "</Punch>";
            }
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Punch punch : punchList) {
                sb.append(punch);
            }
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<Punches\n" +
                    "\tterminal=\""+terminal+"\"\n" +
                    "\ttz=\""+tz+"\"\n" +
                    "\tuid=\""+uid+"\"\n" +
                    "\tmode=\""+mode+"\">\n" +
                    "\t" + sb.toString() + "\n" +
                    "</Punches>";
        }
    }

    @POST("/ats/punch")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
//         "User-Agent: UCS-XML/2.0 Universal"
         "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<ResponseBody> sendPunch(@Body Punches punches);



    @GET("/ats/employees")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
         "User-Agent: UCS-XML/2.0 Universal"
    })
    Call<Employees> getEmployees();


    @GET("/ats/employees")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
         "User-Agent: UCS-XML/2.0 Universal"
    })
    Call<Response> getEmployeesRaw();


    @GET("/ats/employees/complete/")//12467.0.0/0")
    @Headers({
         //"Authorization: ATS 1439144:PASS",
         "Authorization: ATS 123456:PASS",
         //"Authorization: ATS 000001325575:PASS",
//         "User-Agent: UCS-XML/2.0 Universal"
         "User-Agent: UCS-XML/3.0 Universal"
    })
    Call<TagNode> getEmployeesRaw1();


    @Root
    public static class Employees {
        @Attribute(name="full", required=false) public boolean full;
        @Attribute(name="version", required=false) public String version;
        @ElementList(inline=true, entry="Employee", required=false) public List<Employee> list;
    }

    /*
    <Employee badge="1a003" schedule_id="11504" break_schedule_id="11504"
     meal_schedule_id="11504" supervisor_level="1" language="">
    <Name>Oliver Reynolds</Name>
    <WeekToDateHours>0</WeekToDateHours>
    <SecondPunchTimeRestrict>1</SecondPunchTimeRestrict>
    FingerPrint localinstanceId="06ADFF4777" scanQuality="92" userId="22" device="superma"></FingerPrint>
    </Employee>
    */
}
