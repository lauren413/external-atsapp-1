LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

# Name of this application -- and give it system priviledges
LOCAL_PACKAGE_NAME := TimeCollect
LOCAL_CERTIFICATE := device/fsl/common/security/platform

# Include these libraries to use Appcompat-v7 and not have to downgrade themes, styles, and sdk version.
LOCAL_STATIC_JAVA_LIBRARIES := android-support-v4
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v7-appcompat
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v7-gridlayout
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v13

# This points to the folder with java files, ie activities.
LOCAL_SRC_FILES := $(call all-java-files-under, java)

# The first line points to the folder with the resource files including layouts. 
# The next two lines point to the Appcompat-v7 resources in the android build.
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res
LOCAL_RESOURCE_DIR += prebuilts/sdk/current/support/v7/appcompat/res
LOCAL_RESOURCE_DIR += prebuilts/sdk/current/support/v7/gridlayout/res

# These settings are also required to use Appcompat.
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
LOCAL_AAPT_FLAGS := --auto-add-overlay
LOCAL_AAPT_FLAGS += --extra-packages android.support.v7.appcompat:android.support.v7.gridlayout

include $(BUILD_PACKAGE)

