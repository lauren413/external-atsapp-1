LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
include $(LOCAL_PATH)/common.mk

#LOCAL_SHARED_LIBRARIES := libstlport
#include external/stlport/libstlport.mk

LOCAL_MODULE := snappydb_native
#LOCAL_NDK_STL_VARIANT := c++_static
LOCAL_NDK_STL_VARIANT := gnustl_static
LOCAL_C_INCLUDES := $(C_INCLUDES)
LOCAL_SDK_VERSION := 17
LOCAL_CPP_EXTENSION := cc
LOCAL_CFLAGS := -DLEVELDB_PLATFORM_ANDROID -std=gnu++0x -g -w
LOCAL_SRC_FILES := $(SOURCES) ./port/port_android.cc snappydb.cpp snappydblocal.cpp
LOCAL_LDLIBS +=  -llog -ldl
include $(BUILD_SHARED_LIBRARY)
#include $(BUILD_STATIC_LIBRARY)
