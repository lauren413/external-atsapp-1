/*
 * Copyright (C) 2013 Nabil HACHICHA.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <jni.h>
#include <string.h>
#include <sstream>
#include <iomanip>
#include <unordered_map>
#include <vector>
#include <stdlib.h>
#include <sys/time.h>
#include "com_snappydb_internal_DBImplLocal.h"
#include "leveldb/db.h"
#include "leveldb/options.h"
#include "debug.h";

extern void throwException(JNIEnv *env, const char* msg) ;

class DbProfile {
    public:
        explicit DbProfile(std::string pathStr):storagePath(pathStr){}
        bool isOpen;
        std::string storagePath;
        leveldb::DB* levelDb;
};

typedef std::unordered_map<std::string, DbProfile *>  DbProfileMap;
typedef std::unordered_map<std::string, long long>  TestMap;
DbProfileMap dbProfileMap;
TestMap testmap;

void testDbProfile() {
	std::string dbpath2 = std::string("abcd");
    LOGI("--dbpath2 %s", dbpath2.c_str());

    long long dbp = 0;
    std::unordered_map<std::string, long long>::const_iterator got  = testmap.find(dbpath2);
    if (got == testmap.end()) {
        LOGI("--NOT found for %s, dbp=%u", dbpath2.c_str(), dbp);
    }
    else {
        LOGI("-- found for %s, dbp=%u", got->first.c_str(), got->second);
        dbp = got->second;
    }
    if (dbp == 0) {
        LOGI("--dbp is NULL");
        std::string storagePath(dbpath2);
        LOGI("--storagePath %s", storagePath.c_str());
        dbp = 1001;
        testmap[storagePath] =  dbp;
    }
    LOGI("--dbp is =%x=", dbp);
    for (const auto &p : testmap) {
        LOGI("first=%s, second=%u", p.first.c_str() , p.second);
    }
    LOGI("-------------------------------------------");
    return;
}

DbProfile * getDbProfile(JNIEnv * env, jobject thiz) {
    //struct timeval tv1, tv2, diff;
    //gettimeofday(&tv1, NULL);
    jclass callingClass = env->GetObjectClass(thiz);
    jfieldID fieldId = env->GetFieldID(callingClass, "dbPath", "Ljava/lang/String;");
    jstring dbpath1 = (jstring) env->GetObjectField(thiz, fieldId);
	const char* dbpath2 = env->GetStringUTFChars(dbpath1, 0);
    //gettimeofday(&tv2, NULL);
    //timeval_subtract(&diff, &tv2, &tv1);
    //LOGI("--time to retrieve dbpath was  %d.%05d", diff.tv_sec, diff.tv_usec );
    LOGI("--dbPath2 %s", dbpath2);

    DbProfile * dbp = NULL;
    std::string storagePath(dbpath2);
    env->ReleaseStringUTFChars(dbpath1, dbpath2);
    auto search = dbProfileMap.find(storagePath);
    if (search != dbProfileMap.end()) {
        dbp = search->second;
    }
    if (dbp == NULL) {
        LOGI("--dbp is NULL");
        LOGI("--storagePath %s", storagePath.c_str());
        dbp = new DbProfile(storagePath);
        if (dbp == NULL) {
            throwException(env, "OutOfMemory when creating dbProfile");
            return NULL;
        }
        dbp->isOpen = false;
        dbProfileMap[storagePath] =  dbp;
    }
    LOGI("--dbp is =%x=", dbp);
    return dbp;
}

void clearDbProfile(DbProfile * dbp)  {
    if (dbp != NULL) {
        if (!dbp->storagePath.empty()) {
            dbProfileMap[dbp->storagePath] = NULL;
            dbProfileMap.erase(dbp->storagePath);
        }
        delete dbp;
    }
    return ;
}

void clearDbProfile1(const char * pathName)  {
    if (pathName != NULL) {
        DbProfile * dbp = dbProfileMap[pathName];
        clearDbProfile(dbp);
    }
    return ;
}


//***************************
//*   Internal utilities
//***************************



 /* Subtract the `struct timeval' values X and Y,
  * result = X - Y
    storing the result in RESULT.
    Return 1 if the difference is negative, otherwise 0.  */

int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
    /* Perform the carry for the later subtraction by updating y. */
    if (x->tv_usec < y->tv_usec) {
        int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
        y->tv_usec -= 1000000 * nsec;
        y->tv_sec += nsec;
    }
    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }

    /* Compute the time remaining to wait.
       tv_usec is certainly positive. */
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;

    /* Return 1 if result is negative. */
    return x->tv_sec < y->tv_sec;
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1open(JNIEnv * env,
		jobject thiz, jstring dbpath) {
	LOGI("Opening database");

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp != NULL) {
        if (dbp->isOpen) {
            LOGI("database was already open %s",dbp->storagePath.c_str());
            return;
        } else {
            LOGI("--database was not already open %s", dbp->storagePath.c_str());
        }
    } else {
		throwException (env, "Could not create DbProfile");
    }

	leveldb::Options options;
	options.create_if_missing = true;
	options.compression = leveldb::kSnappyCompression;
	leveldb::Status status = leveldb::DB::Open(options, dbp->storagePath.c_str(), &(dbp->levelDb));

	LOGI("-- address of levelDb %d", &(dbp->levelDb));

	if (status.ok()) {
		LOGI("Opened database");
        dbp->isOpen = true;
	} else {
		LOGE("Failed to open database");
        dbp->isOpen = false;
		std::string err("Failed to open/create database: " + status.ToString());
		throwException (env, err.c_str());
        return;
	}
}

void closeDb(JNIEnv *env, DbProfile *dbp) {
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }
        if (dbp->isOpen) {
            LOGI("Closing database %s", dbp->storagePath.c_str());
            delete dbp->levelDb;
            clearDbProfile(dbp);
            dbp->isOpen = false;
        } else {
            clearDbProfile(dbp);
            throwException (env, "Database was already closed");
        }
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1close(JNIEnv *env,
		jobject thiz) {
    DbProfile * dbp = getDbProfile(env, thiz);
    closeDb(env, dbp);
}


JNIEXPORT jboolean JNICALL Java_com_snappydb_internal_DBImplLocal__1_1isOpen
  (JNIEnv * env, jobject thiz) {
    LOGI("Is database open");
    DbProfile * dbp = getDbProfile(env, thiz);

    if (dbp->isOpen) {
        return JNI_TRUE;
    } else {
        return JNI_FALSE;
    }
  }

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1destroy(
		JNIEnv * env, jobject thiz, jstring dbpath) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }
	LOGI("Destroying database %s", dbp->storagePath.c_str());
	leveldb::Options options;
	leveldb::Status status = DestroyDB(dbp->storagePath.c_str(), options);

	if (status.ok()) {
        LOGI("Destroyed database %s", dbp->storagePath.c_str());
        closeDb(env,dbp);
	} else {
        closeDb(env,dbp);
		std::string err("Failed to destroy database: " + status.ToString());
		throwException (env, err.c_str());
	}
}


//***********************
//*      CREATE
//***********************

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1del(JNIEnv *env,
		jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }
	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Deleting entry");
	const char* key = env->GetStringUTFChars(jKey, 0);

	leveldb::Status status = dbp->levelDb->Delete(leveldb::WriteOptions(), key);
	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully delete");

	} else {
		std::string err("Failed to delete: " + status.ToString());
		throwException (env, err.c_str());
	}
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1put__Ljava_lang_String_2Ljava_lang_String_2(
		JNIEnv *env, jobject thiz, jstring jKey, jstring jValue) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Putting a String ");


	const char* key = env->GetStringUTFChars(jKey, 0);
	const char* value = env->GetStringUTFChars(jValue, 0);

	leveldb::Status status = dbp->levelDb->Put(leveldb::WriteOptions(), key, value);
	env->ReleaseStringUTFChars(jValue, value);
	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully storing a String");

	} else {
		std::string err("Failed to put a String: " + status.ToString());
		throwException (env, err.c_str());

	}
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1put__Ljava_lang_String_2_3B(
		JNIEnv *env, jobject thiz, jstring jKey, jbyteArray arr) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Putting a Serializable ");

	int len = env->GetArrayLength(arr);
	jbyte* data =  (jbyte*)env->GetPrimitiveArrayCritical(arr, 0);
	if (data == NULL) {
	    /* out of memory exception thrown */
		throwException(env, "OutOfMemory when trying to get bytes array for Serializable");
		return;
	}

	const char* key(env->GetStringUTFChars(jKey, 0));
	leveldb::Slice value(reinterpret_cast<char*>(data), len);

	leveldb::Status status = dbp->levelDb->Put(leveldb::WriteOptions(), key, value);

	env->ReleasePrimitiveArrayCritical(arr, data, 0);
	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully writing a Serializable");
	} else {
		std::string err("Failed to put a Serializable: " + status.ToString());
		throwException(env, err.c_str());
	}
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1putLong(
		JNIEnv *env, jobject thiz, jstring jKey, jlong jVal) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Putting a long ");

	const char* key(env->GetStringUTFChars(jKey, 0));
	leveldb::Slice value((char*) &jVal, sizeof(jlong));

	leveldb::Status status = dbp->levelDb->Put(leveldb::WriteOptions(), key, value);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully writing a long");

	} else {
		std::string err("Failed to put a long: " + status.ToString());
		throwException(env, err.c_str());
	}
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1putInt(JNIEnv *env,
		jobject thiz, jstring jKey, jint jVal) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Putting an int");

	const char* key(env->GetStringUTFChars(jKey, 0));
	leveldb::Slice value((char*) &jVal, sizeof(jint));

	leveldb::Status status = dbp->levelDb->Put(leveldb::WriteOptions(), key, value);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully writing an int");
	} else {
		std::string err("Failed to put an int: " + status.ToString());
		throwException(env, err.c_str());
	}
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1putShort(
		JNIEnv *env, jobject thiz, jstring jKey, jshort jValue) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Putting a short");

	const char* key(env->GetStringUTFChars(jKey, 0));
	leveldb::Slice value((char*) &jValue, sizeof(jshort));

	leveldb::Status status = dbp->levelDb->Put(leveldb::WriteOptions(), key, value);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully writing a short");
	} else {
		std::string err("Failed to put a short: " + status.ToString());
		throwException(env, err.c_str());
	}
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1putBoolean(
		JNIEnv *env, jobject thiz, jstring jKey, jboolean jValue) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Putting a boolean");

	const char* key(env->GetStringUTFChars(jKey, 0));
	leveldb::Slice value((char*) &jValue, sizeof(jboolean));

	leveldb::Status status = dbp->levelDb->Put(leveldb::WriteOptions(), key, value);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully writing a boolean");

	} else {
		std::string err("Failed to put a boolean: " + status.ToString());
		throwException(env, err.c_str());
	}
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1putDouble(
		JNIEnv *env, jobject thiz, jstring jKey, jdouble jVal) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Putting a double");

	const char* key(env->GetStringUTFChars(jKey, 0));

	std::ostringstream oss;
	oss << std::setprecision(17) << jVal;
	std::string value = oss.str();
	leveldb::Status status = dbp->levelDb->Put(leveldb::WriteOptions(), key, value);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully writing a double");
	} else {
		std::string err("Failed to put a double: " + status.ToString());
		throwException(env, err.c_str());
	}
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1putFloat(
		JNIEnv *env, jobject thiz, jstring jKey, jfloat jValue) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return;
	}

	LOGI("Putting a float");


	const char* key(env->GetStringUTFChars(jKey, 0));
	std::ostringstream oss;
	oss << std::setprecision(16) << jValue;
	std::string value = oss.str();

	leveldb::Status status = dbp->levelDb->Put(leveldb::WriteOptions(), key, value);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully writing a float");

	} else {
		std::string err("Failed to put a float: " + status.ToString());
		throwException(env, err.c_str());
	}
}

//***********************
//*      RETRIEVE
//***********************

JNIEXPORT jlong JNICALL Java_com_snappydb_internal_DBImplLocal__1_1getLong(JNIEnv *env,
		jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("Getting a long");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string data;
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &data);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		if (8 == data.length()) {
			LOGI("Successfully reading a long");
			const char* bytes = data.data();
			long long ret = 0;

			ret = bytes[7];
			ret = (ret << 8) + (unsigned char)bytes[6];
			ret = (ret << 8) + (unsigned char)bytes[5];
			ret = (ret << 8) + (unsigned char)bytes[4];
			ret = (ret << 8) + (unsigned char)bytes[3];
			ret = (ret << 8) + (unsigned char)bytes[2];
			ret = (ret << 8) + (unsigned char)bytes[1];
			ret = (ret << 8) + (unsigned char)bytes[0];
			return ret;
		} else {
			throwException(env, "Failed to get a long");
			return NULL;
		}
	} else {
		std::string err("Failed to get a long: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}

JNIEXPORT jint JNICALL Java_com_snappydb_internal_DBImplLocal__1_1getInt(JNIEnv *env,
		jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("Getting an int");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string data;
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &data);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		if (4 == data.length()) {
			LOGI("Successfully reading an int");

			const char* bytes = data.data();
			int ret = 0;
			ret = (unsigned char)bytes[3];
			ret = (ret << 8) + (unsigned char)bytes[2];
			ret = (ret << 8) + (unsigned char)bytes[1];
			ret = (ret << 8) + (unsigned char)bytes[0];

			return ret;

		} else {
			throwException(env, "Failed to get an int");
			return NULL;
		}

	} else {
		std::string err("Failed to get an int: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}

JNIEXPORT jdouble JNICALL Java_com_snappydb_internal_DBImplLocal__1_1getDouble(
		JNIEnv *env, jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("Getting a double");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string data;
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &data);

	env->ReleaseStringUTFChars(jKey, key);


	if (status.ok()) {// we can't use data.length() here to make sure of the size of float since it was encoded as string
		double d = atof(data.c_str());
		LOGI("Successfully reading a double");
		return d;

	} else {
		std::string err("Failed to get a double: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}

JNIEXPORT jshort JNICALL Java_com_snappydb_internal_DBImplLocal__1_1getShort(JNIEnv *env,
		jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("Getting a short");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string data;
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &data);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		if (2 == data.length()) {
			LOGI("Successfully reading a short");

			const char* bytes = data.data();
			short ret = 0;
			ret = bytes[1];
			ret = (ret << 8) + bytes[0];

			return ret;

		} else {
			throwException(env, "Failed to get a short");
			return NULL;
		}

	} else {
		std::string err("Failed to get a short: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}

JNIEXPORT jboolean JNICALL Java_com_snappydb_internal_DBImplLocal__1_1getBoolean(
		JNIEnv *env, jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("Getting a boolean");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string data;
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &data);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		if (1 == data.length()) {
			LOGI("Successfully reading a boolean");
			return data.data()[0];

		} else {
			throwException(env, "Failed to get a boolean");
			return NULL;
		}
	} else {
		std::string err("Failed to get a boolean: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}

JNIEXPORT jstring JNICALL Java_com_snappydb_internal_DBImplLocal__1_1get(JNIEnv *env,
		jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("Getting a String");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string value;
	leveldb::ReadOptions();
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &value);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Successfully reading a String");
		const char* re = value.c_str();
		return env->NewStringUTF(re);

	} else {
		std::string err("Failed to get a String: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}

JNIEXPORT jbyteArray JNICALL Java_com_snappydb_internal_DBImplLocal__1_1getBytes(
		JNIEnv *env, jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("Getting a byte array");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string data;
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &data);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		int size = data.size();

		char* elems = const_cast<char*>(data.data());
		jbyteArray array = env->NewByteArray(size * sizeof(jbyte));
		env->SetByteArrayRegion(array, 0, size, reinterpret_cast<jbyte*>(elems));

		LOGI("Successfully reading a byte array");
		return array;

	} else {
		std::string err("Failed to get a byte array: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}

JNIEXPORT jfloat JNICALL Java_com_snappydb_internal_DBImplLocal__1_1getFloat(JNIEnv *env,
		jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("Getting a float");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string data;
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &data);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {// we can't use data.length() here to make sure of the size of float since it was encoded as string
			LOGI("Successfully reading a float");
			float f = atof(data.c_str());
			return f;

	} else {
		std::string err("Failed to get a float: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}


//****************************
//*      KEYS OPERATIONS
//****************************

JNIEXPORT jboolean JNICALL Java_com_snappydb_internal_DBImplLocal__1_1exists
  (JNIEnv *env, jobject thiz, jstring jKey) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("does key exists");

	const char* key = env->GetStringUTFChars(jKey, 0);
	std::string value;
	leveldb::Status status = dbp->levelDb->Get(leveldb::ReadOptions(), key, &value);

	env->ReleaseStringUTFChars(jKey, key);

	if (status.ok()) {
		LOGI("Key Found ");
		return JNI_TRUE;

	} else if (status.IsNotFound()) {
		LOGI("Key Not Found ");
		return JNI_FALSE;

	} else {
		std::string err("Failed to check if a key exists: " + status.ToString());
		throwException(env, err.c_str());
		return NULL;
	}
}


JNIEXPORT jobjectArray JNICALL Java_com_snappydb_internal_DBImplLocal__1_1findKeys
  (JNIEnv *env, jobject thiz, jstring jPrefix, jint offset, jint limit) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("find keys");

	const char* prefix = env->GetStringUTFChars(jPrefix, 0);

	std::vector<std::string> result;
	leveldb::Iterator* it = dbp->levelDb->NewIterator(leveldb::ReadOptions());

	int count = 0;
	for (it->Seek(prefix); count < (offset + limit) && it->Valid() && it->key().starts_with(prefix);
			it->Next()) {
        if (count >= offset) {
    		result.push_back(it->key().ToString());
    	}
        ++count;
	}

	std::vector<std::string>::size_type n = result.size();
	jobjectArray ret= (jobjectArray)env->NewObjectArray(n,
		         env->FindClass("java/lang/String"),
		         NULL);

	jstring str;
	for (int i=0; i<n ; i++) {
		str = env->NewStringUTF(result[i].c_str());
		env->SetObjectArrayElement(ret, i, str);
		env->DeleteLocalRef(str);
	}

	env->ReleaseStringUTFChars(jPrefix, prefix);
	delete it;

	return ret;
}

JNIEXPORT jint JNICALL Java_com_snappydb_internal_DBImplLocal__1_1countKeys
  (JNIEnv *env, jobject thiz, jstring jPrefix) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("count keys");

	const char* prefix = env->GetStringUTFChars(jPrefix, 0);

	leveldb::Iterator* it = dbp->levelDb->NewIterator(leveldb::ReadOptions());

	jint count = 0;
	for (it->Seek(prefix); it->Valid() && it->key().starts_with(prefix);
    		it->Next()) {
    	++count;
    }

    env->ReleaseStringUTFChars(jPrefix, prefix);
    delete it;

    return count;
}

JNIEXPORT jobjectArray JNICALL Java_com_snappydb_internal_DBImplLocal__1_1findKeysBetween
  (JNIEnv *env, jobject thiz, jstring jStartPrefix, jstring jEndPrefix, jint offset, jint limit) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("find keys between range");

	const char* startPrefix = env->GetStringUTFChars(jStartPrefix, 0);
	const char* endPrefix = env->GetStringUTFChars(jEndPrefix, 0);

	std::vector<std::string> result;
	leveldb::Iterator* it = dbp->levelDb->NewIterator(leveldb::ReadOptions());

	int count = 0;
	for (it->Seek(startPrefix); count < (offset + limit) && it->Valid() && it->key().compare(endPrefix) <= 0;
			it->Next()) {
		if (count >= offset) {
    		result.push_back(it->key().ToString());
    	}
    	++count;
	}

	std::vector<std::string>::size_type n = result.size();
	jobjectArray ret= (jobjectArray)env->NewObjectArray(n,
		         env->FindClass("java/lang/String"),
		         env->NewStringUTF(""));

	jstring str;
	for (int i=0; i<n ; i++) {
		str = env->NewStringUTF(result[i].c_str());
		env->SetObjectArrayElement(ret, i, str);
		env->DeleteLocalRef(str);
	}

	env->ReleaseStringUTFChars(jStartPrefix, startPrefix);
	env->ReleaseStringUTFChars(jEndPrefix, endPrefix);
	delete it;

	return ret;
}

JNIEXPORT jint JNICALL Java_com_snappydb_internal_DBImplLocal__1_1countKeysBetween
  (JNIEnv *env, jobject thiz, jstring jStartPrefix, jstring jEndPrefix) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("count keys between range");

	const char* startPrefix = env->GetStringUTFChars(jStartPrefix, 0);
	const char* endPrefix = env->GetStringUTFChars(jEndPrefix, 0);

	leveldb::Iterator* it = dbp->levelDb->NewIterator(leveldb::ReadOptions());

	jint count = 0;
	for (it->Seek(startPrefix); it->Valid() && it->key().compare(endPrefix) <= 0;
			it->Next()) {
    	++count;
	}

	env->ReleaseStringUTFChars(jStartPrefix, startPrefix);
	env->ReleaseStringUTFChars(jEndPrefix, endPrefix);
	delete it;

	return count;
}

JNIEXPORT jlong JNICALL Java_com_snappydb_internal_DBImplLocal__1_1findKeysIterator
  (JNIEnv *env, jobject thiz, jstring jPrefix, jboolean reverse) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("find keys iterator");

	leveldb::Iterator* it = dbp->levelDb->NewIterator(leveldb::ReadOptions());

	if (jPrefix) {
		const char* prefix = env->GetStringUTFChars(jPrefix, 0);
		LOGI("(%p) Seeking prefix: %s", it, prefix);
		it->Seek(prefix);
	    env->ReleaseStringUTFChars(jPrefix, prefix);
	} else if (reverse) {
		it->SeekToLast();
	} else {
		it->SeekToFirst();
	}

	// When seeking in a leveldb iterator, if the key does not exists, it is positioned to the key
	// immediately *after* what we are seeking or invalid. In the case of a reverse iterator, we
	// want the key immediately *before* or the last.
	if (reverse) {
		if (!it->Valid()) {
			it->SeekToLast();
		} else if (jPrefix) {
			const char* prefix = env->GetStringUTFChars(jPrefix, 0);
			if (it->key().compare(prefix) > 0) {
				it->Prev();
			}
			env->ReleaseStringUTFChars(jPrefix, prefix);
		}
	}

	return (jlong) it;
}

JNIEXPORT jobjectArray JNICALL Java_com_snappydb_internal_DBImplLocal__1_1iteratorNextArray
  (JNIEnv *env, jobject thiz, jlong ptr, jstring jEndPrefix, jboolean reverse, jint max) {

    DbProfile * dbp = getDbProfile(env, thiz);
    if (dbp == NULL) {
        throwException (env, "Database is closed or it doesnt exist");
        return NULL;
    }

	if (!dbp->isOpen) {
		throwException (env, "database is not open");
		return NULL;
	}

	LOGI("iterator next array");

	std::vector<std::string> result;
	leveldb::Iterator* it = (leveldb::Iterator*) ptr;

	if (!it->Valid()) {
		throwException (env, "iterator is not valid");
		return NULL;
	}

	const char* endPrefix = NULL;
	if (jEndPrefix) {
		endPrefix = env->GetStringUTFChars(jEndPrefix, 0);
	}

	int count = 0;
	while (count < max && it->Valid() && (!endPrefix || (!reverse && it->key().compare(endPrefix) <= 0) || (reverse && it->key().compare(endPrefix) >= 0))) {
		result.push_back(it->key().ToString());
    	++count;
        if (reverse) { it->Prev(); }
        else { it->Next(); }
	}

	if (jEndPrefix) {
		env->ReleaseStringUTFChars(jEndPrefix, endPrefix);
	}

	std::vector<std::string>::size_type n = result.size();
	jobjectArray ret= (jobjectArray)env->NewObjectArray(n,
		         env->FindClass("java/lang/String"),
		         env->NewStringUTF(""));

	jstring str;
	for (int i=0; i<n ; i++) {
		str = env->NewStringUTF(result[i].c_str());
		env->SetObjectArrayElement(ret, i, str);
		env->DeleteLocalRef(str);
	}

	return ret;
}

JNIEXPORT jboolean JNICALL Java_com_snappydb_internal_DBImplLocal__1_1iteratorIsValid
  (JNIEnv *env, jobject thiz, jlong ptr, jstring jEndPrefix, jboolean reverse) {

	LOGI("iterator is valid");

	leveldb::Iterator* it = (leveldb::Iterator*) ptr;

	if (!it->Valid()) {
		return false;
	}
	if (jEndPrefix) {
		const char* endPrefix = env->GetStringUTFChars(jEndPrefix, 0);
		if ((!reverse && it->key().compare(endPrefix) > 0) || (reverse && it->key().compare(endPrefix) < 0)) {
			env->ReleaseStringUTFChars(jEndPrefix, endPrefix);
			return false;
		}
		env->ReleaseStringUTFChars(jEndPrefix, endPrefix);
	}
	return true;
}

JNIEXPORT void JNICALL Java_com_snappydb_internal_DBImplLocal__1_1iteratorClose
  (JNIEnv *env, jobject thiz, jlong ptr) {

	LOGI("iterator delete");

	leveldb::Iterator* it = (leveldb::Iterator*) ptr;

	delete it;
}
